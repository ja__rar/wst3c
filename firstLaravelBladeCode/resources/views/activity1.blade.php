<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            
        </style>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
            .town{
                color: red;
            }
            .password{
                border-color: red;
                border-width: thick;
                border-style: solid;
            }
            .click{
                border-color: red;
                border-width: medium;
                border-style: solid;
                background: green;
                color: white;
                padding: 10px;
                font-weight: bold;
            }
        </style>
    </head>
    <body class="antialiased">

            <form>
             <fieldset>
              <legend>Personal Information</legend>
              <div class="form-group mt-4">
                <label for="firstname">Firstname:</label><br>
                <input type="text" class="form-control" id="firstname" placeholder="Enter your firstname">
              </div><br>
              <div class="form-group mt-4">
                <label for="lastname">Lastname:</label><br>
                <input type="text" class="form-control" id="lastname" placeholder="Enter your lastname">
              </div><br>
              <div class="form-group mt-4">
                <label for= "username">Username:</label><br>
                <input type="text" class="form-control" id= "username" placeholder="Enter your username">
              </div><br>
              <div class="form-group mt-4">
                <label for="password">Password:</label><br>
                <input class="password" type="password" class="form-control" id="password" placeholder="Enter your password">
              </div><br>
              <div class="form-group mt-4">
                <textarea class="form-control" id="txtarea" rows="10" cols="50"></textarea>
              </div>
              <div class="form-group mt-4">
                <label for="bday">Birthdate:</label>
                <input type="date" class="form-control" id="bday">
              </div>
              <div class="form-group">
                <label for="town" class="town">Town:</label>
                <select class="form-control" id="town">
                  <option>Nancayasan</option>
                  <option>Dilan Paurido</option>
                  <option>Sta. Lucia</option>
                  <option>Nancamaliran West</option>
                  <option>Nancamaliran East</option>
                </select>
              </div>
              <div class="form-group mt-4">
                <label for="browser">Browser:</label>
                <input type="text" class="form-control" id="browser">
              </div>
                <input class="click" type="submit" value="Click Me">
                <input type="submit" value="Reset">
                <input type="submit" value="pindot me">
                <br>
             </fieldset>
            </form>

    </body>
</html>
