<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/customer/{cusid?}/{cusname?}/{cusadd?}', [OrderController::class, 'displayCustomer' ])->name('order.customer');

Route::get('/item/{itemnum?}/{itemname?}/{itemprice?}', [OrderController::class, 'displayItem' ])->name('order.item');

Route::get('/order/{cusid?}/{cusname?}/{ordernum?}/{date?}', [OrderController::class, 'displayOrder' ])->name('order.order');

Route::get('/details/{transnum?}/{ordernum?}/{itemid?}/{name?}/{price?}/{quantity?}', [OrderController::class, 'displayDetails' ])->name('order.details');
