<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<title>Order</title>
</head>
<body>
	<br>
	<br>
		<div class = "container mx-auto bg-success">
			<center><b class = "text-light">You're in the Order Page</b></center>
		</div>
		<br>
		<div class = "container mx-auto">
			<div class="form-group row">
			  <div class="col-xs-2 m-5">
			    <label for="ex1">Customer ID</label>
			    <input class="form-control bg-success text-light" id="ex1" type="text" value="{{$cusid}}" readonly="">
			  </div>
			  <div class="col-xs-3 m-5">
			    <label for="ex2">Customer Name</label>
			    <input class="form-control bg-success text-light" id="ex2" type="text" value="{{$cusname}}" readonly="">
			  </div>
			  <div class="col-xs-4 m-5">
			    <label for="ex3">Order No</label>
			    <input class="form-control bg-success text-light" id="ex3" type="text" value="{{$ordernum}}" readonly="">
			  </div>
			  <div class="col-xs-4 m-5">
			    <label for="ex3">Date</label>
			    <input class="form-control bg-success text-light" id="ex3" type="text" value="{{$date}}" readonly="">
			  </div>

			</div>
		</div>

</body>
</html>