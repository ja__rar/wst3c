<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<title>Item</title>
</head>
<body>
	<center>
		<br>
		<br>
		<div class = "container mx-auto bg-info">
			<b class = "text-light">You're in the Item Page</b>
		</div>
		<br>
		<div class = "container mx-auto">
			<div class="form-group row">
			  <div class="col-xs-2 m-5">
			    <label for="ex1">Item No</label>
			    <input class="form-control bg-info text-light" id="ex1" type="text" value="{{$itemnum}}" readonly="">
			  </div>
			  <div class="col-xs-3 m-5">
			    <label for="ex2">Item Name</label>
			    <input class="form-control bg-info text-light" id="ex2" type="text" value="{{$itemname}}" readonly="">
			  </div>
			  <div class="col-xs-4 m-5">
			    <label for="ex3">Item Price</label>
			    <input class="form-control bg-info text-light" id="ex3" type="text" value="{{$itemprice}}" readonly="">
			  </div>
			</div>
		</div>
	</center>
</body>
</html>