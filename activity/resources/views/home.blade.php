<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<title>Home</title>
</head>
<body>
	<br>
	<center>
		<div class = "container mx-auto bg-primary">
			<b class = "text-light">Homepage</b>
		</div>
		<div class = "container p-4">
			<a href="{{ route('order.item') }}">Item</a>
			<br>
			<a href="{{ route('order.customer') }}">Customer</a>
			<br>
			<a href="{{ route('order.order') }}">Order</a>
			<br>
			<a href="{{ route('order.details') }}">Order Details</a>
		</div>
	</center>
</body>
</html>