<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<title>Details</title>
</head>
<body>
	<br>
	<br>
		<div class = "container mx-auto bg-danger">
			<center><b class = "text-light">You're in the Order Details Page</b></center>
		</div>
		<br>
		<div class = "container mx-auto">
			<div class="form-group row">
			  <div class="col-xs-2 m-5">
			    <label for="ex1">Transaction No</label>
			    <input class="form-control bg-danger text-light" id="ex1" type="text" value="{{$transnum}}" readonly="">
			  </div>
			  <div class="col-xs-3 m-5">
			    <label for="ex2">Order No</label>
			    <input class="form-control bg-danger text-light" id="ex2" type="text" value="{{$ordernum}}" readonly="">
			  </div>
			  <div class="col-xs-4 m-5">
			    <label for="ex3">Item ID</label>
			    <input class="form-control bg-danger text-light" id="ex3" type="text" value="{{$itemid}}" readonly="">
			  </div>
			  <div class="col-xs-4 m-5">
			    <label for="ex3">Name</label>
			    <input class="form-control bg-danger text-light" id="ex3" type="text" value="{{$name}}" readonly="">
			  </div>
			  <div class="col-xs-4 m-5">
			    <label for="ex3">Price</label>
			    <input class="form-control bg-danger text-light" id="ex3" type="text" value="{{$price}}" readonly="">
			  </div>
			  <div class="col-xs-4 m-5">
			    <label for="ex3">Quantity</label>
			    <input class="form-control bg-danger text-light" id="ex3" type="text" value="{{$quantity}}" readonly="">
			  </div>

			</div>
		</div>


</body>
</html>