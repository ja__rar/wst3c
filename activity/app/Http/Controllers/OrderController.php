<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function displayCustomer($cusid, $cusname, $cusadd)  
    {  
       return view('customer')->with(['cusid'=>$cusid, 'cusname'=>$cusname, 'cusadd'=>$cusadd]);;  
    }

    public function displayItem($itemnum, $itemname, $itemprice)  
    {  
       return view('item')->with(['itemnum'=>$itemnum, 'itemname'=>$itemname, 'itemprice'=>$itemprice]);;  
    }

    public function displayOrder($cusid, $cusname, $ordernum, $date)  
    {  
       return view('order')->with(['cusid'=>$cusid, 'cusname'=>$cusname, 'ordernum'=>$ordernum, 'date'=>$date]);;  
    }

    public function displayDetails($transnum, $ordernum, $itemid, $name, $price, $quantity)  
    {  
       return view('details')->with(['transnum'=>$transnum, 'ordernum'=>$ordernum, 'itemid'=>$itemid, 'name'=>$name, 'price'=>$price, 'quantity'=>$quantity]);;  
    }
}
