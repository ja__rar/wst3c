<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="/images/icon.png" type="image/x-icon">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
        <script>
            const toggleForm = () => {
                const container = document.querySelector('.container');
                container.classList.toggle('active');
            };
        </script>

        <title>Contact Us/Gallery</title>
        <link href="/css/gallery.css" rel="stylesheet" type="text/css">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
        </style>
    </head>
    <body class="antialiased">
      <nav id="navbar" class="">
        <div class="nav-wrapper">
          <!-- Navbar Logo -->
          <div class="logo">
            <a href="homepage"><img class="logo" src="/images/logo.png" /></a>
          </div>

          <!-- Navbar Links -->
          <ul id="menu">
            <li><a href="homepage">Home</a></li><!--
         --><li><a href="about">About Me</a></li><!--
         --><li><a href="gallery">Contact Us/Gallery</a></li>
            <li><a href="registration">Login/Registration</a></li>

          </ul>
        </div>
      </nav>


      <!-- Menu Icon -->
      <div class="menuIcon">
        <span class="icon icon-bars"></span>
        <span class="icon icon-bars overlay"></span>
      </div>


      <div class="overlay-menu">
        <ul id="menu">
            <li><a href="#home">Home</a></li>
            <li><a href="#about">About Me</a></li>
            <li><a href="#congal">Contact Us/Gallery</a></li>
            <li><a href="#registration">Registration</a></li>

          </ul>
      </div>

      <div class="hero">
        <div class="text">
          <p class="text__short">Sunsets. Portraits. Furbabies. Travels. Photography.</p>
          <h1 class="text__title">These are the days we live for</h1>
          <p class="text__description">In a room full of art, I'd still stare at you. In a room full of people, I'd still search for you. In a room full of flowers, I'd still pick you. In a life full of imperfections, I'll still choose the most broken part of you.</p>
          <a class="text__button"href="#">Show more photos</a>
        </div>
        
        <div class="grid__container">
          <div class="grid__item one"></div>
          <div class="grid__item two"></div>
          <div class="grid__item three"></div>
          <div class="grid__item four"></div>
          <div class="grid__item five"></div>
          <div class="grid__item six"></div>
          <div class="grid__item seven"></div>
          <div class="grid__item eight"></div>
          <div class="grid__item nine"></div>
          <div class="grid__item ten"></div>
          <div class="grid__item eleven"></div>
          <div class="grid__item twelve"></div>
        </div>
      </div>

      <div class="cont pt-4 mt-auto">
        <ul class="contact">
          <li style="text-align: left;">
            <p><i class="fa fa-map-marker fa-3x"></i></p>
            <p><strong>Pangasinan</strong></br>Pozorrubio, Pangasinan</p>
          </li>
          <li style="text-align: left;">
            <p><i class="fa fa-phone fa-3x"></i></p>
            <p><strong>Phone</strong></br>+63-945-755-2819</p>
          </li>
          <li style="text-align: left;">
            <p><i class="fa fa-envelope fa-3x"></i></p>
            <p><strong>Email</strong></br>jrosal_19ur0206@psu.edu.ph</p>
          </li>
        </ul>
      </div>

    </body>
</html>
