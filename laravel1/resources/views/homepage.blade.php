<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="/images/icon.png" type="image/x-icon">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
        <script>
          document.addEventListener("DOMContentLoaded", function () {
  const options = {
    root: null,
    rootMargin: "0px",
    threshold: 0.4
  };

  // IMAGE ANIMATION

  let revealCallback = (entries) => {
    entries.forEach((entry) => {
      let container = entry.target;

      if (entry.isIntersecting) {
        console.log(container);
        container.classList.add("animating");
        return;
      }

      if (entry.boundingClientRect.top > 0) {
        container.classList.remove("animating");
      }
    });
  };

  let revealObserver = new IntersectionObserver(revealCallback, options);

  document.querySelectorAll(".reveal").forEach((reveal) => {
    revealObserver.observe(reveal);
  });

  // TEXT ANIMATION

  let fadeupCallback = (entries) => {
    entries.forEach((entry) => {
      let container = entry.target;
      container.classList.add("not-fading-up");

      if (entry.isIntersecting) {
        container.classList.add("fading-up");
        return;
      }

      if (entry.boundingClientRect.top > 0) {
        container.classList.remove("fading-up");
      }
    });
  };

  let fadeupObserver = new IntersectionObserver(fadeupCallback, options);

  document.querySelectorAll(".fadeup").forEach((fadeup) => {
    fadeupObserver.observe(fadeup);
  });
});

            const toggleForm = () => {
                const container = document.querySelector('.container');
                container.classList.toggle('active');
            };
        </script>

        <title>Home</title>
        <link href="/css/home.css" rel="stylesheet" type="text/css">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
          body {
                font-family: 'Nunito', sans-serif;
          }   
        </style>

    </head>
    <body class="antialiased">
      
      <nav id="navbar" class="">
        <div class="nav-wrapper">
          <!-- Navbar Logo -->
          <div class="logo">
            <a href="homepage"><img class="logo" src="/images/logo.png" /></a>
          </div>

          <!-- Navbar Links -->
          <ul id="menu">
            <li><a href="homepage">Home</a></li>
            <li><a href="about">About Me</a></li>
            <li><a href="gallery">Contact Us/Gallery</a></li>
            <li><a href="registration">Login/Registration</a></li>

          </ul>
        </div>
      </nav>

      <!-- Menu Icon -->
      <div class="menuIcon">
        <span class="icon icon-bars"></span>
        <span class="icon icon-bars overlay"></span>
      </div>


      <div class="overlay-menu">
        <ul id="menu">
            <li><a href="#home">Home</a></li>
            <li><a href="#about">About Me</a></li>
            <li><a href="#congal">Contact Us/Gallery</a></li>
            <li><a href="#registration">Login/Registration</a></li>
          </ul>
      </div>

      <section>


      <div class="projects-container pt-5">
      <a href="" class="project-tile">
        <img src="/images/home1.jpg" alt="Traveller">
        <p>Escape</p>
      </a>
        
      <a href="" class="project-tile">
        <img src="/images/home2.jpg" alt="Plantita">
        <p>Plantita</p>
      </a>
        
      <a href="" class="project-tile">
        <img src="/images/home3.jpg" alt="FurMOM">
        <p>FurMOM</p>
      </a>
        
      <a href="" class="project-tile">
        <img src="/images/home4.jpg" alt="Moon and Stars">
        <p>Uranophile</p>
      </a>
        
      <a href="" class="project-tile">
        <img src="/images/home5.jpg" alt="Tree">
        <p>Dendrophile</p>
      </a>
        
      <a href="" class="project-tile">
        <img src="/images/home6.jpg" alt="sunset">
        <p>Opacarophile</p>
      </a>
      </div>
    </section>

    <div class="container-fluid purple-background whitespace" id="portfolio">
      <h2 class="text-center">Portfolio</h2>
      <p class="text-center">Who's Rhaemonette in a nutshell?</p>
    </div>

    <div class="container about special-top">
          <div class="row portfolio">
            <div class="col-md-7">
              <h2 class="portfolio-heading">Someone who always seeks for a great escape.</h2>
              <p class="lead">We all have that one friend who spends countless hours through the night or day trying to find peace in every place she goes to, only to realize hours later that you haven’t started any of your homework. I am that friend. It’s not procrastination or laziness. It’s a psychological concept known as escapism, which is habitual distraction to an imaginary world or entertainment to escape from reality. </p>
              
            </div>
            <div class="col-md-5">
              <img class="portfolio-image img-responsive center-block" src="/images/home1.jpg" alt="escape">
            </div>
          </div>

          <hr class="portfolio-divider">

          <div class="row portfolio">
            <div class="col-md-7 col-md-push-5">
             <h2 class="portfolio-heading">The bestest experience: being a furmom</h2>
              <p class="lead">Just like every mother around the world, being a fur mom comes with great responsibilities. It requires a lot of hard work to take care of the furry kids. But at the end of the day, seeing your furkids stay healthy and happy is one of the greatest joys of being a fur mom. Living with a dog or cat means you never have to be alone anymore. They follow you everywhere you go, in your bedroom, in the kitchen, or even inside the bathroom while you’re doing your business.  </p>
            </div>
            <div class="col-md-5 col-md-pull-7">
              <img class="portfolio-image img-responsive center-block" src="/images/home3.jpg" alt="furmom">
            </div>
          </div>

          <hr class="portfolio-divider">

          <div class="row portfolio">
            <div class="col-md-7">
              <h2 class="portfolio-heading">Always makes time to watch sunsets</h2>
              <p class="lead">Sunsets are romantic, inspiring and Instagram-able, yet most of us only make time for this special experience when we are on vacation. They don’t call it magic hour for no reason. Taking just a few minutes out of your day to experience some of the breath-taking moments of a sunset will be worth it - even if you have to go back into the office afterwards. Nature is natural fuel for the soul, and just 10 to 20 minutes of fresh air can make you feel refreshed and centered.</p>
            </div>
            <div class="col-md-5">
              <img class="portfolio-image img-responsive center-block" src="/images/home6.jpg" alt="sunset">
            </div>
          </div>
    </div>


    </body>
</html>
