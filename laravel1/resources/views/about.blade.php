<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="/images/icon.png" type="image/x-icon">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
        <script>
            const toggleForm = () => {
                const container = document.querySelector('.container');
                container.classList.toggle('active');
            };
            function myFunction() {
              var dots = document.getElementById("dots");
              var moreText = document.getElementById("more");
              var btnText = document.getElementById("myBtn");

              if (dots.style.display === "none") {
                dots.style.display = "inline";
                btnText.innerHTML = "Continue Reading"; 
                moreText.style.display = "none";
              } else {
                dots.style.display = "none";
                btnText.innerHTML = "Read Less"; 
                moreText.style.display = "inline";
              }
            }
        </script>

        <title>About Me</title>
        <link href="/css/a.css" rel="stylesheet" type="text/css">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>

        </style>
    </head>
    <body class="antialiased">
      
      <nav id="navbar" class="">
        <div class="nav-wrapper">
          <!-- Navbar Logo -->
          <div class="logo">
            <a href="homepage"><img class="logo" src="/images/logo.png" /></a>
          </div>

          <!-- Navbar Links -->
          <ul id="menu">
            <li><a href="homepage">Home</a></li><!--
         --><li><a href="about">About Me</a></li><!--
         --><li><a href="gallery">Contact Us/Gallery</a></li>
            <li><a href="registration">Login/Registration</a></li>

          </ul>
        </div>
      </nav>

      <!-- Menu Icon -->
      <div class="menuIcon">
        <span class="icon icon-bars"></span>
        <span class="icon icon-bars overlay"></span>
      </div>


      <div class="overlay-menu">
        <ul id="menu">
            <li><a href="#home">Home</a></li>
            <li><a href="#about">About Me</a></li>
            <li><a href="#congal">Contact Us/Gallery</a></li>
            <li><a href="#registration">Login/Registration</a></li>

          </ul>
      </div>

      <div class="container pt-5">
        <div class="page">
          <div class="row">
            <div class="col-md-5">
              <img src="/images/julay.png" class="img-responsive" alt="Profile Picture">
            </div>


            <div class="col-md-7">
              <h1 class="text-center"> ROSAL, <br/> July Anne Rhaemonette A. <br> <small> <!-- &amp; --> Aspiring Photographer </small></h1>

            </div>

          </div>

          <div class="row">
            <div class="col-md-7 vertical">
              <div id="intro">
                <h3> INTRODUCTION </h3>
                <p>Hi, I'm July Anne Rhaemonette and I am passionate about technology and it's innovations. I'm an aspiring graphic designer and photographer with a strong love for front-end development. I enjoy designing things, and I know being in tech would not only help me grow in
                  my career but enable me deliver wolrd class designs and portraits. </p>
              </div>

              <div id="exp">
                <h3> EXPERIENCE </h3>
                <h5><strong> ARCANYS, Graphic Designer </strong> <br/> <small> June 2026 – Present </small></h5>

                <p>I was in charge of outgoing and incoming mails, all available social media platforms, sorting, editing, organising of the current year’s corp members photo album that goes into the annual company's magazine and any other IT related issue.</p>

                <h5><strong> Freelance Web Developer </strong> <br/> <small> October 2023 – July 2024 </small></h5>
                <p> I developed websites with html and css (bootstrap framework), while still developing most of my skills.</p>


              </div>
              <div id="skills">
                <h3> SKILLS </h3>

                <div>
                  <div>
                    <p>HTML5</p>
                  </div>
                </div>

                <div>
                  <div>
                    <p>CSS3</p>
                  </div>
                </div>

                <div>
                  <div>
                    <p>BOOTSTRAP</p>
                  </div>
                </div>

                <div>
                  <div>
                    <p>MYSQL</p>
                  </div>
                </div>

              </div>
            </div>

            <div class="col-md-5">
              <div id="dets">
                <h5> Address: <small> Pozorrubio Pangasinan </small> </h5>

                <h5> Phone: <small> +63-945-755-2819  </small> </h5>

                <h5> Email: <small> jrosal_19ur0206@psu.edu.ph </small> </h5>

              </div>

              <div id="socials">
                <i class="fa-brands fa-twitter">  <a href="https://twitter.com/dewlayxiv" target="_blank">dewlayxiv</a></i><br><br>
                <i class="fa-brands fa-github">  <a href="https://github.com/takumi-Z" target="_blank">takumi-Z</a></i><br><br>
                <i class="fa-brands fa-instagram"> <a href="https://www.instagram.com/dewlayxiv/" target="_blank">@dewlayxiv</a></i>
              </div>

              <div id="edu">
                <h3> Education </h3>
                <p><strong> BS Information Technology </strong> </p>
                <small> Pangasinan State University - Urdaneta City Campus </small>
                <br><br>
                <p><strong> Humanities and Social Sciences </strong> </p>
                <small> Pangasinan State University - Urdaneta City Campus </small>

                <br><br>

              </div>


              <div id="ref">
                <h3> References </h3>

                <p> Available Upon Request </p>

              </div>

              <div id="ref">
                <h3> Download Resume </h3>

                <p> <a href="/images/digital_resume.pdf" download="myResume"> 
                  <img src="https://icon-library.com/images/free-pdf-icon/free-pdf-icon-14.jpg" alt="myResume" width="100" height="100"></a></p>

              </div>
            </div>
          </div>

        </div>

    </div>

</body>
</html>
