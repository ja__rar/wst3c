<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class UserController extends Controller
{
    public function addUserForm() {
    return view('register');
    }

    public function addUser(Request $request) {
    $this->validate($request,[
        'email'=>'required|email|unique:users,email',
        'username'=>'required|max:10|min:7|unique:users,username',
        'password'=>'required|max:10|min:7'
    ]);
    
    $email = $request->input('email');
    $username = $request->input('username');
    $password = $request->input('password');

    $data=array('email'=>$email,'username'=>$username,'password'=>$password);
        DB::table('users')->insert($data);
        echo "Record inserted successfully.<br/>";
        return redirect('/register')->with('message', "Account successfully registered.");         
    }

    public function loginForm() {
      return view('login');
    }

    public function validateForm(Request $request) {
      $this->validate($request,[
         'username'=>'required|max:10|min:7',
         'password'=>'required|max:10|min:7'
      ]);

        $username = $request->input('username');
        $password =$request->input('password');
        $date = Carbon::now();

        $adminUsername = DB::table('users')->where('username',$request->username)->first();
        $adminPassword = DB::table('users')->where('password',$password)->first();

        if($adminUsername && $adminPassword){
            $data = $request->input();
            $request->session()->put('username',$data['username']);
            
            return redirect('/item');

        }
        else{
         return view('login')->withErrors("Unsuccessful login. Sorry.");
      }
      
   }

   public function showItems() {
        $items_ = DB::select('select * from items');
        $items = $items_;
        return view('item',['items'=>$items]);
    }
}
