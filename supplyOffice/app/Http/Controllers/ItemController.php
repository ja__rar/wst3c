<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class ItemController extends Controller
{
    public function showItems() {
        $items_ = DB::select('select * from items');
        $items = $items_;
        return view('items',compact('items'));    
    }
}
