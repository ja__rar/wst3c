<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function adminLoginForm() {
      return view('admin_login');
    }

    public function adminValidateForm(Request $request) {
      $this->validate($request,[
         'username'=>'required|max:10|min:7',
         'password'=>'required|max:10|min:7'
      ]);

        $username = $request->input('username');
        $password =$request->input('password');
        $date = Carbon::now();

        $adminUsername = DB::table('admin')->where('username',$request->username)->first();
        $adminPassword = DB::table('admin')->where('password',$password)->first();

        if($adminUsername && $adminPassword){
            $data = $request->input();
            $request->session()->put('username',$data['username']);
            
            return redirect('/admin_home');

        }
        else{
         return view('login')->withErrors("Unsuccessful login. Sorry.");
      }
      
   }

   public function showUsers() {
        $users_ = DB::select('select * from users');
        $users = $users_;
        $items_ = DB::select('select * from items');
        $items = $items_;
        return view('admin_home',['users'=>$users],['items'=>$items]);
    }
}
