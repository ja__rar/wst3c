<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/register', function () {
    return view('register');
});

Route::get('/item', function () {
    return view('item');
});

Route::get('/admin', function () {
    return view('admin_login');
});

Route::get('register', [UserController::class, 'addUserForm' ]);
Route::post('register', [UserController::class, 'addUser' ]);


Route::get('/', [UserController::class, 'loginForm' ]);
Route::post('/', [UserController::class, 'validateForm' ]);

Route::get('/admin', [AdminController::class, 'adminLoginForm' ]);
Route::post('/admin', [AdminController::class, 'adminValidateForm' ]);

Route::get('/login', [UserController::class, 'loginForm' ]);
Route::post('/login', [UserController::class, 'validateForm' ]);

Route::get('/item', [UserController::class, 'showItems' ]);

Route::get('/admin_home', [AdminController::class, 'showUsers' ]);



