<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" href="/images/logo.png" type="image/x-icon">
	<title>Log In</title>

	<link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
	<link href="/css/admin_lp.css" rel="stylesheet" type="text/css">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>

	<style>
    	body {
    		font-family: 'Work Sans', sans-serif;
    		background-color: #EFEBE9;
        }
        .btn:hover {
            background-color: black;
            color: white;
        }
        .form button {
          text-transform: uppercase;
          outline: 0;
          background: maroon;
          width: 100%;
          border: 0;
          border-radius: 5px;
          padding: 15px;
          color: #FFFFFF;
          font-size: 14px;
          -webkit-transition: all 0.3 ease;
          transition: all 0.3 ease;
          cursor: pointer;
        }
    </style>

    <script>
        $(function() {
            var timeout = 3000; // in miliseconds (3*1000)
            $('.alert').delay(timeout).fadeOut(300);
        });
        function date_time(id) {
            date = new Date;
            year = date.getFullYear();
            month = date.getMonth();
            months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
            d = date.getDate();
            day = date.getDay();
            days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
            h = date.getHours();
            if (h < 10) {
                h = "0" + h;
            }
            m = date.getMinutes();
            if (m < 10) {
                m = "0" + m;
            }
            s = date.getSeconds();
            if (s < 10) {
                s = "0" + s;
            }
            result = ' ' + months[month] + ' ' + d + '' + ', ' + year + ' | ' + days[day] + ' &nbsp; &nbsp;&nbsp; ' + h + ' : ' + m + ' : ' + s;
            document.getElementById(id).innerHTML = result;
            setTimeout('date_time("' + id + '");', '1000');
            return true;
        }
    </script>

</head>
<body>
		<div class="login">
			@if (count($errors) > 0)
                     <div class="alert alert-danger">
                        <ul>
                           @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                           @endforeach
                        </ul>
                     </div>
            @endif
			<div class="form">
				<form action = "" method = "post">
				  {{ csrf_field() }}
                  <div class="form-row">
                  	<span class=""><i class="fa-solid fa-lock"></i></span>
                    <div class="col m-4">
                      <input type="text"  name="username" class="form-control" placeholder="Username">
                    </div>
                    <div class="col mb-4 m-4">
                      <input type="password" name="password" class="form-control" placeholder="Password">
                    </div>
                  </div>
                  <div class="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
                    <button type="submit" class="btn btn-primary btn-sm mb-4">Admin Log In</button>
                  </div>
                </form>
			</div>
		</div>
	<div class = "container mx-auto m-4 p-4">
		<div class="row">
			<div class ="col-md-7 p-4">

				<div class ="col-12 col-md-5 p-4">
				</div>	
			</div>
		</div>
	</div>

	<br>

</body>
</html>