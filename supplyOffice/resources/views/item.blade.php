<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" href="/images/logo.png" type="image/x-icon">
	<title>Items</title>

	<link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
	<link href="/css/admin_lp.css" rel="stylesheet" type="text/css">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>

	<style>
    	body {
    		font-family: 'Work Sans', sans-serif;
    		background-color: #EFEBE9;
        }
        .btn:hover {
            background-color: black;
            color: white;
        }
        .form button {
          text-transform: uppercase;
          outline: 0;
          background: maroon;
          width: 100%;
          border: 0;
          border-radius: 5px;
          padding: 15px;
          color: #FFFFFF;
          font-size: 14px;
          -webkit-transition: all 0.3 ease;
          transition: all 0.3 ease;
          cursor: pointer;
        }
    </style>

</head>
<body>
    <section class="py-5">
            <div class="container px-4 px-lg-5 mt-5">
                <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
                    @foreach($items as $item)
                    <div class="col mb-5">
                        <div class="card h-100">
                            <!-- Product image-->
                            <img class="card-img-top" src="items/1.jpg" alt="..." />
                            <!-- Product details-->
                            <div class="card-body p-4">
                                <div class="text-center">
                                    <!-- Product name-->
                                    <b>{{$item->name}}</b>
                                    <h5 class="fw-bolder"></h5>
                                    {{$item->price}}
                                  
                                </div>
                            </div>
                            <!-- Product actions-->
                            <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                <div class="text-center">
                                    @if($item->stocks == 0)
                                        <a class="btn btn-outline-dark mt-auto disabled" href="#">Out of Stock</a>
                                    @endif
                                    @if($item->stocks != 0)
                                        <a class="btn btn-outline-dark mt-auto" href="request/{{$item->id}}">Request</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                   
                </div>
            </div>
        </section>

</body>
</html>