<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
	<title>Admin</title>

	<script type="text/javascript">
        $(document).ready(function() {
            $('#table1').DataTable();
        } );
        $(document).ready(function() {
            $('#table2').DataTable();
        } );
	</script>
</head>
<body>

	<div class="container m-4">
		<h5>List of Users<h5>
<table class="table table-dark table-striped mt-4" id="table1">
                                      <thead>
                                        <tr>
                                          <th scope="col">User ID</th>
                                          <th scope="col">Username</th>
                                          <th scope="col">Email</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach ($users as $user)
                                        <tr>

                                          <th scope="row">{{ $user->user_id }}</th>
                                          <td>{{ $user->username }}</td>
                                          <td>{{ $user->email }}</td>
                                        </tr>
                                        @endforeach
                                      </tbody>
</table>

<h5>List of Items<h5>

<table class="table table-dark table-striped mt-4" id="table2">
                                      <thead>
                                        <tr>
                                          <th scope="col">Item ID</th>
                                          <th scope="col">Item Name</th>
                                          <th scope="col">Item Price</th>
                                          <th scope="col">Item Stock</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach ($items as $item)
                                        <tr>

                                          <th scope="row">{{ $item->id }}</th>
                                          <td>{{ $item->name }}</td>
                                          <td>{{ $item->price }}</td>
                                          <td>{{ $item->stocks }}</td>
                                        </tr>
                                        @endforeach
                                      </tbody>
</table>

	</div>


</body>
</html>