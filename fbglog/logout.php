<?php
    require_once 'config.php';
    require_once 'gconfig.php';

    unset($_SESSION['access_token']);
    $gClient->revokeToken();
    session_destroy();
    header ('Location: index.php');
    exit();

    unset($_SESSION['facebook_access_token']);

    unset($_SESSION['fb_user_id']);
    unset($_SESSION['fb_user_name']);
    unset($_SESSION['fb_user_email']);
    unset($_SESSION['fb_user_pic']);


    header("Location:index.php");
?>
