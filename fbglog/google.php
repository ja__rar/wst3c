<?php
	session_start();

	if(!isset($_SESSION['access_token'])){
		header ('Location: index.php');
		exit();
	}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

	<style>
		body {
	    	background-color: #EDE7F6;
	    }

		.container{
			background-color: #D1C4E9;
		}

		.info{
			background-color: #F3E5F5;
		}

		.login{
			background-color: #FBE9E7;
		}
	</style>

</head>
<body>

	<?php
		date_default_timezone_set('Singapore');
		$date = date('F j, Y');
		$time = date('g:i A');
		$name = $_SESSION['name'];
		$email = $_SESSION['email'];
		$photo = $_SESSION['picture'];
	?>
	<div class = "container mx-auto w-50 p-4 mt-5">
		<center>
			<h4>Elective 1 (Web Systems and Technologies 2)</h4>
			<h5>Login FB / Google API Script</h5>
		</center>
			<div class = "info container mx-auto w-100 p-4 mt-3">
				<div class = "row">
					<div class = "col-6">
						<p>
					  		<b>Name:</b> <?php echo $name; ?> <br>
					  		<b>Email:</b> <?php echo $email; ?><br>
					  	</p>
					</div>
					<div class = "col-6 text-end">
						<p>
							<?php echo $date; ?> <br>
			  				<?php echo $time; ?>
						</p>
					</div>
					<div class = "home row mx-auto w-100 p-4 mt-3">
						<div class = "col-12">
							<center>
								<img class="mx-auto" src="<?php echo $photo;?>"><br>
								<img class="mx-auto" src="https://houseofaestheticsandbeauty.com/wp-content/uploads/2020/05/welcome-1.png">
							</center>
						</div>
						<div class = "text-end mt-4">
							<center>
								<div class = "text-end mt-4">
									
										<a class="btn btn-danger" href="logout.php" role="button">Logout</a>
								</div>		
						</div>		
					</div>
				</div>
			</div>
	</div>

</body>
</html>
