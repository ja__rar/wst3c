<?php

	require_once 'config.php';
	require_once 'gconfig.php';
	
	$gloginURL = $gClient->createAuthUrl();
	
	if(isset($_SESSION['access_token'])){
		header ('Location: google.php');
		exit();
	}

//FACEBOOK
	$permissions = ['email']; //optional
		if (isset($accessToken)){
			if (!isset($_SESSION['facebook_access_token'])) {
				//get short-lived access token
				$_SESSION['facebook_access_token'] = (string) $accessToken;
				
				//OAuth 2.0 client handler
				$oAuth2Client = $fb->getOAuth2Client();
				
				//Exchanges a short-lived access token for a long-lived one
				$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
				$_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
				
				//setting default access token to be used in script
				$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
			}

			else{
				$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
			}
			
		//redirect the user to the index page if it has $_GET['code']
			if (isset($_GET['code'])){
				header('Location: ./');
			}
		
		
			try {
				$fb_response = $fb->get('/me?fields=name,first_name,last_name,email');
				$fb_response_picture = $fb->get('/me/picture?redirect=false&height=200');
				
				$fb_user = $fb_response->getGraphUser();
				$picture = $fb_response_picture->getGraphUser();
				
				$_SESSION['fb_user_id'] = $fb_user->getProperty('id');
				$_SESSION['fb_user_name'] = $fb_user->getProperty('name');
				$_SESSION['fb_user_email'] = $fb_user->getProperty('email');
				$_SESSION['fb_user_pic'] = $picture['url'];
				
				
			} catch(Facebook\Exceptions\FacebookResponseException $e) {
				echo 'Facebook API Error: ' . $e->getMessage();
				session_destroy();
				// redirecting user back to app login page
				header("Location: ./");
				exit;
			} catch(Facebook\Exceptions\FacebookSDKException $e) {
				echo 'Facebook SDK Error: ' . $e->getMessage();
				exit;
			}
		} 
	
	else {	
		// replace your website URL same as added in the developers.Facebook.com/apps e.g. if you used http instead of https and you used
		$fb_login_url = $fb_helper->getLoginUrl('http://localhost/fbglog/', $permissions);
	}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login with Facebook and Google</title>
  <meta charset="utf-8">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

	<style>
		body {
	    	background-color: #EDE7F6;
	    }

		.container{
			background-color: #D1C4E9;
		}

		.info{
			background-color: #F3E5F5;
		}

		.login{
			background-color: #FBE9E7;
		}
	</style>
  
</head>
<body>
<?php if(isset($_SESSION['fb_user_id'])){
		date_default_timezone_set('Singapore');
		$date = date('F j, Y');
		$time = date('g:i A');
	?>
	<div class = "container mx-auto w-50 p-4 mt-5">
		<center>
			<h4>Elective 1 (Web Systems and Technologies 2)</h4>
			<h5>NILOG IN MO YONG FACEBOOK</h5>
		</center>
			<div class = "info container mx-auto w-100 p-4 mt-3">
				<div class = "row">
					<div class = "col-6">
						<p>
					  		<b>Facebook Name:</b> <?php echo $_SESSION['fb_user_name']; ?> <br>
					  		<b>Email:</b> <?php echo $_SESSION['fb_user_email']; ?><br>
					  	</p>
					</div>
					<div class = "col-6 text-end">
						<p>
							<?php echo $date; ?> <br>
			  				<?php echo $time; ?>
						</p>
					</div>
					<div class = "home row mx-auto w-100 p-4">
						<div class = "col-12">
							<center>
								<img class="mx-auto" src="<?php echo  $_SESSION['fb_user_pic']; ?>"><br>
								<img class="mx-auto" src="https://houseofaestheticsandbeauty.com/wp-content/uploads/2020/05/welcome-1.png">
							</center>
						</div>
						<div class = "text-end mt-4">
							<center>
								<div class = "text-end mt-4">
									
										<a class="btn btn-danger" href="logout.php" role="button">Logout</a>
								</div>		
						</div>		
					</div>
				</div>
			</div>
	</div>

<?php 
	}

//LOGIN
	else{
	?>
	<?php
		date_default_timezone_set('Singapore');
		$date = date('F j, Y');
		$time = date('g:i A');
		$myName = "ROSAL, July Anne Rhaemonette A.";
		$secYear = "Third Year - Section C";
	?>
	<div class = "container mx-auto w-50 p-4 mt-5">
		<center>
			<h4>Elective 1 (Web Systems and Technologies 2)</h4>
			<h5>Login FB / Google API Script</h5>
		</center>
			<div class = "info container mx-auto w-100 p-4 mt-3">
				<div class = "row">
					<div class = "col-6">
						<p>
					  		<b>Name:</b> <?php echo $myName; ?> <br>
					  		<b>Year and Section:</b> <?php echo $secYear; ?><br>
					  	</p>
					</div>
					<div class = "col-6 text-end">
						<p>
							<?php echo $date; ?> <br>
			  				<?php echo $time; ?>
						</p>
					</div>
					<div class = "login row mx-auto w-100 p-4 mt-3">
						<div class = "col-6 social-btn">
							<center>
								<button type="button" onclick="window.location='<?php echo $fb_login_url;?>';" class="btn btn-primary btn-lg"><i class="fa-brands fa-facebook-f"></i>&nbsp;&nbsp;&nbsp;Login with <b>Facebook</b></button>
								
							</center>
						</div>
						<div class = "col-6">
							<center>
								<button type="button" onclick="window.location='<?php echo $gloginURL ?>';" class="btn btn-danger btn-lg"><i class="fa-brands fa-google-plus-g"></i>&nbsp;&nbsp;&nbsp;Sign In with <b>Google+</b></button>
							</center>
						</div>
					</div>
				</div>
			</div>
	</div>
	<?php
}
?>
     
</body>
</html>