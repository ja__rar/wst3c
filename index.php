<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

	<style>
		body {
	    	background-color: #D7CCC8;
	    }

		.container{
			background-color: #BCAAA4;
		}
	</style>

</head>
<body>

	<?php
		date_default_timezone_set('Singapore');
		$date = date('Y-m-d');
		$time = date('H:i:s');
	?>
	<div class = "container mx-auto w-25 p-4 mt-5">
		<center>
	  		<h5>ROSAL, July Anne Rhaemonette A.</h5>
	  		<p>
	  			Third Year - Section C<br>
	  			Elective 1 (Web Systems and Technologies 2)<br><br>
	  			<b>CURRENT DATE:</b> <?php echo $date; ?> <br>
	  			<b>CURRENT TIME:</b> <?php echo $time; ?>
	  		</p>
	  	</center>
	</div>

</body>
</html>
