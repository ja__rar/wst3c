<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\ValidationController;
use App\Http\Controllers\StudInsertController;
use App\Http\Controllers\StudViewController;
use App\Http\Controllers\StudUpdateController;
use App\Http\Controllers\StudDeleteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('stud_create');
});*/

// Route::get('/contact', function(){  
//     return view('Contact',['name'=>'John']);    
// });

// Route::get('/about',[PostController::class, 'display' ]);  

// Route::get('/studentdetails',[StudentController::class, 'display' ]);

// Route::get('/studentdetails/{id}',[StudentController::class, 'display' ]);

// Route::get('/details', [StudentController::class, 'display' ]);

// Route::get('/details/{i}', 'StudentController@display');   

// Route::get('/details/{i}', [StudentController::class, 'display' ]);



Route::get('/validation', [ValidationController::class, 'showform' ]);
Route::post('/validation', [ValidationController::class, 'validateform' ]);

/*Route::get('/validation','ValidationController@showform');
Route::post('/validation','ValidationController@validateform');*/

Route::get('insert', [StudInsertController::class, 'insertform' ]);
Route::post('create', [StudInsertController::class, 'insert' ]);
Route::get('view-records', [StudViewController::class, 'index' ]);
Route::get('edit-records',[StudUpdateController::class, 'index']);
Route::get('edit/{id}',[StudUpdateController::class, 'show']);
Route::post('edit/{id}',[StudUpdateController::class, 'edit']);
Route::get('delete-records',[StudDeleteController::class, 'index']);
Route::get('delete/{id}',[StudDeleteController::class, 'destroy']);