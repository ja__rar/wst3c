**First Laravel Blade Code**

There are two php files that can be seen in this folder:
1. web.php
2. activity1.blade.php

Under your Laravel project folder, you have a folder named "routes".
Open it and replace the web.php file in your folder with the one that is seen in this folder.

Also, in your Laravel project folder, you have a folder named "resources".
Once you opened it, there are three subfolders:
1. css
2. js
3. views

Open the views folder then add the activity1.blade.php.

From there, you can now visit the activity1/ as one of your routes.