**Activity Laravel #1**
There are two subfolders that can be seen in this folder:
1. css
2. images

Under your Laravel project folder, go to the folder named "public". Then,
transfer the abovementioned folders.

Additionally, there are five (5) php files which is also seen in this folder:
1. web.php
2. about.blade.php
3. gallery.blade.php
4. homepage.blade.php
5. registration.blade.php

Again, under your Laravel project folder, you have a folder named "routes".
Open it and replace the web.php file in your folder with the one that is seen in this folder.

Also, in your Laravel project folder, you have a folder named "resources".
Once you opened it, there are three subfolders:
1. css
2. js
3. views

Open the views folder then add the about.blade.php, gallery.blade.php, 
homepage.blade.php, and registration.blade.php.

From there, you can now visit the registration/ as one of your routes. It also supports
login/, homepage/, about/, and gallery/ routes. 