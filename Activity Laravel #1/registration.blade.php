<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="/images/icon.png" type="image/x-icon">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
        <script>
            const toggleForm = () => {
                const container = document.querySelector('.container');
                container.classList.toggle('active');
            };
        </script>

        <title>Registration</title>
        <link href="/css/reg.css" rel="stylesheet" type="text/css">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
        <nav id="navbar" class="">
          <div class="nav-wrapper">
            <!-- Navbar Logo -->
            <div class="logo">
              <center><a href="homepage"><img class="logo" src="/images/logo.png" /></a></center>
            </div>
          </div>
        </nav>
        <section>
            <div class="container">
              <div class="user signinBx">
                <div class="imgBx"><img src="https://images.unsplash.com/photo-1629757740928-b30b24102c81?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=504&q=80" alt="" /></div>
                <div class="formBx">
                  <form action="homepage" onsubmit="return true;">
                    <h2>Log In</h2>
                    <input type="text" name="" placeholder="Username" required />
                    <input type="password" name="" placeholder="Password" required />
                    <input type="submit" name="" value="Login" />
                    <p class="signup">
                      Don't have an account ?
                      <a href="#" onclick="toggleForm();">Register.</a>
                    </p>
                  </form>
                </div>
              </div>
              <div class="user signupBx">
                <div class="formBx">
                  <form action="homepage" onsubmit="return true;">
                    <h2>Registration</h2>
                    <p class="reg">Register to the Rhaemonette's e-news to receive updates, sugar, wine, and everything fine. You can unsubsribe at any time, and we will always protect the information you provide us with.</p>
                    <input type="text" name="" placeholder="Username" required/>
                    <input type="email" name="" placeholder="Email Address" required/>
                    <input type="password" name="" placeholder="Create Password" required/>
                    <input type="password" name="" placeholder="Confirm Password" required/>
                    <input type="submit" name="" value="Register" />
                    <p class="signup">
                      Already have an account ?
                      <a href="#" onclick="toggleForm();">Log In.</a>
                    </p>
                  </form>
                </div>
                <div class="imgBx"><img src="https://images.unsplash.com/photo-1647175352672-b09fa2cd097f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=435&q=80" alt="" /></div>
              </div>
            </div>
        </section>
    </body>
</html>
