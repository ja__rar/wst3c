-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 18, 2022 at 03:54 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `acms_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `accomplishments`
--

CREATE TABLE `accomplishments` (
  `id` int(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `image` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `accomplishments`
--

INSERT INTO `accomplishments` (`id`, `title`, `date`, `image`) VALUES
(1, 'Silew-Silew ed Brgy. Amagbagan 2021', '2021-12-05', '12.jpg'),
(2, 'Safety Seal 2021', '2021-07-26', '10.jpg'),
(3, 'Barangay Visitation and Implementation of IATF Protocols', '2021-09-10', '9.jpg'),
(4, 'Swivel Chairs for the Sangguniang Barangay', '2021-10-23', '13.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `account_created` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `account_created`, `last_login`) VALUES
(10, 'admin4_kgd', '4a0cb551199fbbf69af48bedfff0b219', '2022-06-09 19:41:46', '2022-06-09 19:41:46'),
(11, 'admin5_kgd', '51720ce4a8e43c1ff323e82da98c98af', '2022-06-09 19:44:37', '2022-06-10 04:43:12'),
(12, 'admin3_sk', 'a1a8cc5323428e71486af9595ad7dcc2', '2022-06-09 20:05:53', '2022-06-10 10:47:22'),
(13, 'admin1_kap', '416d9359acd0562d0ef964a8f7fa4a39', '2022-06-09 20:06:09', '2022-06-17 10:13:06'),
(14, 'admin2_kgd', 'bc4ee3f1555ae329a7342f5653ce2f76', '2022-06-09 20:06:22', '2022-06-10 04:43:39');

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `album_id` int(11) NOT NULL,
  `album_title` varchar(50) NOT NULL,
  `album_cover` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`album_id`, `album_title`, `album_cover`, `date_created`) VALUES
(2, 'Clean-Up Drive', '20220616082818.jpeg', '2022-06-16 00:28:18'),
(3, 'Sports', '20220616082929.jpg', '2022-06-16 00:29:29'),
(4, 'Feeding Program', '20220616083004.jpg', '2022-06-16 00:30:04'),
(5, 'Fiesta', '20220616083144.jpg', '2022-06-16 00:31:44'),
(6, 'COVID-19 Operations', '20220616083223.jpg', '2022-06-16 00:32:23'),
(7, 'Assemblies', '20220616083256.jpg', '2022-06-16 00:32:56'),
(8, 'Others', '20220616083306.jpeg', '2022-06-16 00:33:06'),
(9, 'Brgy Elections', '20220617090027.jpg', '2022-06-17 01:00:27');

-- --------------------------------------------------------

--
-- Table structure for table `announce_card`
--

CREATE TABLE `announce_card` (
  `id` int(20) NOT NULL,
  `source` varchar(100) NOT NULL,
  `source_url` varchar(200) DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  `details` varchar(500) NOT NULL,
  `date_posted` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `announce_card`
--

INSERT INTO `announce_card` (`id`, `source`, `source_url`, `title`, `details`, `date_posted`) VALUES
(1, 'Pozorrubio Agriculture Office', 'https://www.facebook.com/omagpozorrubio', 'Young Farmers Challenge 2022', 'Ikaw na ba ang susunod na Young Farmers Challenge awardee? The Young Farmers Challenge program is a competitive financial grant assistance program for the youth who will engage in new agri-fishery enterprises.  The Program is open to all interested youth with proposed agri-fishery enterprises with profitability potential. The grants shall serve as start-up capital for the planned agri-fishery enterprise operated by an individual youth or as a group-managed venture. Goodluck Young Farmers!', '2022-05-05 09:05:51'),
(2, 'SK - Pozorrubio', 'https://www.facebook.com/SKPozorrubio2018', 'International Youth Day 2021', 'Community Basura palit Pagkain sa Mesa. The Sangguniang Kabataan Municipal Federation of Pozorrubio will buy locally produced products of our kabaleyan and will give it to the indigent youth in exchange of recyclable waste. The interested individuals will be evaluated and verified by their respective SK Chairperson if they are YWD, OSY, PoPY and not a member of 4P’s.', '2021-08-12 09:10:08'),
(3, 'SK - Pozorrubio', 'https://www.facebook.com/SKPozorrubio2018', 'Provincial Ordinance No. 263-2021', 'An ordinance amending the curfew ordinance of the province of Pangasinan by imposing curfew from eight o\'clock in the evening (8:00 pm) to four o\'clock in the morning (4:00 am) pursuant to executive order no. 0030-2021 of the Provincial Governor, Amado I. Espino III, and providing a supplementary provision thereto.', '2022-05-05 09:10:08');

-- --------------------------------------------------------

--
-- Table structure for table `announce_photo`
--

CREATE TABLE `announce_photo` (
  `id` int(20) NOT NULL,
  `title` varchar(50) NOT NULL,
  `photo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `announce_photo`
--

INSERT INTO `announce_photo` (`id`, `title`, `photo`) VALUES
(3, 'Animal Branding and Vaccination', '20220607045208.png'),
(4, 'COVID-19 Vaccination', '8.png');

-- --------------------------------------------------------

--
-- Table structure for table `carousel`
--

CREATE TABLE `carousel` (
  `id` int(20) NOT NULL,
  `image` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `carousel`
--

INSERT INTO `carousel` (`id`, `image`) VALUES
(1, '20220617194436.png'),
(2, '2.png'),
(3, '3.png');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `album_id` int(11) NOT NULL,
  `date_uploaded` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `image`, `album_id`, `date_uploaded`) VALUES
(2, '20220616090829.jpg', 3, '2022-06-16 01:08:29'),
(3, '20220616090842.jpg', 3, '2022-06-16 01:08:42'),
(4, '20220616091329.jpg', 8, '2022-06-16 01:13:29'),
(5, '20220616091352.jpg', 8, '2022-06-16 01:13:52'),
(8, '20220616092144.jpg', 7, '2022-06-16 01:21:44'),
(9, '20220616092154.jpg', 7, '2022-06-16 01:21:54'),
(10, '20220616095107.jpg', 8, '2022-06-16 01:51:07'),
(11, '20220616095114.jpg', 8, '2022-06-16 01:51:14'),
(12, '20220616185904.jpg', 8, '2022-06-16 10:59:04'),
(13, '20220616185929.jpg', 8, '2022-06-16 10:59:29'),
(14, '20220616185951.jpg', 8, '2022-06-16 10:59:51'),
(15, '20220616190327.jpg', 8, '2022-06-16 11:03:27'),
(16, '20220616190331.jpg', 8, '2022-06-16 11:03:31'),
(19, '20220616190740.jpg', 7, '2022-06-16 11:07:40'),
(20, '20220616190752.jpg', 7, '2022-06-16 11:07:52'),
(21, '20220616190803.jpg', 7, '2022-06-16 11:08:03'),
(22, '20220616190816.jpg', 7, '2022-06-16 11:08:16'),
(23, '20220616190829.jpg', 7, '2022-06-16 11:08:29'),
(24, '20220616190906.jpg', 7, '2022-06-16 11:09:06'),
(25, '20220616190919.jpg', 7, '2022-06-16 11:09:19'),
(26, '20220616191001.jpg', 2, '2022-06-16 11:10:01'),
(27, '20220616191019.jpeg', 2, '2022-06-16 11:10:19'),
(28, '20220616191112.jpg', 4, '2022-06-16 11:11:12'),
(29, '20220616191138.jpg', 4, '2022-06-16 11:11:38'),
(30, '20220616191147.jpg', 4, '2022-06-16 11:11:47'),
(31, '20220616191203.jpg', 4, '2022-06-16 11:12:03'),
(32, '20220616191210.jpg', 4, '2022-06-16 11:12:10'),
(33, '20220616191219.jpg', 4, '2022-06-16 11:12:19'),
(34, '20220616191232.jpg', 4, '2022-06-16 11:12:32'),
(35, '20220616191320.jpg', 3, '2022-06-16 11:13:20'),
(36, '20220616191329.jpg', 3, '2022-06-16 11:13:29'),
(37, '20220616191346.jpg', 3, '2022-06-16 11:13:46'),
(38, '20220616191357.jpg', 3, '2022-06-16 11:13:57'),
(39, '20220616191418.jpeg', 6, '2022-06-16 11:14:18'),
(40, '20220616191435.jpg', 6, '2022-06-16 11:14:35'),
(41, '20220616191440.jpg', 6, '2022-06-16 11:14:40'),
(42, '20220616191446.jpg', 6, '2022-06-16 11:14:46'),
(43, '20220616191456.jpeg', 6, '2022-06-16 11:14:56'),
(44, '20220616191522.jpg', 5, '2022-06-16 11:15:22'),
(45, '20220616191530.jpg', 5, '2022-06-16 11:15:30'),
(46, '20220616191536.jpg', 5, '2022-06-16 11:15:36'),
(47, '20220616191547.jpeg', 5, '2022-06-16 11:15:47'),
(48, '20220616191558.jpg', 5, '2022-06-16 11:15:58'),
(49, '20220616191607.jpg', 5, '2022-06-16 11:16:07'),
(50, '20220616191615.jpg', 5, '2022-06-16 11:16:15');

-- --------------------------------------------------------

--
-- Table structure for table `health_advisory`
--

CREATE TABLE `health_advisory` (
  `id` int(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `subtitle` text NOT NULL,
  `image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `health_advisory`
--

INSERT INTO `health_advisory` (`id`, `title`, `subtitle`, `image`) VALUES
(2, 'Paano nakahahawa ang coronavirus?', 'Ito ay naipapasa ng tao-sa-tao sa pamamagitan ng pagsagap ng mga malilit na talsik ng laway mula sa pagsasalita, pagbahing, o pag-ubo ng isang taong may COVID-19. Ito ay karaniwang nangyayari sa mga taong may malapitang pakikisalamuha sa may sakit – tulad ng mga kapamilya at healthcare workers kaya mariing pinapayo na panatilihin ang isang metrong layo sa mga taong may sintomas tulad ng pag-ubo at pagbahing.', '5.jpg'),
(4, 'WHY VACCINATE? WHY BAKUNATION?', 'With the availability of COVID-19 vaccines which can (1) prevent symptomatic infection and possibly (2) prevent severe infection and (3) prevent transmission, we have the opportunity to get ahead of the virus.', '20220608130739.png'),
(5, 'How do vaccines differ?', 'Vaccines differ in their composition and how they trigger the immune response to create antibodies. These antibodies protect the body from microorganisms and serve as protection once a person gets infected with disease.', '20220608131435.jpg'),
(6, 'Is vaccination mandatory?', 'No, vaccination is not mandatory. But the government highly encourages the public to get vaccinated and be protected against preventable disease.', '20220608131631.jpg'),
(7, 'Can I purchase the vaccine from private clinics or pharmacies?', 'No, you cannot purchase COVID-19 vaccines from private clinics or pharmacies. At present, only the government is duly authorized to procure and administer vaccines.', '20220608131906.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `hotlines`
--

CREATE TABLE `hotlines` (
  `id` int(20) NOT NULL,
  `logo` varchar(50) NOT NULL,
  `office` varchar(100) NOT NULL,
  `smart` varchar(15) NOT NULL,
  `globe` varchar(15) NOT NULL,
  `landline` varchar(20) NOT NULL,
  `last_updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hotlines`
--

INSERT INTO `hotlines` (`id`, `logo`, `office`, `smart`, `globe`, `landline`, `last_updated`) VALUES
(1, '20220610201722.png', 'Mayor\'s Office', 'Unavailable', 'Unavailable', '(075)-632-1305', '2022-06-10 12:17:22'),
(3, '20220610203004.jpg', 'PNP Pozorrubio', '09985985131', '09152533140', '(075)-632-1495', '2022-06-10 12:30:04'),
(4, '20220610203427.jpg', 'BFP Pozorrubio', 'Unavailable', '09171869611', '(075)-632-1499', '2022-06-10 12:34:27'),
(5, '20220610203527.jpg', 'MDRRMO', 'Unavailable', '09271603900', '(075)-632-7328', '2022-06-10 12:35:27'),
(6, '20220610203714.jpg', 'RHU Pozorrubio', 'Unavailable', 'Unavailable', '(075)-632-1502', '2022-06-10 12:37:14'),
(7, '20220610203928.jpg', 'Pozorrubio Community Hospital', 'Unavailable', 'Unavailable', '(075)-632-1305', '2022-06-10 12:39:28'),
(8, '20220610204104.png', 'PANELCO III', '09431452320', '09551627517', '(075)-562-3562', '2022-06-10 12:41:04'),
(9, '20220610204307.png', 'Brgy. Amagbagan', 'Unavailable', '09166887755', 'Unavailable', '2022-06-10 12:43:07');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `sender` varchar(100) NOT NULL,
  `contact_number` varchar(15) DEFAULT NULL,
  `message` varchar(1000) NOT NULL,
  `date_posted` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `seen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `sender`, `contact_number`, `message`, `date_posted`, `seen`) VALUES
(10, 'Julalay', '09457552819', 'Kailan po ang vaccination sa brgy hall?', '2022-06-17 10:41:27', 1),
(14, 'Julalay', '09457552819', 'Test', '2022-06-17 11:26:11', 1),
(15, 'Sheila', '09457552819', 'Test', '2022-06-17 11:43:28', 0);

-- --------------------------------------------------------

--
-- Table structure for table `officials`
--

CREATE TABLE `officials` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `position` varchar(75) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `image` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `officials`
--

INSERT INTO `officials` (`id`, `name`, `position`, `phone`, `image`) VALUES
(1, 'Rommel Gamboa Erpelo', 'Punong Barangay(Chairperson-Captain)', '09654903705', 'kap.jpg'),
(2, 'Leo Bambilla Florida', 'Sangguniang Barangay Member(Kagawad)', '09914844755', 'kgd1.jpg'),
(3, 'Lydia Briones Madres', 'Sangguniang Barangay Member(Kagawad)', 'N/A', 'kgd3.jpg'),
(4, 'Arsenia Datario Ortiz', 'Sangguniang Barangay Member(Kagawad)', 'N/A', 'kgd2.jpg'),
(5, 'Violeta Velasquez Songcuan', 'Sangguniang Barangay Member(Kagawad)', '09367576283', 'kgd4.jpg'),
(6, 'Cipriano Moina Abrigo Jr.', 'Sangguniang Barangay Member(Kagawad)', 'N/A', 'kgd5.jpg'),
(7, 'Carlos Paredes Songcuan', 'Sangguniang Barangay Member(Kagawad)', 'N/A', 'kgd6.jpg'),
(8, 'Ruperto Duran Lopez', 'Sangguniang Barangay Member(Kagawad)', 'N/A', 'kgd7.jpg'),
(9, 'Haidee Mabalot Tobias', 'SK Chairperson', '09261353186', 'sk.jpg'),
(10, 'Daisy Sibucao Barrozo', 'Barangay Secretary', 'N/A', 'sec.jpg'),
(11, 'Edwin Abuzo', 'Barangay Treasurer', 'N/A', 'treas.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `officials_group`
--

CREATE TABLE `officials_group` (
  `id` int(11) NOT NULL,
  `image` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `officials_group`
--

INSERT INTO `officials_group` (`id`, `image`) VALUES
(1, '20220607151732.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `quick_stats`
--

CREATE TABLE `quick_stats` (
  `id` int(20) NOT NULL,
  `place` varchar(20) NOT NULL,
  `population` varchar(20) NOT NULL,
  `year` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quick_stats`
--

INSERT INTO `quick_stats` (`id`, `place`, `population`, `year`) VALUES
(1, 'Pangasinan', '3,163,190', 2020),
(2, 'Pozorrubio', '74,729', 2020),
(3, 'Amagbagan', '2,657', 2020);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accomplishments`
--
ALTER TABLE `accomplishments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`album_id`);

--
-- Indexes for table `announce_card`
--
ALTER TABLE `announce_card`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announce_photo`
--
ALTER TABLE `announce_photo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carousel`
--
ALTER TABLE `carousel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `health_advisory`
--
ALTER TABLE `health_advisory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotlines`
--
ALTER TABLE `hotlines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `officials`
--
ALTER TABLE `officials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `officials_group`
--
ALTER TABLE `officials_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quick_stats`
--
ALTER TABLE `quick_stats`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accomplishments`
--
ALTER TABLE `accomplishments`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `album_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `announce_card`
--
ALTER TABLE `announce_card`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `announce_photo`
--
ALTER TABLE `announce_photo`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `carousel`
--
ALTER TABLE `carousel`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `health_advisory`
--
ALTER TABLE `health_advisory`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `hotlines`
--
ALTER TABLE `hotlines`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `officials`
--
ALTER TABLE `officials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `officials_group`
--
ALTER TABLE `officials_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `quick_stats`
--
ALTER TABLE `quick_stats`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
