<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class GalleryController extends Controller
{
    public function addAlbumForm() {
        if(session()->has('username')){
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();
            return view('add_album',['count'=>$count]);
        }
        else return redirect('/admin');
    }

    public function addAlbum(Request $request) {
    $request->validate([
        'title' => 'required',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5000',
    ]);

    $image = $request->input('image');
    $title = $request->input('title');
    $date = Carbon::now();

    if ($image = $request->file('image')) {
            $destinationPath = 'images/';
            $coverImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $coverImage);
            $input['image'] = "$coverImage";
        }

    DB::table('albums')->insert([
    'album_title' => $title,
    'album_cover' => $coverImage,
    'date_created' => $date,
    ]);
    return redirect()->back()->with('message', 'Added an album successfully!');
    }

    public function indexAlbum($id) {
        if(session()->has('username')){
            $photos_ = DB::table('gallery')->where('album_id',$id)->orderBy('date_uploaded', 'desc')->paginate(6);
            $unseen = 0;
            $count_= DB::table('messages')->where('seen',$unseen)->count();
            $album_id_=$id;
            $count_photos_ = DB::table('gallery')->where('album_id',$id)->count();

            $photos = $photos_;
            $count = $count_;
            $album_id = $album_id_;
            $count_photos =$count_photos_;

            return view('viewAlbum',compact('photos','count','album_id','count_photos'));
        }
        else return redirect('/admin');   
    }

    public function addPhoto(Request $request) {
    $request->validate([
        'album_id' => 'required',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5000',
    ]);

    $image = $request->input('image');
    $album_id = $request->input('album_id');
    $date = Carbon::now();

    if ($image = $request->file('image')) {
            $destinationPath = 'images/';
            $galleryImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $galleryImage);
            $input['image'] = "$galleryImage";
        }

    DB::table('gallery')->insert([
    'image' => $galleryImage,
    'album_id' => $album_id,
    'date_uploaded' => $date,
    ]);

    return redirect()->back()->with('message', 'Added a photo successfully!');
    }

    public function deletePhoto($id) {
    DB::delete('delete from gallery where id = ?',[$id]);
    return redirect()->back()->with('deleted', 'Deleted a photo successfully!');
    }

}
