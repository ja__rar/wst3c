<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HealthAdvisoryController extends Controller
{
    public function show($id) {
        if(session()->has('username')){
            $HAs = DB::select('select * from health_advisory where id = ?',[$id]);
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();
            return view('edit_HA',['HAs'=>$HAs],['count'=>$count]);
        }
        else return redirect('/admin');
    
    }
    public function edit(Request $request,$id) {
    $request->validate([
        'title' => 'required',
        'subtitle' => 'required',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ]);

    $input = $request->all();

    if ($image = $request->file('image')) {
            $destinationPath = 'images/';
            $advisoryImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $advisoryImage);
            $input['image'] = "$advisoryImage";
        }else{
            unset($input['image']);
        }

    $title = $input['title'];
    $subtitle = $input['subtitle'];
    $img = $input['image'];
    DB::update('update health_advisory set title = ? where id = ?',[$title,$id]);
    DB::update('update health_advisory set subtitle = ? where id = ?',[$subtitle,$id]);
    DB::update('update health_advisory set image = ? where id = ?',[$img,$id]);
    
    return redirect('/admin_home')->with('success', "Updated successfully");
    }

    public function insertAdvisoryForm() {
        if(session()->has('username')){
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();
            return view('addAdvisory',['count'=>$count]);
        }
        else return redirect('/admin');
    
    }

    public function insertAdvisory(Request $request) {
    $request->validate([
        'title' => 'required',
        'subtitle' => 'required',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5000',
    ]);

    $input = $request->all();

    $title = $input['title'];
    $subtitle = $input['subtitle'];
    $image = $request->input('image');

    if ($image = $request->file('image')) {
            $destinationPath = 'images/';
            $advisoryImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $advisoryImage);
            $input['image'] = "$advisoryImage";
        }

    DB::table('health_advisory')->insert([
    'title' => $title,
    'subtitle' => $subtitle,
    'image' => $advisoryImage
    ]);

    return redirect()->back()->with('message', 'Added a new advisory successfully!');
    }

    public function destroyAdvisory($id) {
    DB::delete('delete from health_advisory where id = ?',[$id]);
    return redirect()->back()->with('deleted', 'Deleted an advisory successfully!');
    }
}
