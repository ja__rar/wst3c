<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class PublicController extends Controller
{
    public function indexLandingPage() {
    $carouselList = DB::select('select * from carousel');
    $announcementsList = DB::table('announce_card')->orderBy('date_posted', 'desc')->take(3)->get();
    $statsList = DB::select('select * from quick_stats');

    $carousels = $carouselList;
    $announcements = $announcementsList;
    $stats = $statsList;

    return view('public_lp',compact('carousels','announcements','stats'));

    }

    public function publicGallery(){
        $albums = DB::table('albums')->orderBy('date_created', 'desc')->get();
        return view('public_gallery',['albums'=>$albums]);
    }

    public function viewAlbum($id) {
        $photos_ = DB::table('gallery')->where('album_id',$id)->orderBy('date_uploaded', 'desc')->paginate(9);;
        $count_photos_ = DB::table('gallery')->where('album_id',$id)->count();

        $photos = $photos_;
        $count_photos = $count_photos_;

        return view('public_viewAlbum',compact('photos','count_photos'));  
    }

    public function publicHome() {
    $stats = DB::select('select * from quick_stats');
    $HAs = DB::select('select * from health_advisory');

    return view('public_home',['stats'=>$stats],['HAs'=>$HAs]);
    }

    public function publicEmergency() {
    $hotlines = DB::select('select * from hotlines');

    return view('public_emergency',['hotlines'=>$hotlines]);
    }

    public function publicAnnouncements() {
    $photo_anns = DB::select('select * from announce_photo');
    // $card_anns = DB::select('select * from announce_card');
    $card_anns = DB::table('announce_card')->orderBy('date_posted', 'desc')->take(3)->get();
    return view('public_ann',['photo_anns'=>$photo_anns],['card_anns'=>$card_anns]);
    }

    public function publicAccomplishments() {
    $accs = DB::table('accomplishments')->orderBy('date', 'desc')->paginate(4);
    return view('public_acc',['accs'=>$accs]);
    }

    public function publicOfficials() {
    $officials = DB::select('select * from officials');
    $group = DB::select('select * from officials_group');
    return view('public_officials',['officials'=>$officials],['group'=>$group[0]]);
    }

    public function sendMessageForm() {
    return view('public_message');
    }

    public function sendMessage(Request $request) {
    $request->validate([
        'sender' => 'required',
        'contact_number' => 'required|max:11|min:11',
        'message' => 'required|max:500'
    ]);

    $date = Carbon::now();
    $sender = $request->input('sender');
    $contact_number = $request->input('contact_number');
    $message = $request->input('message');
    $seen = 0;

    $data=array('sender'=>$sender, 'contact_number'=>$contact_number,'message'=>$message, 'date_posted'=>$date, 'seen'=>$seen);

    DB::table('messages')->insert($data);
    return redirect()->back()->with('message', 'Message sent!');
    }

    public function showImage($id) {
    $photo = DB::select('select * from gallery where id = ?',[$id]);
    $album_id = DB::table('gallery')->where('id',$id)->pluck('album_id');
    return view('viewPhoto',['photo'=>$photo],['album_id'=>$album_id]);
    }

    public function showImgAcc($id) {
    $acc = DB::select('select * from accomplishments where id = ?',[$id]);
    return view('pub_open_acc',['acc'=>$acc]);
    }
}
