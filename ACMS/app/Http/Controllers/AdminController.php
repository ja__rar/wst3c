<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function indexManageAdmins() {
        if(session()->has('username')){
            $admins = DB::select('select * from admin');
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();
            
            return view('manageAdmins',['admins'=>$admins],['count'=>$count]);
        }
        else return redirect('/admin');
    }

    public function insertAdminForm() {
        if(session()->has('username')){
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();
            return view('newAdmin',['count'=>$count]);
        }
        else return redirect('/admin');
    }

    public function insertAdmin(Request $request) {
    $request->validate([
        'username' => 'required|unique:admin,username|max:20|min:7',
        'password' => 'required'
    ]);

    $date = Carbon::now();
    $username = $request->input('username');
    $password = md5($request->input('password'));

    $data=array('username'=>$username,'password'=>$password, 'account_created'=>$date, 'last_login'=>null);

    DB::table('admin')->insert($data);
    return redirect()->back()->with('message', 'Added a record successfully!');
    }

    public function showAdmin($id) {
        if(session()->has('username')){
            $admins = DB::select('select * from admin where id = ?',[$id]);
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();
            return view('editAdmin',['admins'=>$admins],['count'=>$count]);
        }
        else return redirect('/admin');
    }
   
    public function editAdmin(Request $request,$id) {
    $request->validate([
        'username' => 'required|unique:admin,username|max:20|min:7',
        'password' => 'required'
    ]);

    $username = $request->input('username');
    $password = md5($request->input('password'));

    DB::update('update admin set username = ? where id = ?',[$username,$id]);
    DB::update('update admin set password = ? where id = ?',[$password,$id]);

    return redirect('/manageAdmins')->with('success', "Updated successfully");
    }

    public function destroyAdmin($id) {
    DB::delete('delete from admin where id = ?',[$id]);
    return redirect()->back()->with('deleted', 'Deleted a record successfully!');
    }

    public function showHistory(){
        if(session()->has('username')){
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();
            return view('admin_his',['count'=>$count]);
        }
        else return redirect('/admin');
    }

    public function showMissionVision(){
        if(session()->has('username')){
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();
            return view('admin_misvis',['count'=>$count]);
        }
        else return redirect('/admin');
    }

    public function showGallery(){
        if(session()->has('username')){
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();
            $albums = DB::table('albums')->orderBy('date_created', 'desc')->get();
            return view('admin_gallery',['count'=>$count],['albums'=>$albums]);
        }
        else return redirect('/admin');
    }

    public function showMessages(){
        if(session()->has('username')){
            $messages = DB::select('select * from messages');
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();

            return view('admin_messages',['messages'=>$messages],['count'=>$count]);
        }
        else return redirect('/admin');
    }

    public function markRead($id) {
        DB::update('update messages set seen = 1 where id = ?',[$id]);
        $messages = DB::select('select * from messages');
        $unseen = 0;
        $count= DB::table('messages')->where('seen',$unseen)->count();
        return redirect()->back();
    }

    public function markAllRead() {
        DB::update('update messages set seen = 1 where seen = 0');
        $messages = DB::select('select * from messages');
        $unseen = 0;
        $count= DB::table('messages')->where('seen',$unseen)->count();
        return redirect()->back();
    }
    
}
