<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AccomplishmentController extends Controller
{
    public function index() {
        if(session()->has('username')){
            $accs = DB::table('accomplishments')->paginate(4);
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();

            return view('admin_acc',['accs'=>$accs],['count'=>$count]);
        }
        else return redirect('/admin');
    }

    public function showAcc($id) {
        $accs = DB::select('select * from accomplishments where id = ?',[$id]);
        $unseen = 0;
        $count= DB::table('messages')->where('seen',$unseen)->count();
        return view('edit_acc',['accs'=>$accs],['count'=>$count]);
    }

    public function editAcc(Request $request,$id) {
    $request->validate([
        'title' => 'required',
        'date' => 'required',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ]);

    $input = $request->all();

    if ($image = $request->file('image')) {
            $destinationPath = 'images/';
            $advisoryImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $advisoryImage);
            $input['image'] = "$advisoryImage";
        }else{
            unset($input['image']);
        }

    $title = $input['title'];
    $date = $input['date'];
    $img = $input['image'];

    DB::update('update accomplishments set title = ? where id = ?',[$title,$id]);
    DB::update('update accomplishments set date = ? where id = ?',[$date,$id]);
    DB::update('update accomplishments set image = ? where id = ?',[$img,$id]);

    $unseen = 0;
    $count= DB::table('messages')->where('seen',$unseen)->count();
    
    return redirect('/admin_acc')->with('success', "Updated successfully");
    }

    public function insertAccomplishmentForm() {
        if(session()->has('username')){
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();
            return view('addAccomplishment',['count'=>$count]);
        }
        else return redirect('/admin');
    
    }

    public function insertAccomplishment(Request $request) {
    $request->validate([
        'title' => 'required',
        'date' => 'required',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5000',
    ]);

    $input = $request->all();

    $title = $input['title'];
    $date = $input['date'];
    $image = $request->input('image');

    if ($image = $request->file('image')) {
            $destinationPath = 'images/';
            $accImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $accImage);
            $input['image'] = "$accImage";
        }

    DB::table('accomplishments')->insert([
    'title' => $title,
    'date' => $date,
    'image' => $accImage
    ]);

    return redirect()->back()->with('message', 'Added a new advisory successfully!');
    }

    public function destroyAccomplishment($id) {
    DB::delete('delete from accomplishments where id = ?',[$id]);
    return redirect()->back()->with('deleted', 'Deleted an accomplishment successfully!');
    }

}
