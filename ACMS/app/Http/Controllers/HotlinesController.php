<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class HotlinesController extends Controller
{
    public function indexManageHotlines() {
        if(session()->has('username')){
            $hotlines = DB::select('select * from hotlines');
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();
            return view('manageHotlines',['hotlines'=>$hotlines],['count'=>$count]);
        }
        else return redirect('/admin');
    }

    public function insertHotlineForm() {
        if(session()->has('username')){
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();
            return view('newHotline',['count'=>$count]);
        }
        else return redirect('/admin');
    }

    public function insertHotline(Request $request) {
    $request->validate([
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'office' => 'required|unique:hotlines,office',
        'smart' => 'required|min:11|max:11',
        'globe' => 'required|min:11|max:11',
        'landline' => 'required',
    ]);

    $date = Carbon::now();
    $image = $request->input('image');
    $office = $request->input('office');
    $smart = $request->input('smart');
    $globe = $request->input('globe');
    $landline = $request->input('landline');

    if ($image = $request->file('image')) {
            $destinationPath = 'images/';
            $hotlineImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $hotlineImage);
            $input['image'] = "$hotlineImage";
        }

    $data=array('logo'=>$hotlineImage,'office'=>$office, 'smart'=>$smart, 'globe'=>$globe, 'landline'=>$landline, 'last_updated'=>$date);

    DB::table('hotlines')->insert($data);
    return redirect()->back()->with('message', 'Added a record successfully!');
    }

    public function showHotline($id) {
    $hotlines = DB::select('select * from hotlines where id = ?',[$id]);
    $unseen = 0;
    $count= DB::table('messages')->where('seen',$unseen)->count();
    return view('editHotline',['hotlines'=>$hotlines],['count'=>$count]);
    }

    public function editHotline(Request $request,$id) {
    $request->validate([
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'office' => 'required',
        'smart' => 'required|min:11|max:11',
        'globe' => 'required|min:11|max:11',
        'landline' => 'required',
    ]);

    $input = $request->all();

    if ($image = $request->file('image')) {
            $destinationPath = 'images/';
            $hotlineImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $hotlineImage);
            $input['image'] = "$hotlineImage";
        }else{
            unset($input['image']);
        }

    $office = $input['office'];
    $smart = $input['smart'];
    $globe = $input['globe'];
    $landline = $input['landline'];
    $date = Carbon::now();
    $img = $input['image'];

    DB::update('update hotlines set logo = ? where id = ?',[$img,$id]);
    DB::update('update hotlines set office = ? where id = ?',[$office,$id]);
    DB::update('update hotlines set smart = ? where id = ?',[$smart,$id]);
    DB::update('update hotlines set globe = ? where id = ?',[$globe,$id]);
    DB::update('update hotlines set landline = ? where id = ?',[$landline,$id]);
    DB::update('update hotlines set last_updated = ? where id = ?',[$date,$id]);
    
    return redirect('/manageHotlines')->with('success', "Updated successfully");
    }

    public function destroyHotline($id) {
    DB::delete('delete from hotlines where id = ?',[$id]);
    return redirect()->back()->with('deleted', 'Deleted a hotline successfully!');
    }
}
