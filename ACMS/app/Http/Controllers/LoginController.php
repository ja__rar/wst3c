<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class LoginController extends Controller
{
    public function loginForm() {
      return view('/admin_lp');
    }

   public function validateForm(Request $request) {
      $this->validate($request,[
         'username'=>'required|max:10|min:7',
         'password'=>'required|max:10|min:7'
      ]);

        $username = $request->input('username');
        $password = md5($request->input('password'));
        $date = Carbon::now();

        $adminUsername = DB::table('admin')->where('username',$request->username)->first();
        $adminPassword = DB::table('admin')->where('password',$password)->first();

        if($adminUsername && $adminPassword){
            $data = $request->input();
            $request->session()->put('username',$data['username']);
            
            DB::update('update admin set last_login = ? where username = ?',[$date,$username]);
            return redirect('/admin_home');

        }
        else{
         return view('admin_lp')->withErrors("Unsuccessful login. Sorry.");
      }
      
   }
}
