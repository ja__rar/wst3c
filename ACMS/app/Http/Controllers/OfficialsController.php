<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class OfficialsController extends Controller
{
    public function index() {
        if(session()->has('username')){
            $officials_ = DB::select('select * from officials');
            $group_ = DB::select('select * from officials_group');
            $unseen = 0;
            $count_= DB::table('messages')->where('seen',$unseen)->count();

            $officials = $officials_;
            $group = $group_[0];
            $count = $count_;
            return view('admin_officials',compact('officials','group','count'));
            // return view('admin_officials',['officials'=>$officials],['group'=>$group[0]],['count'=>$count]);
        }
        else return redirect('/admin');
    
    }

    public function showGroup($id) {
        if(session()->has('username')){
            $group = DB::select('select * from officials_group where id = ?',[$id]);
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();
            return view('edit_group',['group'=>$group],['count'=>$count]);
        }
        else return redirect('/admin');
    
    }

    public function editGroup(Request $request,$id) {
    $request->validate([
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5000',
    ]);

    $input = $request->all();

    if ($image = $request->file('image')) {
            $destinationPath = 'images/';
            $advisoryImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $advisoryImage);
            $input['image'] = "$advisoryImage";
        }else{
            unset($input['image']);
        }
    $img = $input['image'];

    DB::update('update officials_group set image = ? where id = ?',[$img,$id]);
    
    return redirect('/admin_officials')->with('success', "Updated successfully");
    }

    public function showOfficials($id) {
        if(session()->has('username')){
            $officials = DB::select('select * from officials where id = ?',[$id]);
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();
            return view('edit_official',['officials'=>$officials],['count'=>$count]);
        }
        else return redirect('/admin');
    
    }

    public function editOfficials(Request $request,$id) {
    $request->validate([
        'name' => 'required',
        'position' => 'required',
        'phone' => 'required|max:11|min:11',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ]);

    $input = $request->all();

    if ($image = $request->file('image')) {
            $destinationPath = 'images/';
            $advisoryImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $advisoryImage);
            $input['image'] = "$advisoryImage";
        }else{
            unset($input['image']);
        }

    $name = $input['name'];
    $position = $input['position'];
    $phone = $input['date'];
    $img = $input['image'];

    DB::update('update officials set title = ? where id = ?',[$name,$id]);
    DB::update('update officials set date = ? where id = ?',[$position,$id]);
    DB::update('update officials set date = ? where id = ?',[$phone,$id]);
    DB::update('update officials set image = ? where id = ?',[$img,$id]);
    
    return redirect('/admin_officials')->with('success', "Updated successfully");
    }
}
