<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class AnnouncementController extends Controller
{
    public function index() {
        if(session()->has('username')){
            $photo_anns_ = DB::select('select * from announce_photo');
            $card_anns_ = DB::table('announce_card')->orderBy('date_posted', 'desc')->get();
            $unseen = 0;
            $count_= DB::table('messages')->where('seen',$unseen)->count();

            $photo_anns =$photo_anns_;
            $card_anns = $card_anns_;
            $count = $count_;

            return view('admin_ann',compact('photo_anns','card_anns','count'));
        }
        else return redirect('/admin');
    
    }

    public function showPhotoAnns($id) {
        if(session()->has('username')){
            $photo_anns = DB::select('select * from announce_photo where id = ?',[$id]);
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();
            return view('edit_photo_anns',['photo_anns'=>$photo_anns],['count'=>$count]);
        }
        else return redirect('/admin');
    
    }

    public function insertPubMatForm() {
        if(session()->has('username')){
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();
            return view('addAnnouncementPhoto',['count'=>$count]);
        }
        else return redirect('/admin');
    
    }

    public function insertPubMat(Request $request) {
    $request->validate([
        'title' => 'required',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5000',
    ]);

    $input = $request->all();

    $title = $input['title'];
    $image = $request->input('image');

    if ($image = $request->file('image')) {
            $destinationPath = 'images/';
            $annImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $annImage);
            $input['image'] = "$annImage";
        }

    DB::table('announce_photo')->insert([
    'title' => $title,
    'photo' => $annImage
    ]);

    return redirect()->back()->with('message', 'Added a new announcement photo successfully!');
    }

    public function editPhotoAnns(Request $request,$id) {
    $request->validate([
        'title' => 'required',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ]);

    $input = $request->all();

    if ($image = $request->file('image')) {
            $destinationPath = 'images/';
            $advisoryImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $advisoryImage);
            $input['image'] = "$advisoryImage";
        }else{
            unset($input['image']);
        }

    $title = $input['title'];
    $img = $input['image'];

    DB::update('update announce_photo set title = ? where id = ?',[$title,$id]);
    DB::update('update announce_photo set photo = ? where id = ?',[$img,$id]);
    
    return redirect('/admin_ann');
    }

    public function destroyPhotoAnns($id) {
    DB::delete('delete from announce_photo where id = ?',[$id]);
    return redirect()->back()->with('deleted', 'Deleted a photo successfully!');
    }


    public function showCardAnns($id) {
        if(session()->has('username')){
            $card_anns = DB::select('select * from announce_card where id = ?',[$id]);
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();
            return view('edit_card_anns',['card_anns'=>$card_anns],['count'=>$count]);
        }
        else return redirect('/admin');
    
    }

    public function insertCardAnnouncementForm() {
        if(session()->has('username')){
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();
            return view('addAnnouncementCard',['count'=>$count]);
        }
        else return redirect('/admin');
    
    }

    public function insertCardAnnouncement(Request $request) {
    $request->validate([
        'source' => 'required',
        'title' => 'required',
        'details' => 'required',
    ]);

    $input = $request->all();

    $title = $input['title'];
    $source = $input['source'];
    $details = $input['details'];
    $source_url = $request->input('source_url');
    $date = Carbon::now();

    DB::table('announce_card')->insert([
    'title' => $title,
    'source' => $source,
    'details' => $details,
    'source_url' => $source_url,
    'date_posted' => $date
    ]);

    return redirect()->back()->with('message', 'Added a new announcement successfully!');
    }

    public function editCardAnns(Request $request,$id) {
    $request->validate([
        'source' => 'required',
        'title' => 'required',
        'details' => 'required',
    ]);

    $input = $request->all();

    $title = $input['title'];
    $source = $input['source'];
    $details = $input['details'];
    $details = $input['details'];

    DB::update('update announce_card set title = ? where id = ?',[$title,$id]);
    DB::update('update announce_card set source = ? where id = ?',[$source,$id]);
    DB::update('update announce_card set details = ? where id = ?',[$details,$id]);
    
    return redirect('/admin_ann')->with('success', "Updated successfully");
    }

    public function destroyCardAnns($id) {
    DB::delete('delete from announce_card where id = ?',[$id]);
    return redirect()->back()->with('deleted', 'Deleted an announcement card successfully!');
    }
}
