<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class LandingPageController extends Controller
{
    public function indexLandingPage() {
        if(session()->has('username')){
            $carousels = DB::select('select * from carousel');
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();
            
            return view('editLandingPage',['carousels'=>$carousels],['count'=>$count]);
        }
        else return redirect('/admin');
    }

    public function showCarouselForm($id) {
        if(session()->has('username')){
            $carousels = DB::select('select * from carousel where id = ?',[$id]);
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();
            return view('editLandingPageCarousel',['carousels'=>$carousels],['count'=>$count]);
        }
        else return redirect('/admin');
    
    }

    public function editCarousel(Request $request,$id) {
    $request->validate([
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ]);

    $input = $request->all();

    if ($image = $request->file('image')) {
            $destinationPath = 'images/';
            $carouselImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $carouselImage);
            $input['image'] = "$carouselImage";
        }else{
            unset($input['image']);
        }
    $img = $input['image'];

    DB::update('update carousel set image = ? where id = ?',[$img,$id]);
    
    return redirect('/editLandingPage');
    }
}
