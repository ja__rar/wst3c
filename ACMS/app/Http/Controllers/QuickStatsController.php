<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class QuickStatsController extends Controller
{
    public function index() {
        if(session()->has('username')){
            $stats_ = DB::select('select * from quick_stats');
            $HAs_ = DB::table('health_advisory')->paginate(6);
            $unseen = 0;
            $count_= DB::table('messages')->where('seen',$unseen)->count();

            $stats = $stats_;
            $HAs = $HAs_;
            $count = $count_;

            return view('admin_home',compact('stats','HAs','count'));
        }
        else return redirect('/admin');
    
    }
    public function show($id) {
        if(session()->has('username')){
            $stats = DB::select('select * from quick_stats where id = ?',[$id]);
            $unseen = 0;
            $count= DB::table('messages')->where('seen',$unseen)->count();
            return view('edit_stats',['stats'=>$stats],['count'=>$count]);
        }
        else return redirect('/admin');
    }
   
    public function edit(Request $request,$id) {
    $request->validate([
            'place' => 'required',
            'population' => 'required',
            'year' => 'required',
    ]);

    $place = $request->input('place');
    $population = $request->input('population');
    $year = $request->input('year');

    DB::update('update quick_stats set place = ? where id = ?',[$place,$id]);
    DB::update('update quick_stats set population = ? where id = ?',[$population,$id]);
    DB::update('update quick_stats set year = ? where id = ?',[$year,$id]);

    return redirect('/admin_home')->with('success', "Updated successfully");
    }
}
