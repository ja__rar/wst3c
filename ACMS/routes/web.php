<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\QuickStatsController;
use App\Http\Controllers\HealthAdvisoryController;
use App\Http\Controllers\AnnouncementController;
use App\Http\Controllers\AccomplishmentController;
use App\Http\Controllers\OfficialsController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\LandingPageController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\HotlinesController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('public_lp');
});

Route::get('/public_lp', function () {
    return view('public_lp');
});

Route::get('/public_home', function () {
    return view('public_home');
});

Route::get('/public_ann', function () {
    return view('public_ann');
});

Route::get('/public_acc', function () {
    return view('public_acc');
});

Route::get('/public_his', function () {
    return view('public_his');
});

Route::get('/public_officials', function () {
    return view('public_officials');
});

Route::get('/public_misvis', function () {
    return view('public_misvis');
});

Route::get('/admin', function () {
    if(session()->has('username')){
        return view('/admin_home');
    }
    else{
        return redirect('/admin');
    }
    return view('admin_lp');
});

Route::get('/admin_gallery', function () {
    if(session()->has('username')){
        return view('/admin_gallery');
    }
    else{
        return redirect('/admin');
    } 
});

Route::get('/addAdvisory', function () {
    if(session()->has('username')){
        return view('/addAdvisory');
    }
    else{
        return redirect('/admin');
    } 
});

Route::get('/addAccomplishment', function () {
    if(session()->has('username')){
        return view('/addAccomplishment');
    }
    else{
        return redirect('/admin');
    } 
});

Route::get('/admin', function () {
    if(session()->has('username')){
        return redirect('/admin_home');
    }
    else{
        return redirect('/admin');
    }  
});

Route::get('/logout', function () {
    if(session()->has('username')){
        session()->pull('username');
    }
    return redirect('/admin');
});

Route::get('/admin', [LoginController::class, 'loginForm' ]);
Route::post('/admin', [LoginController::class, 'validateForm' ]);

Route::get('admin_home',[QuickStatsController::class, 'index']);
Route::get('edit_stats/{id}',[QuickStatsController::class, 'show']);
Route::post('edit_stats/{id}',[QuickStatsController::class, 'edit']);
Route::get('edit_HA/{id}',[HealthAdvisoryController::class, 'show']);
Route::post('edit_HA/{id}',[HealthAdvisoryController::class, 'edit']);
Route::get('addAdvisory', [HealthAdvisoryController::class, 'insertAdvisoryForm' ]);
Route::post('addAdvisory', [HealthAdvisoryController::class, 'insertAdvisory' ]);
Route::get('admin_home/{id}',[HealthAdvisoryController::class, 'destroyAdvisory']);

Route::get('admin_ann',[AnnouncementController::class, 'index']);
Route::get('addAnnouncementPhoto', [AnnouncementController::class, 'insertPubMatForm' ]);
Route::post('addAnnouncementPhoto', [AnnouncementController::class, 'insertPubMat' ]);
Route::get('addAnnouncementCard', [AnnouncementController::class, 'insertCardAnnouncementForm' ]);
Route::post('addAnnouncementCard', [AnnouncementController::class, 'insertCardAnnouncement' ]);
Route::get('edit_photo_anns/{id}',[AnnouncementController::class, 'showPhotoAnns']);
Route::post('edit_photo_anns/{id}',[AnnouncementController::class, 'editPhotoAnns']);
Route::get('edit_card_anns/{id}',[AnnouncementController::class, 'showCardAnns']);
Route::post('edit_card_anns/{id}',[AnnouncementController::class, 'editCardAnns']);
Route::get('delete_photo_anns/{id}',[AnnouncementController::class, 'destroyPhotoAnns']);
Route::get('delete_card_anns/{id}',[AnnouncementController::class, 'destroyCardAnns']);

Route::get('admin_acc',[AccomplishmentController::class, 'index']);
Route::get('edit_acc/{id}',[AccomplishmentController::class, 'showAcc']);
Route::post('edit_acc/{id}',[AccomplishmentController::class, 'editAcc']);
Route::get('addAccomplishment', [AccomplishmentController::class, 'insertAccomplishmentForm' ]);
Route::post('addAccomplishment', [AccomplishmentController::class, 'insertAccomplishment' ]);
Route::get('admin_acc/{id}',[AccomplishmentController::class, 'destroyAccomplishment']);

Route::get('admin_officials',[OfficialsController::class, 'index']);
Route::get('edit_group/{id}',[OfficialsController::class, 'showGroup']);
Route::post('edit_group/{id}',[OfficialsController::class, 'editGroup']);
Route::get('edit_official/{id}',[OfficialsController::class, 'showOfficials']);
Route::post('edit_official/{id}',[OfficialsController::class, 'editOfficials']);

Route::get('editLandingPage',[LandingPageController::class, 'indexLandingPage']);
Route::get('editLandingPageCarousel/{id}', [LandingPageController::class, 'showCarouselForm' ]);
Route::post('editLandingPageCarousel/{id}',[LandingPageController::class, 'editCarousel']);


Route::get('public_lp',[PublicController::class, 'indexLandingPage']);
Route::get('public_gallery',[PublicController::class, 'publicGallery']);
Route::get('public_viewAlbum/{id}',[PublicController::class, 'viewAlbum']);
Route::get('public_viewAlbum/view/{id}',[PublicController::class, 'showImage']);
Route::get('EmergencyHotlines',[PublicController::class, 'publicEmergency']);
Route::get('ContactUs',[PublicController::class, 'sendMessageForm']);
Route::post('ContactUs',[PublicController::class, 'sendMessage']);
Route::get('/',[PublicController::class, 'indexLandingPage']);
Route::get('public_home',[PublicController::class, 'publicHome']);
Route::get('public_ann',[PublicController::class, 'publicAnnouncements']);
Route::get('public_acc',[PublicController::class, 'publicAccomplishments']);
Route::get('public_officials',[PublicController::class, 'publicOfficials']);
Route::get('public_acc/{id}',[PublicController::class, 'showImgAcc']);


Route::get('manageAdmins',[AdminController::class, 'indexManageAdmins']);
Route::get('newAdmin', [AdminController::class, 'insertAdminForm' ]);
Route::post('newAdmin', [AdminController::class, 'insertAdmin' ]);
Route::get('editAdmin/{id}',[AdminController::class, 'showAdmin']);
Route::post('editAdmin/{id}',[AdminController::class, 'editAdmin']);
Route::get('deleteAdmin/{id}',[AdminController::class, 'destroyAdmin']);
Route::get('Messages/',[AdminController::class, 'showMessages']);
Route::get('read/{id}',[AdminController::class, 'markRead']);
Route::get('readAll',[AdminController::class, 'markAllRead']);
Route::get('admin_his/',[AdminController::class, 'showHistory']);
Route::get('admin_misvis/',[AdminController::class, 'showMissionVision']);
Route::get('admin_gallery/',[AdminController::class, 'showGallery']);

Route::get('manageHotlines',[HotlinesController::class, 'indexManageHotlines']);
Route::get('newHotline', [HotlinesController::class, 'insertHotlineForm' ]);
Route::post('newHotline', [HotlinesController::class, 'insertHotline' ]);
Route::get('editHotline/{id}',[HotlinesController::class, 'showHotline']);
Route::post('editHotline/{id}',[HotlinesController::class, 'editHotline']);
Route::get('deleteHotline/{id}',[HotlinesController::class, 'destroyHotline']);

Route::get('addAlbum', [GalleryController::class, 'addAlbumForm' ]);
Route::post('addAlbum', [GalleryController::class, 'addAlbum' ]);
Route::get('viewAlbum/{id}',[GalleryController::class, 'indexAlbum']);
Route::post('viewAlbum/{id}',[GalleryController::class, 'addPhoto']);
Route::get('viewAlbum/delete/{id}',[GalleryController::class, 'deletePhoto']);
