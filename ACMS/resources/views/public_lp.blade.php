<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" href="/images/logo.png" type="image/x-icon">
	<title>Brgy. Amagbagan</title>

	<link href="https://fonts.googleapis.com/css2?family=Nunito&family=Poppins:wght@300;400&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
	<link href="/css/public_lp.css" rel="stylesheet" type="text/css">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>

	<style>
    	body {
    		font-family: 'Nunito';
    		background-color: #e3e9f7;
        }

        .header {
			overflow: hidden;
			background-color: #FFEBEE;
			padding: 20px 10px;
			  
		}

        .btn:hover{
		    transform: translateY(-3px);
		    box-shadow: 0 10px 20px rgba(0, 0, 0, .2);
		    color: white;
		    background-color: green;
		}
		.btn-white::after {
		    background-color: maroon;
		}
    </style>

    <script>
        function date_time(id) {
            date = new Date;
            year = date.getFullYear();
            month = date.getMonth();
            months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
            d = date.getDate();
            day = date.getDay();
            days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
            h = date.getHours();
            if (h < 10) {
                h = "0" + h;
            }
            m = date.getMinutes();
            if (m < 10) {
                m = "0" + m;
            }
            s = date.getSeconds();
            if (s < 10) {
                s = "0" + s;
            }
            result = ' ' + months[month] + ' ' + d + '' + ', ' + year + ' | ' + days[day] + ' &nbsp; &nbsp;&nbsp; ' + h + ' : ' + m + ' : ' + s;
            document.getElementById(id).innerHTML = result;
            setTimeout('date_time("' + id + '");', '1000');
            return true;
        }
        window.onscroll = () => {
		  toggleTopButton();
		}
		function scrollToTop(){
		  window.scrollTo({top: 0, behavior: 'smooth'});
		}

		function toggleTopButton() {
		  if (document.body.scrollTop > 20 ||
		      document.documentElement.scrollTop > 20) {
		    document.getElementById('back-to-up').classList.remove('d-none');
		  } else {
		    document.getElementById('back-to-up').classList.add('d-none');
		  }
		}

		function reveal() {
		  var reveals = document.querySelectorAll(".reveal");

		  for (var i = 0; i < reveals.length; i++) {
		    var windowHeight = window.innerHeight;
		    var elementTop = reveals[i].getBoundingClientRect().top;
		    var elementVisible = 150;

		    if (elementTop < windowHeight - elementVisible) {
		      reveals[i].classList.add("active");
		    } else {
		      reveals[i].classList.remove("active");
		    }
		  }
		}

		window.addEventListener("scroll", reveal);

    </script>

</head>
<body>
	<div class="header sticky-top">
	  <a href="#default" class="logo"><img class="img-fluid" src="/images/logo.png" style="width:50%; height:auto; max-width:100px;"/><b>Brgy. Amagbagan Information Center</b></a>

	  <div class="header-right">
	    <p class="hidden-sm hidden-xs" id="date_time"></p>
        <script type="text/javascript">window.onload = date_time('date_time');</script>
        <div>
        	<p></p>
        </div>
        <div>
            <p>Brgy. Amagbagan, Pozorrubio, Pangasinan</p>
        </div>
        <div>
            <p>Population as of {{ $stats[2]->year }} census: <b>{{ $stats[2]->population }}</b></p>
        </div>
	  </div>
	</div>

	<div class = "hakdog text-center" style="margin-top: 50px;">
		<p class ="hello">A M A G B A G A N</p>
		<p class ="quote">“Matino, Mahusay at Maasahan Para sa Mapagkalinga at Maunlad na Barangay”</p>
		<a href="public_home" class="btn btn-white btn-animated" id="btn">READ NEW INFORMATION <i class="fa-solid fa-arrow-right"></i></a>
	</div>
	<div class = "container mx-auto m-4 p-4">
		<div class="row">
			<div class ="col-md-7 p-4">
				<div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
					<div class="carousel-inner">
					    <div class="carousel-item active">
                            <img src="{{ asset('images')}}/{{ $carousels[0]->image }}" class="d-block w-100" alt="...">
                        </div>
                        @foreach($carousels as $carousel)
							<div class="carousel-item">
                                <img src="{{ asset('images')}}/{{ $carousel->image }}" class="d-block w-100" alt="...">
                            </div>   
                        @endforeach
					</div>
					<button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
					    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
					    <span class="visually-hidden">Previous</span>
					</button>
					<button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
					    <span class="carousel-control-next-icon" aria-hidden="true"></span>
					    <span class="visually-hidden">Next</span>
					</button>
				</div>
			</div>
			<div class ="col-12 col-md-5 p-4">
				@foreach($announcements as $announcement)
					<div class="card reveal fade-bottom mb-3 text-white" style="background-color: maroon;">
						<div class="card-header">
							Announcement from <a target="_blank" rel="noopener noreferrer" class="text-white" href="{{ $announcement->source_url }}">{{ $announcement->source }}</a>
						</div>
						<div class="card-body bg-light">
							<h5 class="card-title text-dark">{{ $announcement->title }}</h5>
							<p style="color: gray; font-size: 10">Date Posted: {{ $announcement->date_posted }}</p>
							<p class="card-text text-dark" style="text-align: justify;">{{ $announcement->details }}</p>    
						</div>
					</div>
				@endforeach
			</div>
			<div class ="col-12 col-md-5 p-4">
				
				
				
			</div>
		</div>
	</div>

	<br>

	<button class="btn btn-sm btn-primary rounded-circle position-fixed bottom-0 end-0 translate-middle d-none" onclick="scrollToTop()" id="back-to-up">
	  <i class="fa fa-arrow-up" aria-hidden="true"></i>
	</button>

	<footer class="footer text-center text-white">
		<div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);"> © <script type="text/JavaScript">document.write(new Date().getFullYear());</script> Copyright:
			<a class="text-white" href="https://www.facebook.com/skamagbagantransform" target="_blank" rel="noopener noreferrer" style="text-decoration: none;">Brgy. Amagbagan, Pozorrubio, Pangasinan | Developed by July Anne Rhaemonette Rosal</a>
		</div>
  	</footer>


	

</body>
</html>