<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/images/logo.png" type="image/x-icon">
    <title>Messages</title>

    <link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <link href="/css/admin_home.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#table').DataTable({
                "order": [[ 3, "desc" ]], //or asc 
                "className": "text-center",
            });
        });
        $(function() {
            var timeout = 3000; // in miliseconds (3*1000)
            $('.alert').delay(timeout).fadeOut(300);
        });
        const mobileScreen = window.matchMedia("(max-width: 990px )");
            $(document).ready(function () {
                $(".dashboard-nav-dropdown-toggle").click(function () {
                    $(this).closest(".dashboard-nav-dropdown")
                        .toggleClass("show")
                        .find(".dashboard-nav-dropdown")
                        .removeClass("show");
                    $(this).parent()
                        .siblings()
                        .removeClass("show");
                });
                $(".menu-toggle").click(function () {
                    if (mobileScreen.matches) {
                        $(".dashboard-nav").toggleClass("mobile-show");
                    } else {
                        $(".dashboard").toggleClass("dashboard-compact");
                    }
                });
            });
        function toggleText() {
  
            // Get all the elements from the page
            var points = 
                document.getElementById("points");
  
            var showMoreText =
                document.getElementById("moreText");
  
            var buttonText =
                document.getElementById("textButton");
  
            // If the display property of the dots 
            // to be displayed is already set to 
            // 'none' (that is hidden) then this 
            // section of code triggers
            if (points.style.display === "none") {
  
                // Hide the text between the span
                // elements
                showMoreText.style.display = "none";
  
                // Show the dots after the text
                points.style.display = "inline";
  
                // Change the text on button to 
                // 'Show More'
                buttonText.innerHTML = "Read more";
            }
  
            // If the hidden portion is revealed,
            // we will change it back to be hidden
            else {
  
                // Show the text between the
                // span elements
                showMoreText.style.display = "inline";
  
                // Hide the dots after the text
                points.style.display = "none";
  
                // Change the text on button
                // to 'Show Less'
                buttonText.innerHTML = "Read less";
            }
        }
        
    </script>
</head>
<body>

<div class='dashboard'>
    <div class="dashboard-nav">
        <header><a href="#!" class="menu-toggle"><i class="fas fa-bars"></i></a><a href="{{url('admin_home')}}"
                                                                                   class="brand-logo"><center><img class="img-fluid" src="/images/logo.png" style="width:100%; height:auto; max-width:100px;"/> </center><span></span></a></header>
        <nav class="dashboard-nav-list">
            <a href="{{url('admin_home')}}" class="dashboard-nav-item"><i class="fas fa-home"></i>Home</a>
            <a href="{{url('admin_ann')}}" class="dashboard-nav-item"><i class="fas fa-bullhorn"></i>Announcements</a>
            <a href="{{url('admin_acc')}}" class="dashboard-nav-item"><i class="fas fa-check-double"></i> Accomplishments</a>
            <a href="{{url('admin_his')}}" class="dashboard-nav-item"><i class="fas fa-monument"></i>Brief History</a>
            <a href="{{url('admin_officials')}}" class="dashboard-nav-item"><i class="fas fa-sitemap"></i>Barangay Officials</a>
            <a href="{{url('admin_gallery')}}" class="dashboard-nav-item"><i class="fas fa-images"></i>Gallery</a>
            <a href="{{url('admin_misvis')}}" class="dashboard-nav-item"><i class="fas fa-bullseye"></i>Mission and Vision</a>
            <a href="{{url('Messages')}}" class="dashboard-nav-item active"><i class="fas fa-bell"></i><div id="count"></div>Messages ({{$count}} new)</a>

            <div class='dashboard-nav-dropdown'><a href="#!" class="dashboard-nav-item dashboard-nav-dropdown-toggle"><i class="fas fa-info"></i> Others </a>
                <div class='dashboard-nav-dropdown-menu'>
                    <a href="{{url('editLandingPage')}}" class="dashboard-nav-dropdown-item">Landing Page</a>
                    <a href="{{url('manageAdmins')}}" class="dashboard-nav-dropdown-item">Admins</a>
                    <a href="{{url('manageHotlines')}}" class="dashboard-nav-dropdown-item">Emergency Hotlines</a>
                </div>
            </div>
            
            <div class="nav-item-divider"></div>
          
            <a href="/logout" class="dashboard-nav-item"><i class="fa-solid fa-right-from-bracket"></i></i>Logout</a>

        </nav>
    </div>
    <div class='dashboard-app'>
        <header class='dashboard-toolbar'><a href="#!" class="menu-toggle"><i class="fas fa-bars"></i></a></header>
        <div class='dashboard-content'>
            <div class='container'>
                <div class='card'>
                    <div class='card-header bg-warning'>
                        <h1>Messages</h1>
                    </div>

                    <div class='card-body'>
                        <div class ="row">
                            <div class="card mt-4" style="background-clip: unset; border: none;">
                              <div class="card-header" style="background-color: #B71C1C;">
                                <h1 style="color: white; font-size: 20px; text-align: left;">View Messages</h1>
                              </div>
                                <a href="readAll"><button class="btn btn-warning mt-4"><i class="fa-solid fa-check" style="color: black"></i> Mark All as Read</button></a>
                              <div class="container pt-4">
                                
                                    <div class="table-responsive">
                                    <table class="table table-striped mt-4" id="table">
                                      <thead class="bg-warning">
                                        <tr>
                                          <th scope="col">Sender</th>
                                          <th scope="col">Contact Number</th>
                                          <th scope="col">Message</th>
                                          <th scope="col">Date Posted</th>
                                          <th scope="col">Mark as Read</th>
                                        </tr>
                                      </thead>
                                      <tbody><center>
                                        @foreach ($messages as $message)
                                        @if($message->seen=='0')
                                            <tr style="background-color: #FFCDD2;">
                                        @else
                                            <tr style="background-color: #FFE0B2;">
                                        @endif
                                          <th scope="row">{{ $message->sender }}</th>
                                          <td scope="row">{{ $message->contact_number }}</td>
                                          <td>{{ $message->message }}</td>
                                          <td>{{ $message->date_posted }}</td>
                                        @if($message->seen=='0')
                                            <td><a href = "read/{{$message->id}}"><button class="btn" style="background-color:#8BC34A;">Mark as Read</button></a></td>
                                        @else
                                            <td><button class="btn" style="background-color:gray; color: white;" disabled="">Mark as Read</button></td>
                                        @endif
                                          
                                        </tr></center>
                                        @endforeach
                                      </tbody>
                                    </table>
                                </div>
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <footer class="footer text-center text-white">
        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);"> © <script type="text/JavaScript">document.write(new Date().getFullYear());</script> Copyright:
            <a class="text-white" href="" style="text-decoration: none;">Brgy. Amagbagan, Pozorrubio, Pangasinan | Developed by July Anne Rhaemonette Rosal</a>
        </div>
    </footer> 
</div>
      


</div>
</div>

</body>
</html>