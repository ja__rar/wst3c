<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/images/logo.png" type="image/x-icon">
    <title>Gallery</title>

    <link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <link href="/css/public_gallery.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>


    <style type="text/css">
        .modal-header {
            background-image: linear-gradient(#D50000 0%, #FFAB00 100%);

        }
        .modal-body {
            background-image: linear-gradient(purple 0%, pink 100%);
        }
        p:{
            opacity: 1;
        }
        img{
            width: 100%;
            height: 100%;
            object-fit: cover;
            border-radius: 8px;
        }
        .box__title{
            position: absolute;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 25%;
            background-color: maroon;
            border-radius: 0 0 8px 8px;
            vertical-align: middle;
            transition: .5s;
            transition-timing-function: ease-out;
        }
        h6{
            text-align: center;
            color: white;
            text-transform: uppercase;
            margin: 7% 0 0 0;
            font-size: .9em;
        }
        .add_box{
            @extend.box;
            background-color: #f9f9f9;
        }
    </style>


    <script>

        $(document).ready(function(){
            $("#myModal").modal('show');
        });
        const mobileScreen = window.matchMedia("(max-width: 990px )");
            $(document).ready(function () {
                $(".dashboard-nav-dropdown-toggle").click(function () {
                    $(this).closest(".dashboard-nav-dropdown")
                        .toggleClass("show")
                        .find(".dashboard-nav-dropdown")
                        .removeClass("show");
                    $(this).parent()
                        .siblings()
                        .removeClass("show");
                });
                $(".menu-toggle").click(function () {
                    if (mobileScreen.matches) {
                        $(".dashboard-nav").toggleClass("mobile-show");
                    } else {
                        $(".dashboard").toggleClass("dashboard-compact");
                    }
                });
            });
        
    </script>
</head>
<body>

<div class='dashboard'>
    <div class="dashboard-nav">
        <header><a href="#!" class="menu-toggle"><i class="fas fa-bars"></i></a><a href="public_lp"
                                                                                   class="brand-logo"><center><img class="img-fluid" src="/images/logo.png" style="width:100%; height:auto; max-width:100px;"/> </center><span></span></a></header>
        <nav class="dashboard-nav-list">
            <a href="public_home" class="dashboard-nav-item "><i class="fas fa-home"></i>Home</a>
            <a href="public_ann" class="dashboard-nav-item"><i class="fas fa-bullhorn"></i>Announcements</a>
            <a href="public_acc" class="dashboard-nav-item"><i class="fas fa-check-double"></i> Accomplishments</a>
            <a href="public_his" class="dashboard-nav-item"><i class="fas fa-monument"></i>Brief History</a>
            <a href="public_officials" class="dashboard-nav-item"><i class="fas fa-sitemap"></i>Barangay Officials</a>
            <a href="public_gallery" class="dashboard-nav-item active"><i class="fas fa-images"></i>Gallery</a>
            <a href="public_misvis" class="dashboard-nav-item"><i class="fas fa-bullseye"></i>Mission and Vision</a>
            <a href="EmergencyHotlines" class="dashboard-nav-item"><i class="fas fa-info"></i>Emergency Hotlines</a>
            <a href="ContactUs" class="dashboard-nav-item"><i class="fas fa-paper-plane"></i>Contact Us</a>
            <div class="nav-item-divider"></div>
            <div class = "mx-auto">
                <p style="color: white;"><i class="fa-solid fa-thumbs-up fa-lg"></i>&nbsp; &nbsp;Like our Facebook Page</p>
            </div>
            <div class = "mx-auto">
            
              <a href="https://www.facebook.com/skamagbagantransform"><i class="fa-brands fa-facebook-square fa-2xl" style="color: white;"></i>&nbsp; &nbsp; &nbsp;</a>
          
            </div>
        </nav>
    </div>
    <div class='dashboard-app'>
        <header class='dashboard-toolbar'><a href="#!" class="menu-toggle"><i class="fas fa-bars"></i></a></header>
        <div class='dashboard-content'>
            <div class='container'>
                <div class='card'>
                    <div class='card-header bg-warning'>
                        <h1 class="headerText">Community Gallery</h1>
                    </div>

                    <div class='card-body'>
                        <center>
                            <div class ="row mx-auto">
                                <div class="modal fade" id="myModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false">
                                  <div class="modal-dialog">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title text-white" id="exampleModalLabel">View Image</h5>
                                        <a href="{{ url('public_viewAlbum')}}/{{$album_id[0]}}"><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button></a>
                                      </div>
                                      <div class="modal-body">
                                        <img class="img-thumbnail" src="{{ asset('images')}}/{{ $photo[0]->image }}" class="card-img-top img-fluid" alt="..." style="max-width: 500px;max-height: 500px;">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        </div>
         <footer class="footer text-center text-white">
                <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);"> © <script type="text/JavaScript">document.write(new Date().getFullYear());</script> Copyright:
                    <a class="text-white" href="" style="text-decoration: none;">Brgy. Amagbagan, Pozorrubio, Pangasinan | Developed by July Anne Rhaemonette Rosal</a>
                </div>
            </footer>
    </div>
</div>

</body>
</html>