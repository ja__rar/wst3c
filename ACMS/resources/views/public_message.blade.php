<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/images/logo.png" type="image/x-icon">
    <title>Contact Us</title>

    <link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <link href="/css/public_acc.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>

    <style type="text/css">
        .card-img-top {
            width: 100%;
            height: 15vw;
            object-fit: cover;
        }
        .grr {
            background-image: linear-gradient(#D50000 0%, #FFAB00 100%);
        }
        .grr .card-body{
            background-color: #F3E5F5;
        }
        .grr .card-text{
            background-color: black;
        }
        .grr:hover{
            background-image: linear-gradient(purple 0%, pink 100%);
            transform: scale(1.03);
            box-shadow: 10px 10px 15px rgba(0,0,0,0.3);
            transition: transform .5s ease-out 0s;
            color: #FF3D00;
        }
    </style>

    <script>
        $(function() {
            var timeout = 3000; // in miliseconds (3*1000)
            $('.alert').delay(timeout).fadeOut(300);
        });
        const mobileScreen = window.matchMedia("(max-width: 990px )");
            $(document).ready(function () {
                $(".dashboard-nav-dropdown-toggle").click(function () {
                    $(this).closest(".dashboard-nav-dropdown")
                        .toggleClass("show")
                        .find(".dashboard-nav-dropdown")
                        .removeClass("show");
                    $(this).parent()
                        .siblings()
                        .removeClass("show");
                });
                $(".menu-toggle").click(function () {
                    if (mobileScreen.matches) {
                        $(".dashboard-nav").toggleClass("mobile-show");
                    } else {
                        $(".dashboard").toggleClass("dashboard-compact");
                    }
                });
            });
       window.onscroll = () => {
          toggleTopButton();
        }
        function scrollToTop(){
          window.scrollTo({top: 0, behavior: 'smooth'});
        }

        function toggleTopButton() {
          if (document.body.scrollTop > 20 ||
              document.documentElement.scrollTop > 20) {
            document.getElementById('back-to-up').classList.remove('d-none');
          } else {
            document.getElementById('back-to-up').classList.add('d-none');
          }
        }

        function reveal() {
          var reveals = document.querySelectorAll(".reveal");

          for (var i = 0; i < reveals.length; i++) {
            var windowHeight = window.innerHeight;
            var elementTop = reveals[i].getBoundingClientRect().top;
            var elementVisible = 150;

            if (elementTop < windowHeight - elementVisible) {
              reveals[i].classList.add("active");
            } else {
              reveals[i].classList.remove("active");
            }
          }
        }

        window.addEventListener("scroll", reveal);
        
    </script>
</head>
<body>

<div class='dashboard'>
    <div class="dashboard-nav">
        <header><a href="#!" class="menu-toggle"><i class="fas fa-bars"></i></a><a href="public_lp"
                                                                                   class="brand-logo"><center><img class="img-fluid" src="/images/logo.png" style="width:100%; height:auto; max-width:100px;"/> </center><span></span></a></header>
        <nav class="dashboard-nav-list">
            <a href="public_home" class="dashboard-nav-item"><i class="fas fa-home"></i>Home</a>
            <a href="public_ann" class="dashboard-nav-item"><i class="fas fa-bullhorn"></i>Announcements</a>
            <a href="public_acc" class="dashboard-nav-item"><i class="fas fa-check-double"></i> Accomplishments </a>
            <a href="public_his" class="dashboard-nav-item"><i class="fas fa-monument"></i>Brief History</a>
            <a href="public_officials" class="dashboard-nav-item"><i class="fas fa-sitemap"></i>Barangay Officials</a>
            <a href="public_gallery" class="dashboard-nav-item"><i class="fas fa-images"></i>Gallery</a>
            <a href="public_misvis" class="dashboard-nav-item"><i class="fas fa-bullseye"></i>Mission and Vision</a>
            <a href="EmergencyHotlines" class="dashboard-nav-item"><i class="fas fa-info"></i>Emergency Hotlines</a>
            <a href="ContactUs" class="dashboard-nav-item active"><i class="fa-solid fa-paper-plane"></i>Contact Us</a>
            
            <div class="nav-item-divider"></div>
            <div class = "mx-auto">
                <p style="color: white;"><i class="fa-solid fa-thumbs-up fa-lg"></i>&nbsp; &nbsp;Like our Facebook Page</p>
            </div>
            <div class = "mx-auto">
                <a href="https://www.facebook.com/skamagbagantransform"><i class="fa-brands fa-facebook-square fa-2xl" style="color: white;"></i>&nbsp; &nbsp; &nbsp;</a>
            </div>
        </nav>
    </div>
    <div class='dashboard-app'>
        <header class='dashboard-toolbar'><a href="#!" class="menu-toggle"><i class="fas fa-bars"></i></a></header>
        <div class='dashboard-content'>
            <div class='container'>
                <div class='card'>
                    <div class='card-header bg-warning'>
                        <h1 class="headerText">Contact Us</h1>
                    </div>

                    <div class='card-body'>
                        <div class ="row">
                           <div class="row mx-auto">
                               <div class="card mt-4" style="background-clip: unset; border: none;">
                                  <div class="container">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header" style="background-color: maroon;">
                                                <h5 class="modal-title text-white" id="staticBackdropLabel">Send a message</h5>  
                                            </div>
                                            <div class="modal-body">
                                                <div class="form">
                                                    @if (count($errors) > 0)
                                                        <div class = "alert alert-danger">
                                                            <ul>
                                                                @foreach ($errors->all() as $error)
                                                                <li>{{ $error }}</li>
                                                                @endforeach
                                                            </ul>
                                                                     </div>
                                                    @endif
                                                    @if(session()->has('message'))
                                                        <div class="alert alert-success">
                                                            {{ session()->get('message') }}
                                                        </div>
                                                    @endif
                                                    <form action = "" method = "post">
                                                        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                                            <div class="form-row">
                                                                <div class="input-group mb-3">
                                                                    <span class="input-group-text text-dark" style="background-color: #BCAAA4;">Sender's Name</span>
                                                                    <input type="text" name="sender" class="form-control">
                                                                </div>
                                                                <div class="input-group mb-3">
                                                                    <span class="input-group-text text-dark" style="background-color: #BCAAA4;">Sender's Contact Number</span>
                                                                    <input type="text" name="contact_number" class="form-control">
                                                                </div>
                                                                <div class="input-group mb-3">
                                                                    <span class="input-group-text text-dark" style="background-color: #BCAAA4;">Message</span>
                                                                    <textarea type="text" name="message" class="form-control"></textarea>
                                                                </div>
                                                            </div>     
                                                </div>
                                            </div>                                  
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-warning">Send Message</button>
                                            </div>
                                                    </form>  
                                        </div>
                                    </div>
                                </div>
                              </div>
                           </div>
                           
                        </div>
</div>
</div>
</div>
</div>
      
  <footer class="footer text-center text-white">
            <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);"> © <script type="text/JavaScript">document.write(new Date().getFullYear());</script> Copyright:
                <a class="text-white" href="" style="text-decoration: none;">Brgy. Amagbagan, Pozorrubio, Pangasinan | Developed by July Anne Rhaemonette Rosal</a>
            </div>
        </footer>

</div>
</div>

</body>
</html>