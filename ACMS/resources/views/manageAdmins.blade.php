<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/images/logo.png" type="image/x-icon">
    <title>Manage Admins</title>

    <link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <link href="/css/admin_home.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#table').DataTable();
        } );
        $(function() {
            var timeout = 3000; // in miliseconds (3*1000)
            $('.alert').delay(timeout).fadeOut(300);
        });
        const mobileScreen = window.matchMedia("(max-width: 990px )");
            $(document).ready(function () {
                $(".dashboard-nav-dropdown-toggle").click(function () {
                    $(this).closest(".dashboard-nav-dropdown")
                        .toggleClass("show")
                        .find(".dashboard-nav-dropdown")
                        .removeClass("show");
                    $(this).parent()
                        .siblings()
                        .removeClass("show");
                });
                $(".menu-toggle").click(function () {
                    if (mobileScreen.matches) {
                        $(".dashboard-nav").toggleClass("mobile-show");
                    } else {
                        $(".dashboard").toggleClass("dashboard-compact");
                    }
                });
            });
        
    </script>
</head>
<body>

<div class='dashboard'>
    <div class="dashboard-nav">
        <header><a href="#!" class="menu-toggle"><i class="fas fa-bars"></i></a><a href="admin_home"
                                                                                   class="brand-logo"><center><img class="img-fluid" src="/images/logo.png" style="width:100%; height:auto; max-width:100px;"/> </center><span></span></a></header>
        <nav class="dashboard-nav-list">
            <a href="admin_home" class="dashboard-nav-item"><i class="fas fa-home"></i>
            Home</a>
            <a href="admin_ann" class="dashboard-nav-item"><i class="fas fa-bullhorn"></i>Announcements</a>
            <a href="admin_acc" class="dashboard-nav-item"><i class="fas fa-check-double"></i> Accomplishments</a>
            <a href="admin_his" class="dashboard-nav-item"><i class="fas fa-monument"></i>Brief History</a>
            <a href="admin_officials" class="dashboard-nav-item"><i class="fas fa-sitemap"></i>Barangay Officials</a>
            <a href="admin_gallery" class="dashboard-nav-item"><i class="fas fa-images"></i>Gallery</a>
            <a href="admin_misvis" class="dashboard-nav-item"><i class="fas fa-bullseye"></i>Mission and Vision</a>
            <a href="Messages" class="dashboard-nav-item"><i class="fas fa-bell"></i><div id="count"></div>Messages ({{$count}} new)</a>

            <div class='dashboard-nav-dropdown'><a href="#" class="dashboard-nav-item dashboard-nav-dropdown-toggle active"><i class="fas fa-info"></i> Others </a>
                <div class='dashboard-nav-dropdown-menu'>
                    <a href="editLandingPage" class="dashboard-nav-dropdown-item ">Landing Page</a>
                    <a href="manageAdmins" class="dashboard-nav-dropdown-item active">Admins</a>
                    <a href="manageHotlines" class="dashboard-nav-dropdown-item">Emergency Hotlines</a>
                </div>
            </div>
            
            <div class="nav-item-divider"></div>
          
            <a href="/logout" class="dashboard-nav-item"><i class="fa-solid fa-right-from-bracket"></i></i>Logout</a>

        </nav>
    </div>
    <div class='dashboard-app'>
        <header class='dashboard-toolbar'><a href="#!" class="menu-toggle"><i class="fas fa-bars"></i></a></header>
        <div class='dashboard-content'>
            <div class='container'>
                <div class='card'>
                    <div class='card-header bg-warning'>
                        <h1>Administrator Management</h1>
                    </div>

                    <div class='card-body'>
                        <div class ="row">
                            <div class="card mt-4" style="background-clip: unset; border: none;">
                              <div class="card-header" style="background-color: #B71C1C;">
                                <h1 style="color: white; font-size: 20px; text-align: left;">Manage Records</h1>
                              </div>
                              <div class="container pt-4">
                                @if(session()->has('deleted'))
                                    <div class="alert alert-danger">
                                        {{ session()->get('deleted') }}
                                    </div>
                                 @endif
                                  <form action = "" method = "post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="table-responsive">
                                    <table class="table table-dark table-striped mt-4" id="table">
                                      <thead>
                                        <tr>
                                          <th scope="col">Admin ID</th>
                                          <th scope="col">Username</th>
                                          <th scope="col">Password</th>
                                          <th scope="col">Account Created</th>
                                          <th scope="col">Login</th>
                                          <th scope="col">Actions</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach ($admins as $admin)
                                        <tr>

                                          <th scope="row">{{ $admin->id }}</th>
                                          <td>{{ $admin->username }}</td>
                                          <td>{{ $admin->password }}</td>
                                          <td>{{ $admin->account_created }}</td>
                                          <td>{{ $admin->last_login }}</td>
                                          <td><a href = 'editAdmin/{{ $admin->id }}'><i class="fa-solid fa-edit" style="color: #FFC107"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href = 'deleteAdmin/{{ $admin->id }}'><i class="fa-solid fa-trash" style="color: #F44336"></i></a></td>
                                        </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                  </form>
                              </div>
                              <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                <a href="newAdmin"><button type="button" class="btn btn-success">Add NEW Admin</button></a>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer text-center text-white">
        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);"> © <script type="text/JavaScript">document.write(new Date().getFullYear());</script> Copyright:
            <a class="text-white" href="" style="text-decoration: none;">Brgy. Amagbagan, Pozorrubio, Pangasinan | Developed by July Anne Rhaemonette Rosal</a>
        </div>
    </footer>
</div>
      
  

</div>
</div>

</body>
</html>