<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/images/logo.png" type="image/x-icon">
    <title>Gallery</title>

    <link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <link href="/css/public_gallery.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>

    <style type="text/css">
        .box:hover{
            box-shadow: 15px 15px 13px rgba($color: #555, $alpha: 0.3);
            transition: .4s;
            transform: translate(-1%, -1%);
        }
        .box{
            position: relative;
            height: 200px;
            box-shadow: 3px 3px 10px rgba($color: #555, $alpha: 0.5);
            border-radius: 8px;
            margin-bottom: 7%;
            transition: 1s;
        }
        a:hover .box__title{
            height: 35%;
            transition: 1s;
        }
        p:{
            opacity: 1;
        }
        img{
            width: 100%;
            height: 100%;
            object-fit: cover;
            border-radius: 8px;
        }
        .box__title{
            position: absolute;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 25%;
            background-color: maroon;
            border-radius: 0 0 8px 8px;
            vertical-align: middle;
            transition: .5s;
            transition-timing-function: ease-out;
        }
        p{
            opacity: 0;
            text-align: center;
            color: #bababa;
            font-size: .8em;
            margin: 0;
            transition-delay: .2s;
            transition: .2s;
            transition-timing-function: ease-out;
        }
        h6{
            text-align: center;
            color: white;
            text-transform: uppercase;
            margin: 7% 0 0 0;
            font-size: .9em;
        }
        .add_box{
            @extend.box;
            background-color: #f9f9f9;
        }
    </style>

    <script>
        $(function() {
            var timeout = 3000; // in miliseconds (3*1000)
            $('.alert').delay(timeout).fadeOut(300);
        });
        const mobileScreen = window.matchMedia("(max-width: 990px )");
            $(document).ready(function () {
                $(".dashboard-nav-dropdown-toggle").click(function () {
                    $(this).closest(".dashboard-nav-dropdown")
                        .toggleClass("show")
                        .find(".dashboard-nav-dropdown")
                        .removeClass("show");
                    $(this).parent()
                        .siblings()
                        .removeClass("show");
                });
                $(".menu-toggle").click(function () {
                    if (mobileScreen.matches) {
                        $(".dashboard-nav").toggleClass("mobile-show");
                    } else {
                        $(".dashboard").toggleClass("dashboard-compact");
                    }
                });
            });
        
    </script>
</head>
<body>

<div class='dashboard'>
    <div class="dashboard-nav">
        <header><a href="#!" class="menu-toggle"><i class="fas fa-bars"></i></a><a href="{{url('admin_home')}}"
                                                                                   class="brand-logo"><center><img class="img-fluid" src="/images/logo.png" style="width:100%; height:auto; max-width:100px;"/> </center><span></span></a></header>
        <nav class="dashboard-nav-list"><a href="admin_home" class="dashboard-nav-item "><i class="fas fa-home"></i>
            Home </a><a
                href="admin_ann" class="dashboard-nav-item"><i class="fas fa-bullhorn"></i>Announcements
        </a><a
                href="admin_acc" class="dashboard-nav-item"><i class="fas fa-check-double"></i> Accomplishments </a>
                <a
                href="admin_his" class="dashboard-nav-item"><i class="fas fa-monument"></i>Brief History
        </a>
        <a
                href="admin_officials" class="dashboard-nav-item"><i class="fas fa-sitemap"></i>Barangay Officials
        </a>
        <a
                href="admin_gallery" class="dashboard-nav-item active"><i class="fas fa-images"></i>Gallery
        </a>
        <a
                href="admin_misvis" class="dashboard-nav-item"><i class="fas fa-bullseye"></i>Mission and Vision
        </a>
        <a href="Messages" class="dashboard-nav-item"><i class="fas fa-bell"></i><div id="count"></div>Messages ({{$count}} new)</a>
        <div class='dashboard-nav-dropdown'><a href="#!" class="dashboard-nav-item dashboard-nav-dropdown-toggle"><i class="fas fa-info"></i> Others </a>
            <div class='dashboard-nav-dropdown-menu'>
                <a href="/editLandingPage" class="dashboard-nav-dropdown-item">Landing Page</a>
                <a href="manageAdmins" class="dashboard-nav-dropdown-item">Admins</a>
                <a href="manageHotlines" class="dashboard-nav-dropdown-item">Emergency Hotlines</a>
            </div>
        </div>
          <div class="nav-item-divider"></div>
            <a
                href="/logout" class="dashboard-nav-item"><i class="fa-solid fa-right-from-bracket"></i></i>Logout
            </a>
        </nav>
    </div>
    <div class='dashboard-app'>
        <header class='dashboard-toolbar'><a href="#!" class="menu-toggle"><i class="fas fa-bars"></i></a></header>
        <div class='dashboard-content'>
            <div class='container'>
                <div class='card'>
                    <div class='card-header bg-warning'>
                        <h1 class="headerText">Gallery</h1>
                    </div>

                    <div class='card-body'>
                        <div class ="row">
                           <div class="row mx-auto">
                               <div class="card mt-4" style="background-clip: unset; border: none;">
                                  <div class="card-header" style="background-color: #B71C1C;">
                                    <h1 style="color: white; font-size: 20px; text-align: left;">Community Gallery</h1>
                                  </div>
                                  <div>
                                    <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header" style="background-color: maroon;">
                                                    <h5 class="modal-title text-white" id="staticBackdropLabel">Add Album</h5>  
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form">
                                                        @if (count($errors) > 0)
                                                         <div class = "alert alert-danger">
                                                            <ul>
                                                               @foreach ($errors->all() as $error)
                                                                  <li>{{ $error }}</li>
                                                               @endforeach
                                                            </ul>
                                                         </div>
                                                        @endif
                                                        @if(session()->has('message'))
                                                            <div class="alert alert-success">
                                                                {{ session()->get('message') }}
                                                            </div>
                                                        @endif
                                                        <form action = "" method = "post" enctype="multipart/form-data">
                                                            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                                                <div class="form-row">
                                                                    <div class="input-group mb-3">
                                                                        <span class="input-group-text text-dark" style="background-color: #BCAAA4;">Album Title</span>
                                                                        <input type="text" name="title" class="form-control">
                                                                    </div>
                                                                    <div class="input-group mb-3">
                                                                        <span class="input-group-text text-dark" style="background-color: #BCAAA4;">Album Cover</span>
                                                                        <input type="file" name="image" class="form-control">
                                                                    </div>
                                                                </div>     
                                                    </div>
                                                </div>                                  
                                                <div class="modal-footer">
                                                    <a href="admin_gallery"><button type="button" class="btn btn-secondary">Back to Gallery Albums</button></a>
                                                    <button type="submit" class="btn btn-warning">Add Photo</button>
                                                </div>
                                                        </form>  
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
</div>
</div>
</div>
      
<footer class="footer text-center text-white">
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);"> © <script type="text/JavaScript">document.write(new Date().getFullYear());</script> Copyright:
        <a class="text-white" href="" style="text-decoration: none;">Brgy. Amagbagan, Pozorrubio, Pangasinan | Developed by July Anne Rhaemonette Rosal</a>
    </div>
</footer>  

</div>
</div>

</body>
</html>