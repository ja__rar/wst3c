<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/images/logo.png" type="image/x-icon">
    <title>Gallery</title>

    <link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <link href="/css/public_gallery.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>


    <style type="text/css">
        .cardo{
            display: grid;
            grid-template-columns: 300px;
            grid-template-rows: 300px;
            overflow: hidden;
            width: 300px;
            height: 300px;
            box-shadow: 0 0 10px rgba(0,0,0);
        }
        .cardo img{
            transition: 0.3s ease-out;
        }
        .cardo:hover img{
            transform: scale(1.2, 1.2);
        }
        .box:hover{
            box-shadow: 15px 15px 13px rgba($color: #555, $alpha: 0.3);
            transition: .4s;
            transform: translate(-1%, -1%);
        }
        .box{
            position: relative;
            height: 200px;
            box-shadow: 3px 3px 10px rgba($color: #555, $alpha: 0.5);
            border-radius: 8px;
            margin-bottom: 7%;
            transition: 1s;
        }
        a:hover .box__title{
            height: 35%;
            transition: 1s;
        }
        p:{
            opacity: 1;
        }
        img{
            width: 100%;
            height: 100%;
            object-fit: cover;
            border-radius: 8px;
        }
        .box__title{
            position: absolute;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 25%;
            background-color: maroon;
            border-radius: 0 0 8px 8px;
            vertical-align: middle;
            transition: .5s;
            transition-timing-function: ease-out;
        }
        h6{
            text-align: center;
            color: white;
            text-transform: uppercase;
            margin: 7% 0 0 0;
            font-size: .9em;
        }
        .add_box{
            @extend.box;
            background-color: #f9f9f9;
        }
    </style>


    <script>
        const mobileScreen = window.matchMedia("(max-width: 990px )");
            $(document).ready(function () {
                $(".dashboard-nav-dropdown-toggle").click(function () {
                    $(this).closest(".dashboard-nav-dropdown")
                        .toggleClass("show")
                        .find(".dashboard-nav-dropdown")
                        .removeClass("show");
                    $(this).parent()
                        .siblings()
                        .removeClass("show");
                });
                $(".menu-toggle").click(function () {
                    if (mobileScreen.matches) {
                        $(".dashboard-nav").toggleClass("mobile-show");
                    } else {
                        $(".dashboard").toggleClass("dashboard-compact");
                    }
                });
            });
        
    </script>
</head>
<body>

<div class='dashboard'>
    <div class="dashboard-nav">
        <header><a href="#!" class="menu-toggle"><i class="fas fa-bars"></i></a><a href="{{url('public_lp')}}"
                                                                                   class="brand-logo"><center><img class="img-fluid" src="/images/logo.png" style="width:100%; height:auto; max-width:100px;"/> </center><span></span></a></header>
        <nav class="dashboard-nav-list">
            <a href="{{url('public_home')}}" class="dashboard-nav-item "><i class="fas fa-home"></i>Home</a>
            <a href="{{url('public_ann')}}" class="dashboard-nav-item"><i class="fas fa-bullhorn"></i>Announcements</a>
            <a href="{{url('public_acc')}}" class="dashboard-nav-item"><i class="fas fa-check-double"></i> Accomplishments</a>
            <a href="{{url('public_his')}}" class="dashboard-nav-item"><i class="fas fa-monument"></i>Brief History</a>
            <a href="{{url('public_officials')}}" class="dashboard-nav-item"><i class="fas fa-sitemap"></i>Barangay Officials</a>
            <a href="{{ url('public_gallery')}}" class="dashboard-nav-item active"><i class="fas fa-images"></i>Gallery</a>
            <a href="{{url('public_misvis')}}" class="dashboard-nav-item"><i class="fas fa-bullseye"></i>Mission and Vision</a>
            <a href="{{url('EmergencyHotlines')}}" class="dashboard-nav-item"><i class="fas fa-info"></i>Emergency Hotlines</a>
            <a href="{{url('ContactUs')}}" class="dashboard-nav-item"><i class="fas fa-paper-plane"></i>Contact Us</a>
            <div class="nav-item-divider"></div>
            <div class = "mx-auto">
                <p style="color: white;"><i class="fa-solid fa-thumbs-up fa-lg"></i>&nbsp; &nbsp;Like our Facebook Page</p>
            </div>
            <div class = "mx-auto">
            
              <a href="https://www.facebook.com/skamagbagantransform"><i class="fa-brands fa-facebook-square fa-2xl" style="color: white;"></i>&nbsp; &nbsp; &nbsp;</a>
          
            </div>
        </nav>
    </div>
    <div class='dashboard-app'>
        <header class='dashboard-toolbar'><a href="#!" class="menu-toggle"><i class="fas fa-bars"></i></a></header>
        <div class='dashboard-content'>
            <div class='container'>
                <div class='card'>
                    <div class='card-header bg-warning'>
                        <h1 class="headerText">Community Gallery</h1>
                    </div>

                    <div class='card-body'>
                        <center>
                            <div class ="row mx-auto">
                                {{ $photos->links() }}
                                    @if($count_photos == 0 )
                                        <div class="container">
                                            <p style="text-align: center; color: seagreen;">No uploaded photos yet.</p>
                                        </div>
                                    @endif
                                    @foreach ($photos as $photo)
                                        <div class="col mb-4">
                                            <a href="view/{{ $photo->id }}">
                                                <div class="cardo card text-center" style="max-width: 300px;">
                                                    <div class="card-image-wrapper">
                                                        <img src="{{ asset('images')}}/{{ $photo->image }}" class="card-img-top img-fluid" alt="..." style="max-width: 300px;max-height: 300px;">
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        </div>
         <footer class="footer text-center text-white">
            <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);"> © <script type="text/JavaScript">document.write(new Date().getFullYear());</script> Copyright:
                <a class="text-white" href="" style="text-decoration: none;">Brgy. Amagbagan, Pozorrubio, Pangasinan | Developed by July Anne Rhaemonette Rosal</a>
            </div>
        </footer>
    </div>
</div>

</body>
</html>