<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" href="/images/logo.png" type="image/x-icon">
	<title>Home</title>

	<link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
	<link href="/css/admin_home.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>

    <script>
        $(function() {
            var timeout = 3000; // in miliseconds (3*1000)
            $('.alert').delay(timeout).fadeOut(300);
        });
        function date_time(id) {
            date = new Date;
            year = date.getFullYear();
            month = date.getMonth();
            months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
            d = date.getDate();
            day = date.getDay();
            days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
            h = date.getHours();
            if (h < 10) {
                h = "0" + h;
            }
            m = date.getMinutes();
            if (m < 10) {
                m = "0" + m;
            }
            s = date.getSeconds();
            if (s < 10) {
                s = "0" + s;
            }
            result = ' ' + months[month] + ' ' + d + '' + ', ' + year + ' | ' + days[day] + ' &nbsp; &nbsp;&nbsp; ' + h + ' : ' + m + ' : ' + s;
            document.getElementById(id).innerHTML = result;
            setTimeout('date_time("' + id + '");', '1000');
            return true;
        }
        const mobileScreen = window.matchMedia("(max-width: 990px )");
            $(document).ready(function () {
                $(".dashboard-nav-dropdown-toggle").click(function () {
                    $(this).closest(".dashboard-nav-dropdown")
                        .toggleClass("show")
                        .find(".dashboard-nav-dropdown")
                        .removeClass("show");
                    $(this).parent()
                        .siblings()
                        .removeClass("show");
                });
                $(".menu-toggle").click(function () {
                    if (mobileScreen.matches) {
                        $(".dashboard-nav").toggleClass("mobile-show");
                    } else {
                        $(".dashboard").toggleClass("dashboard-compact");
                    }
                });
            });
        function toggleText() {
  
            // Get all the elements from the page
            var points = 
                document.getElementById("points");
  
            var showMoreText =
                document.getElementById("moreText");
  
            var buttonText =
                document.getElementById("textButton");
  
            // If the display property of the dots 
            // to be displayed is already set to 
            // 'none' (that is hidden) then this 
            // section of code triggers
            if (points.style.display === "none") {
  
                // Hide the text between the span
                // elements
                showMoreText.style.display = "none";
  
                // Show the dots after the text
                points.style.display = "inline";
  
                // Change the text on button to 
                // 'Show More'
                buttonText.innerHTML = "Read more";
            }
  
            // If the hidden portion is revealed,
            // we will change it back to be hidden
            else {
  
                // Show the text between the
                // span elements
                showMoreText.style.display = "inline";
  
                // Hide the dots after the text
                points.style.display = "none";
  
                // Change the text on button
                // to 'Show Less'
                buttonText.innerHTML = "Read less";
            }
        }
        
    </script>
</head>
<body>

<div class='dashboard'>
    <div class="dashboard-nav">
        <header><a href="#!" class="menu-toggle"><i class="fas fa-bars"></i></a><a href="public_lp"
                                                                                   class="brand-logo"><center><img class="img-fluid" src="/images/logo.png" style="width:100%; height:auto; max-width:100px;"/> </center><span></span></a></header>
        <nav class="dashboard-nav-list">
            <a href="" class="dashboard-nav-item active"><i class="fas fa-home"></i>
            Home</a>
            <a href="admin_ann" class="dashboard-nav-item"><i class="fas fa-bullhorn"></i>Announcements</a>
            <a href="admin_acc" class="dashboard-nav-item"><i class="fas fa-check-double"></i> Accomplishments</a>
            <a href="admin_his" class="dashboard-nav-item"><i class="fas fa-monument"></i>Brief History</a>
            <a href="admin_officials" class="dashboard-nav-item"><i class="fas fa-sitemap"></i>Barangay Officials</a>
            <a href="admin_gallery" class="dashboard-nav-item"><i class="fas fa-images"></i>Gallery</a>
            <a href="admin_misvis" class="dashboard-nav-item"><i class="fas fa-bullseye"></i>Mission and Vision</a>
            <a href="Messages" class="dashboard-nav-item"><i class="fas fa-bell"></i><div id="count"></div>Messages ({{$count}} new)</a>

            <div class='dashboard-nav-dropdown'><a href="#!" class="dashboard-nav-item dashboard-nav-dropdown-toggle"><i class="fas fa-info"></i> Others </a>
                <div class='dashboard-nav-dropdown-menu'>
                    <a href="/editLandingPage" class="dashboard-nav-dropdown-item">Landing Page</a>
                    <a href="manageAdmins" class="dashboard-nav-dropdown-item">Admins</a>
                    <a href="manageHotlines" class="dashboard-nav-dropdown-item">Emergency Hotlines</a>
                </div>
            </div>
            
            <div class="nav-item-divider"></div>
          
            <a href="/logout" class="dashboard-nav-item"><i class="fa-solid fa-right-from-bracket"></i></i>Logout</a>

        </nav>
    </div>
    <div class='dashboard-app'>
        <header class='dashboard-toolbar'><a href="#!" class="menu-toggle"><i class="fas fa-bars"></i></a></header>
        <div class='dashboard-content'>
            <div class='container'>
                <div class='card'>
                    <div class='card-header bg-warning'>
                        <h1>Quick Stats</h1>
                    </div>

                    <div class='card-body'>
                        <div class ="row">
                            @foreach ($stats as $stat)
                                <div class="col-xl-4 col-lg-6">
                                  <div class="card card-stats mb-4 mb-xl-0">
                                    <div class="card-body">
                                      <div class="row">
                                        <div class="col">
                                          <h5 class="card-title text-uppercase text-muted mb-0">{{ $stat->place }}</h5>
                                          <span class="h2 font-weight-bold mb-0">{{ $stat->population }}</span>
                                        </div>
                                        <div class="col-auto">
                                          <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                            <i class="fas fa-chart-bar"></i>
                                          </div>
                                        </div>
                                      </div>
                                      <p class="mt-3 mb-0 text-muted text-sm">
                                        <span class="text-success mr-2">Population as of {{ $stat->year }}</span>
                                      </p>
                                      <p class="mt-3 mb-0 text-muted text-sm">
                                        <a href="edit_stats/{{ $stat->id }}"><button type="button" class="btn btn-warning">Edit Details</button></a>
                                      </p>
                                    </div>
                                  </div>
                                </div>
                            @endforeach
                            <div class="card mt-4" style="background-clip: unset; border: none;">
                              <div class="card-header" style="background-color: #B71C1C;">
                                <h1 style="color: white; font-size: 20px; text-align: left;">Public Health Advisory<a href="addAdvisory"><button type="button" class="btn btn-light" style="float: right;">Add Advisory</button></a></h1>
                                
                              </div>
                              <div class="card-body mx-auto">
                                 {{ $HAs->links() }}
                                <div class="row justify-content-center">
                                    @foreach ($HAs as $HA)
                                    <div class="card" style="width: 18rem; margin-right: 10px; margin-bottom: 10px;">
                                      <img class="card-img-top" src="{{ asset('images')}}/{{ $HA->image }}" alt="Card image cap">
                                      <div class="card-body">
                                        <h5 class="card-title"><b>{{ $HA->title }}</b></h5>
                                        <p class="card-text" style="text-align: justify;">{{ $HA->subtitle }}</span></p>
                                        <a href="edit_HA/{{ $HA->id }}"><button type="button" class="btn btn-warning">Edit Details</button></a>
                                        <a href="admin_home/{{ $HA->id }}"><button type="button" class="btn btn-danger">Delete</button></a>
                                      </div>
                                    </div>
                                    @endforeach
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer text-center text-white">
            <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);"> © <script type="text/JavaScript">document.write(new Date().getFullYear());</script> Copyright:
                <a class="text-white" href="" style="text-decoration: none;">Brgy. Amagbagan, Pozorrubio, Pangasinan | Developed by July Anne Rhaemonette Rosal</a>
            </div>
        </footer>
    </div>
</div>

</body>
</html>