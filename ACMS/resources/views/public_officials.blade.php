<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/images/logo.png" type="image/x-icon">
    <title>Brgy. Officials</title>

    <link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <link href="/css/public_officials.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>


    <style type="text/css">
        .hakdog{
          background-image: linear-gradient(#D50000 0%, #FFAB00 100%);
        }
        .grr{
          background-image: linear-gradient(#D50000 0%, #FFAB00 100%);
        }
        .grr .card-text{
          color:  black;
        }
        .grr:hover{
          transform: scale(1.05);
          box-shadow: 10px 10px 15px rgba(0,0,0,0.3);
          transition: transform .5s ease-out 0s;
        }
        .reveal {
          position: relative;
          opacity: 0;
        }
        .reveal.active {
          opacity: 1;
        }
        .active.fade-bottom {
          animation: fade-bottom 1s ease-in;
        }
        .active.fade-left {
          animation: fade-left 1s ease-in;
        }
        .active.fade-right {
          animation: fade-right 1s ease-in;
        }

        @keyframes fade-bottom {
          0% {
            transform: translateY(50px);
            opacity: 0;
          }
          100% {
            transform: translateY(0);
            opacity: 1;
          }
        }
        @keyframes fade-left {
          0% {
            transform: translateX(-100px);
            opacity: 0;
          }
          100% {
            transform: translateX(0);
            opacity: 1;
          }
        }
        @keyframes fade-right {
          0% {
            transform: translateX(100px);
            opacity: 0;
          }
          100% {
            transform: translateX(0);
            opacity: 1;
          }
        }
    </style>


    <script>
        function reveal() {
          var reveals = document.querySelectorAll(".reveal");

          for (var i = 0; i < reveals.length; i++) {
            var windowHeight = window.innerHeight;
            var elementTop = reveals[i].getBoundingClientRect().top;
            var elementVisible = 150;

            if (elementTop < windowHeight - elementVisible) {
              reveals[i].classList.add("active");
            } else {
              reveals[i].classList.remove("active");
            }
          }
        }

        window.addEventListener("scroll", reveal);
        function date_time(id) {
            date = new Date;
            year = date.getFullYear();
            month = date.getMonth();
            months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
            d = date.getDate();
            day = date.getDay();
            days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
            h = date.getHours();
            if (h < 10) {
                h = "0" + h;
            }
            m = date.getMinutes();
            if (m < 10) {
                m = "0" + m;
            }
            s = date.getSeconds();
            if (s < 10) {
                s = "0" + s;
            }
            result = ' ' + months[month] + ' ' + d + '' + ', ' + year + ' | ' + days[day] + ' &nbsp; &nbsp;&nbsp; ' + h + ' : ' + m + ' : ' + s;
            document.getElementById(id).innerHTML = result;
            setTimeout('date_time("' + id + '");', '1000');
            return true;
        }
        const mobileScreen = window.matchMedia("(max-width: 990px )");
            $(document).ready(function () {
                $(".dashboard-nav-dropdown-toggle").click(function () {
                    $(this).closest(".dashboard-nav-dropdown")
                        .toggleClass("show")
                        .find(".dashboard-nav-dropdown")
                        .removeClass("show");
                    $(this).parent()
                        .siblings()
                        .removeClass("show");
                });
                $(".menu-toggle").click(function () {
                    if (mobileScreen.matches) {
                        $(".dashboard-nav").toggleClass("mobile-show");
                    } else {
                        $(".dashboard").toggleClass("dashboard-compact");
                    }
                });
            });
        
    </script>
</head>
<body>

<div class='dashboard'>
    <div class="dashboard-nav">
        <header><a href="#!" class="menu-toggle"><i class="fas fa-bars"></i></a><a href="public_lp"
                                                                                   class="brand-logo"><center><img class="img-fluid" src="/images/logo.png" style="width:100%; height:auto; max-width:100px;"/> </center><span></span></a></header>
        <nav class="dashboard-nav-list">
            <a href="public_home" class="dashboard-nav-item"><i class="fas fa-home"></i>Home</a>
            <a href="public_ann" class="dashboard-nav-item"><i class="fas fa-bullhorn"></i>Announcements</a>
            <a href="public_acc" class="dashboard-nav-item"><i class="fas fa-check-double"></i> Accomplishments </a>
            <a href="public_his" class="dashboard-nav-item"><i class="fas fa-monument"></i>Brief History</a>
            <a href="public_officials" class="dashboard-nav-item active"><i class="fas fa-sitemap"></i>Barangay Officials</a>
            <a href="public_gallery" class="dashboard-nav-item"><i class="fas fa-images"></i>Gallery</a>
            <a href="public_misvis" class="dashboard-nav-item"><i class="fas fa-bullseye"></i>Mission and Vision</a>
            <a href="EmergencyHotlines" class="dashboard-nav-item"><i class="fas fa-info"></i>Emergency Hotlines</a>
            
            <div class="nav-item-divider"></div>
            <div class = "mx-auto">
                <p style="color: white;"><i class="fa-solid fa-thumbs-up fa-lg"></i>&nbsp; &nbsp;Like our Facebook Page</p>
            </div>
            <div class = "mx-auto">
                <a href="https://www.facebook.com/skamagbagantransform"><i class="fa-brands fa-facebook-square fa-2xl" style="color: white;"></i>&nbsp; &nbsp; &nbsp;</a>
            </div>
        </nav>
    </div>
    <div class='dashboard-app'>
        <header class='dashboard-toolbar'><a href="#!" class="menu-toggle"><i class="fas fa-bars"></i></a></header>
        <div class='dashboard-content'>
            <div class='container'>
                <div class='card'>
                    <div class='card-header bg-warning'>
                        <h1 class="headerText">Sangguniang Barangay Officials</h1>
                    </div>

                    <div class='card-body'>
                        <div class="row justify-content-center">
                            <div class="hakdog card p-4 mb-4 text-white">
                                <img class= "an1" src="{{ asset('images')}}/{{ $group->image }}" class="card-img" alt="...">
                            </div>
                            @foreach ($officials as $official)
                                <div class="grr card reveal fade-bottom mb-3 shadow bg-body-rounded m-4" style="max-width: 300px; background-color: maroon;">
                                    <div class="row g-0">
                                        <div class="col-md-12 pt-2">
                                            <img src="{{ asset('images')}}/{{ $official->image }}" class="img-fluid rounded-start" alt="...">
                                        </div>
                                        <div class="col col-md-12">
                                            <div class="card-body text-center text-white">
                                                <h5 class="card-title">{{ $official->name }}</h5>
                                                <p class="card-text">{{ $official->position }}</p>
                                                <p class="card-text"><b>Contact No: {{ $official->phone }} </b></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                         </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer text-center text-white">
            <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);"> © <script type="text/JavaScript">document.write(new Date().getFullYear());</script> Copyright:
                <a class="text-white" href="" style="text-decoration: none;">Brgy. Amagbagan, Pozorrubio, Pangasinan | Developed by July Anne Rhaemonette Rosal</a>
            </div>
        </footer>
    </div>
</div>

</body>
</html>