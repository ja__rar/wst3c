<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/images/logo.png" type="image/x-icon">
    <title>Brief History</title>

    <link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <link href="/css/public_acc.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>


    <script>
        function date_time(id) {
            date = new Date;
            year = date.getFullYear();
            month = date.getMonth();
            months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
            d = date.getDate();
            day = date.getDay();
            days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
            h = date.getHours();
            if (h < 10) {
                h = "0" + h;
            }
            m = date.getMinutes();
            if (m < 10) {
                m = "0" + m;
            }
            s = date.getSeconds();
            if (s < 10) {
                s = "0" + s;
            }
            result = ' ' + months[month] + ' ' + d + '' + ', ' + year + ' | ' + days[day] + ' &nbsp; &nbsp;&nbsp; ' + h + ' : ' + m + ' : ' + s;
            document.getElementById(id).innerHTML = result;
            setTimeout('date_time("' + id + '");', '1000');
            return true;
        }
        const mobileScreen = window.matchMedia("(max-width: 990px )");
            $(document).ready(function () {
                $(".dashboard-nav-dropdown-toggle").click(function () {
                    $(this).closest(".dashboard-nav-dropdown")
                        .toggleClass("show")
                        .find(".dashboard-nav-dropdown")
                        .removeClass("show");
                    $(this).parent()
                        .siblings()
                        .removeClass("show");
                });
                $(".menu-toggle").click(function () {
                    if (mobileScreen.matches) {
                        $(".dashboard-nav").toggleClass("mobile-show");
                    } else {
                        $(".dashboard").toggleClass("dashboard-compact");
                    }
                });
            });
        
    </script>
</head>
<body>

<div class='dashboard'>
    <div class="dashboard-nav">
        <header><a href="#!" class="menu-toggle"><i class="fas fa-bars"></i></a><a href="public_lp"
                                                                                   class="brand-logo"><center><img class="img-fluid" src="/images/logo.png" style="width:100%; height:auto; max-width:100px;"/> </center><span></span></a></header>
        <nav class="dashboard-nav-list">
            <a href="admin_home" class="dashboard-nav-item "><i class="fas fa-home"></i>
            Home</a>
            <a href="admin_ann" class="dashboard-nav-item"><i class="fas fa-bullhorn"></i>Announcements</a>
            <a href="admin_acc" class="dashboard-nav-item"><i class="fas fa-check-double"></i> Accomplishments</a>
            <a href="" class="dashboard-nav-item active"><i class="fas fa-monument"></i>Brief History</a>
            <a href="admin_officials" class="dashboard-nav-item"><i class="fas fa-sitemap"></i>Barangay Officials</a>
            <a href="admin_gallery" class="dashboard-nav-item"><i class="fas fa-images"></i>Gallery</a>
            <a href="admin_misvis" class="dashboard-nav-item"><i class="fas fa-bullseye"></i>Mission and Vision</a>
            <a href="Messages" class="dashboard-nav-item"><i class="fas fa-bell"></i><div id="count"></div>Messages ({{$count}} new)</a>

            <div class='dashboard-nav-dropdown'><a href="#!" class="dashboard-nav-item dashboard-nav-dropdown-toggle"><i class="fas fa-info"></i> Others </a>
                <div class='dashboard-nav-dropdown-menu'>
                    <a href="/editLandingPage" class="dashboard-nav-dropdown-item">Landing Page</a>
                    <a href="manageAdmins" class="dashboard-nav-dropdown-item">Admins</a>
                    <a href="manageHotlines" class="dashboard-nav-dropdown-item">Emergency Hotlines</a>
                </div>
            </div>
          
            <div class="nav-item-divider"></div>
            <a href="/logout" class="dashboard-nav-item"><i class="fa-solid fa-right-from-bracket"></i></i>Logout</a>
        </nav>
    </div>
    <div class='dashboard-app'>
        <header class='dashboard-toolbar'><a href="#!" class="menu-toggle"><i class="fas fa-bars"></i></a></header>
        <div class='dashboard-content'>
            <div class='container'>
                <div class='card'>
                    <div class='card-header bg-warning'>
                        <h1 class="headerText">Brief History</h1>
                    </div>

                    <div class='card-body'>
                        <div class ="row">
                           <div class="row mx-auto">
                               <div class="card mt-4" style="background-clip: unset; border: none;">
                                  <div class="card-header" style="background-color: #B71C1C;">
                                    <h1 style="color: white; font-size: 20px; text-align: left;">AMAGBAGAN – Anatomy of a Historic Barangay</h1>
                                    <p style="color: lightgrey; font-size: 12px; text-align: left;">By: Mel Jovellanos</p>
                                  </div>
                                  <div class="card-body mx-auto">
                                            <div class="col col-md-12">
                                             
                                                <p class="card-text" style="text-align: justify; text-indent: 50px;">Barangay Amagbagan, one of the most developed among the 34 barangays in the Municipality of Pozorrubio, was elevated and simultaneously inaugurated as a barrio of nearby San Jacinto town on March 12, 1834. As a barrio or barangay therefore, it is now 168 years old. But long before it became a barrio, it was a sitio of San Jacinto starting during the late 1700’s. It is safe to conclude that Barangay Amagbagan’s colorful history encompasses more than 200 years antedating independent Pozorrubio by 38 solid years.</p>

                                                <p style="text-align: justify; text-indent: 50px;">The sitio was named after the first great leader among the intrepid Pangasinense settlers who cleared the then thickly-forested area between what are now barangays Talogtog and Amagbagan. From oral traditions, Claris the warrior, was indeed the epitome of a great leader – a man for all time: handsome, tall and heavily built, hard-working, deeply religious and a born leader who led the settlers in driving out the wild inhabitants of the area. The non-Christians were successfully conquered by Claris and his fellow Pangasinenses who all came from the Mother of San Jacinto. Thus the territory, vanquished from the animistic tribesmen of the northern mountains, became known as Claris, in honor of their great leader.</p>

                                                <p style="text-align: justify; text-indent: 50px;">In 1834, by virtue of the approval of the petition of the Gobernadorcillo of San Jacinto, the Spanish Governor-General in Manila, Don Pascual Enrile, approved the elevation of the sitio of Claris into a barrio. The Vicar of San Jacinto, Fray Domingo Naval, presided over the inauguration ceremonies. The barrio’s population at that time was only around 700 people.</p>

                                                <p style="text-align: justify; text-indent: 50px;">In 1839 or five years later, the foundation of a Catholic Chapel in Barrio Claris were laid down. It was finished in 1842. The priests of San Jacinto regularly went to Claris, traversing the ancient dirt road via Lobong and Nantangalan which were also still sitios at that time, to say mass, to visit and encourage the pioneering settlers and to conduct religious instructions. When the petition of Barrio Claris was finally approved by Governor-General Carlos Maria dela Torre y Navarrada to secede from San Jacinto and become an independent town, the Spanish Governor-General reportedly chose the name of the new town, naming it Pozorrubio, in honor of the Governor-General’s hometown in Spain which was also Pozorrubio (in the Spanish province of Cuenca).</p>

                                                <p style="text-align: justify; text-indent: 50px;">The legend whereby two Spaniards discovered a well with reddish drinking water (Pozorrubio or red well) remains to be that – an imaginative legend. Thus the name Claris was obliterated and change into Pozorrubio. In the Pangasinan dialect, the place became known as the place where they replaced, relocated, after physically removing practically everything and bringing them somewhere else.</p>

                                                <p style="text-align: justify; text-indent: 50px;">Ten years after the inauguration of the new town, it was abandoned in favor of a new site was not prone to flooding, farther to the north, in a barangay called Cablong. But while the new town of Pozorrubio expanded, evolving eventually into a 34-barangay municipality, the former site, the barrio of Claris, became known as Amagbagan. From a population of 700 when it was elevated it into a barrio in 1834, the population has since then increased to 2,450 people with 358 households.</p>

                                            </div>
                                  </div>
                              </div>
                           </div>
                        </div>
</div>
</div>
</div>
</div>
      
<footer class="footer text-center text-white">
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);"> © <script type="text/JavaScript">document.write(new Date().getFullYear());</script> Copyright:
        <a class="text-white" href="" style="text-decoration: none;">Brgy. Amagbagan, Pozorrubio, Pangasinan | Developed by July Anne Rhaemonette Rosal</a>
    </div>
</footer>  

</div>
</div>

</body>
</html>