<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/images/logo.png" type="image/x-icon">
    <title>Gallery</title>

    <link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <link href="/css/public_gallery.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>


    <style type="text/css">
        .box:hover{
            box-shadow: 15px 15px 13px rgba($color: #555, $alpha: 0.3);
            transition: .4s;
            transform: translate(-1%, -1%);
        }
        .box{
            position: relative;
            height: 200px;
            box-shadow: 3px 3px 10px rgba($color: #555, $alpha: 0.5);
            border-radius: 8px;
            margin-bottom: 7%;
            transition: 1s;
        }
        a:hover .box__title{
            height: 35%;
            transition: 1s;
        }
        p:{
            opacity: 1;
        }
        img{
            width: 100%;
            height: 100%;
            object-fit: cover;
            border-radius: 8px;
        }
        .box__title{
            position: absolute;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 25%;
            background-color: maroon;
            border-radius: 0 0 8px 8px;
            vertical-align: middle;
            transition: .5s;
            transition-timing-function: ease-out;
        }
        
        h6{
            text-align: center;
            color: white;
            text-transform: uppercase;
            margin: 7% 0 0 0;
            font-size: .9em;
        }
        .add_box{
            @extend.box;
            background-color: #f9f9f9;
        }
    </style>


    <script>
        function date_time(id) {
            date = new Date;
            year = date.getFullYear();
            month = date.getMonth();
            months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
            d = date.getDate();
            day = date.getDay();
            days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
            h = date.getHours();
            if (h < 10) {
                h = "0" + h;
            }
            m = date.getMinutes();
            if (m < 10) {
                m = "0" + m;
            }
            s = date.getSeconds();
            if (s < 10) {
                s = "0" + s;
            }
            result = ' ' + months[month] + ' ' + d + '' + ', ' + year + ' | ' + days[day] + ' &nbsp; &nbsp;&nbsp; ' + h + ' : ' + m + ' : ' + s;
            document.getElementById(id).innerHTML = result;
            setTimeout('date_time("' + id + '");', '1000');
            return true;
        }
        const mobileScreen = window.matchMedia("(max-width: 990px )");
            $(document).ready(function () {
                $(".dashboard-nav-dropdown-toggle").click(function () {
                    $(this).closest(".dashboard-nav-dropdown")
                        .toggleClass("show")
                        .find(".dashboard-nav-dropdown")
                        .removeClass("show");
                    $(this).parent()
                        .siblings()
                        .removeClass("show");
                });
                $(".menu-toggle").click(function () {
                    if (mobileScreen.matches) {
                        $(".dashboard-nav").toggleClass("mobile-show");
                    } else {
                        $(".dashboard").toggleClass("dashboard-compact");
                    }
                });
            });
        
    </script>
</head>
<body>

<div class='dashboard'>
    <div class="dashboard-nav">
        <header>
            <a href="public_lp" class="menu-toggle"><i class="fas fa-bars"></i></a>
            <a href="public_lp" class="brand-logo"><center><img class="img-fluid" src="/images/logo.png" style="width:100%; height:auto; max-width:100px;"/> </center><span></span></a>
        </header>
        <nav class="dashboard-nav-list">
            <a href="public_home" class="dashboard-nav-item"><i class="fas fa-home"></i>Home</a>
            <a href="public_ann" class="dashboard-nav-item"><i class="fas fa-bullhorn"></i>Announcements</a>
            <a href="public_acc" class="dashboard-nav-item"><i class="fas fa-check-double"></i> Accomplishments </a>
            <a href="public_his" class="dashboard-nav-item"><i class="fas fa-monument"></i>Brief History</a>
            <a href="public_officials" class="dashboard-nav-item"><i class="fas fa-sitemap"></i>Barangay Officials</a>
            <a href="public_gallery" class="dashboard-nav-item active"><i class="fas fa-images"></i>Gallery</a>
            <a href="public_misvis" class="dashboard-nav-item"><i class="fas fa-bullseye"></i>Mission and Vision</a>
            <a href="EmergencyHotlines" class="dashboard-nav-item"><i class="fas fa-info"></i>Emergency Hotlines</a>
            <a href="ContactUs" class="dashboard-nav-item"><i class="fas fa-paper-plane"></i>Contact Us</a>
            
            <div class="nav-item-divider"></div>
            <div class = "mx-auto">
                <p style="color: white;"><i class="fa-solid fa-thumbs-up fa-lg"></i>&nbsp; &nbsp;Like our Facebook Page</p>
            </div>
            <div class = "mx-auto">
                <a href="https://www.facebook.com/skamagbagantransform"><i class="fa-brands fa-facebook-square fa-2xl" style="color: white;"></i>&nbsp; &nbsp; &nbsp;</a>
            </div>
        </nav>
    </div>
    <div class='dashboard-app'>
        <header class='dashboard-toolbar'><a href="#!" class="menu-toggle"><i class="fas fa-bars"></i></a></header>
        <div class='dashboard-content'>
            <div class='container'>
                <div class='card'>
                    <div class='card-header bg-warning'>
                        <h1 class="headerText">Gallery</h1>
                    </div>

                    <div class='card-body'>
                        <div class ="row">
                           <div class="row mx-auto">
                               <div class="card mt-4" style="background-clip: unset; border: none;">
                                  <div class="card-header" style="background-color: #B71C1C;">
                                    <h1 style="color: white; font-size: 20px; text-align: left;">Community Gallery</h1>
                                  </div>
                                    <div class="container ">
                                        <div class="row pt-4">
                                            @foreach ($albums as $album)
                                            <div class="col-xl-3 col-lg-4 col-sm-6">
                                              <div class="box">
                                                <a href="public_viewAlbum/{{$album->album_id}}">
                                                  <img src="{{ asset('images')}}/{{ $album->album_cover }}" alt="">
                                                  <div class="box__title">
                                                    <h6>{{$album->album_title}}</h6>
                                                    <h6>{{$album->date_created}}</h6>
                                                  </div>
                                                </a>
                                              </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer text-center text-white">
            <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);"> © <script type="text/JavaScript">document.write(new Date().getFullYear());</script> Copyright:
                <a class="text-white" href="" style="text-decoration: none;">Brgy. Amagbagan, Pozorrubio, Pangasinan | Developed by July Anne Rhaemonette Rosal</a>
            </div>
        </footer>
    </div>
</div>

</body>
</html>