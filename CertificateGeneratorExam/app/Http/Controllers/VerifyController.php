<?php

namespace App\Http\Controllers;

use App\Models\Certificate;
use App\Models\Seminar;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class VerifyController extends Controller
{
    //

    public function displayVerification(Request $request){
        if(session()->has('username')){
            return redirect('/generator');
        }
        else{
            $details = [];
            return view('verify')->with(['details'=> $details]);
        }  
    }

    public function verify(Request $request){

        $request->validate([
            'token' => 'required|min:32|max:32',
        ]);

        $_token = $request->input('token');

        $certificates = Certificate::where([
            'token' => $_token
        ])->get();

        if(count($certificates)>0){

            foreach($certificates as $certificate){
                $seminar_id = $certificate->seminar_id;
                $awardee = $certificate->awardee;
                $img_path = $certificate->img_path;
            }

            $seminar = Seminar::where([
                'id' => $seminar_id
            ])->get();

            if(count($seminar)>0){

                foreach($seminar as $seminars){
                    $title = $seminars->title;
                    $sdate = $seminars->sdate;
                    $edate = $seminars->edate;
                    $venue = $seminars->venue;
                }

                $details = [
                    "token" => $_token,
                    "name" => $awardee,
                    "seminar" => $title,
                    "venue" => $venue,
                    "edate" => $edate,
                    "sdate" => $sdate,
                    "img_path" => $img_path
                ];

                // dd($details);

                
                Alert::success('Verification Success', 'Your certificate is valid.');
                return view('verify', ['details' => $details]);

            }else{
                Alert::error('Verification Failed', 'Seminar not found');
                return view('verify', ['details' => []]);
            }

        }else{
            Alert::error('Verification Failed', 'Participant not found');
            return view('verify', ['details' => []]);
        }
    }
}
