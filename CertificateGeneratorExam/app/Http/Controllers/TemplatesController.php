<?php

namespace App\Http\Controllers;

use App\Models\BGTemplates;
use App\Models\Seminar;
use App\Models\Template;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class TemplatesController extends Controller
{
    //

    public function displayTemplate(Request $request){
        $templates = Template::all();
        $bgtemplates = BGTemplates::orderBy('id', 'desc')->get();
        $seminars = Seminar::orderBy('title', 'asc')->where([
            ['status', '=', '1']
        ])->get();
    

        // $seminars = Seminar::all();

        if ($request->session()->has('username')) {

            return view('templates', ['templates' => $templates, 'seminars' => $seminars, 'bgtemplates' => $bgtemplates]);
        }else{
            return redirect('admin');
        }

    }

    public function previewTemplate(Request $request){
        $request->validate([
            'template' => 'required',
            'seminar' => 'required|unique:templates,seminar_name',
            'contents' => 'required',
            'signame1' => 'required',
            'pos1' => 'required',
            'esig1' => 'required'
        ]);

        $templates = Template::all();
        $bgtemplates = BGTemplates::orderBy('id', 'desc')->get();
        $seminars = Seminar::where([
            ['status', '=', '1']
        ])->get();


        if ($request->session()->has('username')) {

            return view('templates', ['templates' => $templates, 'seminars' => $seminars, 'bgtemplates' => $bgtemplates]);
        }else{
            return redirect('admin');
        }
    }

    public function saveTemplate(Request $request, $seminar_name, $id){
        //move previews/newpreview to templates folder

        $ldate = date('dH-i-s');

        $filename = "templatesFolder/seminar_".$ldate.".png";



       $insert = [
        "seminar_name" => $seminar_name,
        "img_path" => $filename,
        "template" => $id,
       ];


       Template::create($insert);
       File::move(public_path('previews/newpreview.png'), public_path($filename));

       Alert::success('Success Template', 'Template created Successfully');
       return redirect('templates');

    }

    public function deleteTemplate(Request $request, $id){

        $template = Template::where([
            'id' => $request->id
        ])->get();
    
    
        if(count($template)>0){

            foreach($template as $templates){
                $img = $templates->img_path;
            }

            unlink($img);
            Template::where("id", $request->id)->delete();
            Alert::success('Success Template', 'Template deleted Successfully');
            return redirect('templates');
        }else{
            Alert::error('Template Error', 'An error occurred while deleting the template');
            return redirect('templates');
        }

    }


    public function addBGImage(Request $request){
        $request->validate([
            'temp_name' => 'required|unique:bg_templates,temp_name',
            'temp_image' => 'required|max:2000',
        ]);

        $tempname = $request->input('temp_name');


        if($request->hasFile('temp_image')){
            $originName = $request->file('temp_image')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('temp_image')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
            $request->file('temp_image')->move(public_path('assets/templates'), $fileName);
            
            $insert = [
                "temp_name" => $tempname,
                "imgpath" => "assets/templates/".$fileName,
            ];
    
            BGTemplates::create($insert);
    
            Alert::success('Success Template', 'Background Template added Successfully');

        }else{
            Alert::error('Template Error', 'An error occurred while uploading the template');
        }

        return redirect('templates');
    }
}
