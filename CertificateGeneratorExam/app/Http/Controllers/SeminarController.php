<?php

namespace App\Http\Controllers;

use App\Models\Seminar;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
class SeminarController extends Controller
{
    //

    public function displaySeminar(Request $request){
        $seminars = Seminar::all();

        if ($request->session()->has('username')) {

            return view('seminars', ['seminars' => $seminars]);
        }else{
            return redirect('admin');
        }

    }

    public function addSeminar(Request $request){
        $request->validate([
            'title' => 'required',
            'sdate' => 'required|after:tomorrow',
            'edate' => 'required|after:tomorrow',
            'venue' => 'required',
        ]);

        
        $title = $request->input('title');
        $sdate = $request->input('sdate');
        $edate = $request->input('edate');
        $venue = $request->input('venue');


        if($sdate<=$edate){
            //insert

           $insert = [
           "title" => $title,
           "sdate" => $sdate,
           "edate" => $edate,
           "venue" => $venue
           ];

           Seminar::create($insert);
           Alert::success('Success Seminar', 'Seminar Added Successfully');
           
          
       }else{
           //error
           Alert::error('Error Seminar', 'There\'s an error inserting the seminar.');
       }

       return redirect('seminars');
    }

    public function displayEditSeminar(Request $request){
        $seminars = Seminar::where('id', '=', $request->id)->get();

        if ($request->session()->has('username')) {

            return view('editseminars', ['seminars' => $seminars]);
        }else{
            return redirect('admin');
        }

    }

    public function updateSeminar(Request $request){
        $request->validate([
            'title' => 'required',
            'sdate' => 'required|after:tomorrow',
            'edate' => 'required|after:tomorrow',
            'venue' => 'required',
        ]);

        $title = $request->input('title');
        $sdate = $request->input('sdate');
        $edate = $request->input('edate');
        $status = $request->input('status');
        $venue = $request->input('venue');


        if($sdate<=$edate){
            //insert

            $update = [
                "title" => $title,
                "sdate" => $sdate,
                "edate" => $edate,
                "status" => $status,
                "venue" => $venue
            ];
    
            Seminar::where("id", $request->id)->update($update);
            Alert::success('Seminar updated', 'Seminars updated Successfully');
           
          
       }else{
           //error
           Alert::error('Error Seminar', 'There\'s an error updating the seminar.');
       }

       return redirect('seminars');

    }

    public function deleteSeminar(Request $request){    
    //Seminar $seminar 
    //    dd($seminar);
       
        $seminar = Seminar::where([
            'id' => $request->id
        ])->get();


        if(count($seminar)>0){
            Seminar::where("id", $request->id)->delete();
            Alert::success('Seminar Deleted', 'Seminars deleted Successfully');
            return redirect('seminars');
        }else{
            Alert::error('Seminar Error', 'An error occurred while deleting the seminar');
            return redirect('seminars');
        }

    }

    public function updateStatus(Request $request){

        $seminar = Seminar::where([
            'id' => $request->id
        ])->get();

        if(count($seminar)>0){
            $update = [
                "status" => "0"
            ];
    
            Seminar::where("id", $request->id)->update($update);
            Alert::success('Seminar updated', 'Seminars updated Successfully');
            return redirect('seminars');
        }else{
            Alert::error('Seminar Error', 'An error occurred while deleting the seminar');
            return redirect('seminars');
        }

        

    }
}
