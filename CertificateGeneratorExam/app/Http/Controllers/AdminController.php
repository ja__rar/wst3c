<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Certificate;
use App\Models\Seminar;
use App\Models\Template;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;

class AdminController extends Controller
{
    //
    public function displayHome(Request $request){
        // $seminars = Seminar::all();

        $seminars = Template::all();

        if ($request->session()->has('username')) {
            return view('generator', ['seminars' => $seminars]);
        }else{
            return redirect('admin');
        }
    }

    public function saveCertificate(Request $request, $seminar, $awardee, $token){


        $seminar = Seminar::where([
            'title' => $seminar
        ])->get();

        if(count($seminar)>0){
            //save


            foreach($seminar as $seminars){
                $seminar_id = $seminars->id;
            }

            $ldate = date('dH-i-s');

            $filename = "certificates/certificate_".$ldate.".png";

            $insert = [
                "seminar_id" => $seminar_id,
                "token" => $token,
                "awardee" => $awardee,
                "img_path" => $filename,
                "creator" => $request->session()->get('email')
            ];

            Certificate::create($insert);
            File::move(public_path('previews/newcertificate.png'), public_path($filename));


            // return redirect('generator')->with("success-generator","Certificate generated successfully!");
            Alert::success('Certificate Success', 'Certificate Generated successfully!');
            return redirect('generator');

        }else{
            Alert::error('Certificate Failed', 'Seminar doesn\'t exist.');
            return redirect('generator');
            // return redirect('generator')->withErrors("Seminar doesn't exist.");
        }
    }

    public function displayAdmin(Request $request){
        $admins = Admin::all();

        if ($request->session()->has('username')) {
            return view('admins', ['admins' => $admins]);
        }else{
            return redirect('admin');
        }
    }

    public function adminInsert(Request $request){

        $request->validate([
            'email' => 'required|unique:admin,email',
            'username' => 'required|min:7',
            'password' => 'required|max:10|min:7',
        ]);

        
        $email = $request->input('email');
        $username = $request->input('username');
        $password = md5($request->input('password'));

        $insert = [
            "email" => $email,
            "username" => $username,
            "password" => $password
        ];

        Admin::create($insert);
        Alert::success('Success Admin', 'Admin Account Added Successfully');

        return redirect('/admins')->with("success-services", "Admin Account Added successfully.");
    }

    public function adminDelete(Request $request, $id){

        $admins = Admin::where([
            'admin_id' => $id
        ])->get();


        if(count($admins)>0){
            Admin::where("admin_id", $id)->delete();
            Alert::success('Admin Deleted', 'Admin Account deleted Successfully');
        }else{
            Alert::error('Admin Error', 'An error occurred while deleting the account');
        }

        return redirect('/admins')->with("success-services", "Admin Account deleted successfully.");
    }
}
