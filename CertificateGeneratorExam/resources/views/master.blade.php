<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="icon" href="/assets/fav.png" type="image/x-icon">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
   <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
  

    {{-- Title --}}
    @yield('title')

    {{-- Links --}}
    @yield('links')

</head>
<body>

 

  <script>
    function display_ct6() {
    var x = new Date()
    var ampm = x.getHours( ) >= 12 ? ' PM' : ' AM';
    hours = x.getHours( ) % 12;
    hours = hours ? hours : 12;
    var x1=x.getMonth() + 1+ "/" + x.getDate() + "/" + x.getFullYear(); 
    x1 = x1 + " - " +  hours + ":" +  x.getMinutes() + ":" +  x.getSeconds() + ":" + ampm;
    document.getElementById('ct6').innerHTML = x1;
    display_c6();
    }
    
    function display_c6(){
    var refresh=1000; // Refresh rate in milli seconds
    mytime=setTimeout('display_ct6()',refresh)
    }
    display_c6()
    </script>

    
@yield('content')
   



    <div class="container-fluid mt-5" style="background-color: #082b54">
        <div class="row pt-5">
            <div class="col-sm-6 mx-auto">
             
                <div class="row py-3 d-flex justify-content-center text-center text-white">
                    <span class="text-white">
                        &copy; {{ date('Y') }} {{ config('app.name')}}. @lang('All rights reserved.') 
                    </span>        
     
                </div>
                <div class="row py-3 d-flex justify-content-center text-center ">
                    <div class="col-sm-12">
                        <a href="https://www.facebook.com/bnjpcson" class="p-3"><img src="{{asset('assets/developers/benjie.jpg')}}" alt="" class="img-fluid rounded-circle" style="width: 50px;"></a>
                        <a href="https://www.facebook.com/No.0ne.kn0ws.mee" class="p-3"><img src="{{asset('assets/developers/jonel.png')}}" alt="" class="img-fluid rounded-circle" style="width: 50px;"></a>
                        <a href="https://facebook.com/katorinagami" class="p-3"><img src="{{asset('assets/developers/kat.png')}}" alt="" class="img-fluid rounded-circle" style="width: 50px;"></a>
                        <a href="https://www.facebook.com/Windeeeeeeeeeeeel" class="p-3"><img src="{{asset('assets/developers/windel.png')}}" alt="" class="img-fluid rounded-circle" style="width: 50px;"></a>
                        <a href="https://www.facebook.com/Louie.Catabay18" class="p-3"><img src="{{asset('assets/developers/louie.png')}}" alt="" class="img-fluid rounded-circle" style="width: 50px;"></a>
                        
                        
                        
                    </div>
                    <div class="col-sm-12 py-3">
                        
                        <a href="https://www.facebook.com/itsmeeeecmllll" class="p-3"><img src="{{asset('assets/developers/camila.png')}}" alt="" class="img-fluid rounded-circle" style="width: 50px;"></a>
                        <a href="https://www.facebook.com/rhaemonette" class="p-3"><img src="{{asset('assets/developers/july.png')}}" alt="" class="img-fluid rounded-circle" style="width: 50px;"></a>
                        <a href="https://www.facebook.com/jroger24" class="p-3"><img src="{{asset('assets/developers/jroger.png')}}" alt="" class="img-fluid rounded-circle" style="width: 50px;"></a>
                        <a href="https://www.facebook.com/freakaphrodite23" class="p-3"><img src="{{asset('assets/developers/demii.png')}}" alt="" class="img-fluid rounded-circle" style="width: 50px;"></a>
                        
                    </div>
                </div>
                

            </div>
        </div>
        
    </div>

    <script>
      function date_time(id) {
        date = new Date;
        year = date.getFullYear();
        month = date.getMonth();
        months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        d = date.getDate();
        day = date.getDay();
        days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
        h = date.getHours();
        if (h < 10) {
        h = "0" + h;
        }
        m = date.getMinutes();
        if (m < 10) {
        m = "0" + m;
        }
        s = date.getSeconds();
        if (s < 10) {
        s = "0" + s;
        }
        result = ' ' + months[month] + ' ' + d + '' + ', ' + year + ' | ' + days[day] + ' &nbsp; &nbsp;&nbsp; ' + h + ' : ' + m + ' : ' + s;
        document.getElementById(id).innerHTML = result;
        setTimeout('date_time("' + id + '");', '1000');
        return true;
      }
    </script>
</body>
</html>