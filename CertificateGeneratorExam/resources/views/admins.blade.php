@extends('master')

@section('title')
  <title>Admins</title>
  
  <link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
  <link href="/css/A_home.css" rel="stylesheet" type="text/css">

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

  {{-- Datatables --}}
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" ></script>
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
  {{-- Datatables --}}


  <title>Seminars</title>

  <style type="text/css">
    .hello{
      font-size: 30px;
    }
    .active::before{
      background: white;
    }
    .active{
      color: #D84315;
    }
  </style>
  @stack('css-external')
  @stack('css-internal')
@endsection

@section('content')


<header class="header" style="height:130px;">
  <img src="/assets/header.png" alt="" srcset="" class="img-fluid" style="width:400px;">
  <button class="header__btn_open-topnav header__btn"><span class="icon-menu-open"></span></button>
  <ul class="topnav topnav_mobile_show my-auto">
    <button class="header__btn_close-topnav header__btn"><span class="icon-menu-close"></span></button>
    <li class="topnav__item">
      <a href="seminars/" class="btn topnav__link">Seminars</a>
    </li>
    <li class="topnav__item">
      <a href="templates/" class="btn topnav__link">Templates</a>
    </li>
    <li class="topnav__item">
      <a href="generator/" class="btn topnav__link">Generator</a>
    </li>
    <li class="topnav__item">
      <a href="certs/" class="btn topnav__link">Certificates</a>
    </li>
    <li class="topnav__item">
      <a href="/admins" class="btn topnav__link active">Admins</a>
    </li>
    <li class="topnav__item">
      <div class="dropdown">
        <button class="btn topnav__link dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
          My Profile
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
          <li><a class="dropdown-item" href="{{asset('manual/manual.pdf');}}">User's Manual</a></li>
          <li><a href="/logout" class="dropdown-item"><i class="fa fa-sign-out text-danger" aria-hidden="true"></i> Logout</a></li>
        </ul>
      </div>
    </li>
  </ul>
  <div class="p-1 position-absolute bottom-0 end-0 text-end pe-3">
    <span id='ct6'></span>
  </div>
</header>



<div class="card mb-3 col-sm-6 mx-auto mt-5">
  {{-- <img class="card-img-top" src="..." alt="Card image cap"> --}}
  
  @if ($errors->any())
  <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
      </ul>
  </div>
  @endif
  @if(Session::has('success-seminar'))
    <div class="alert alert-success">
        {{ Session::get('success-seminar') }}
        @php
            Session::forget('success-seminar');
        @endphp
    </div>
  @endif
  
  
  
  <div class="card-body">
    <div class = "container col-sm-12">
    <form action="/admins" method = "post" enctype="multipart/form-data">
          <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
          <div class="form-row">
           
  
            <div class="input-group mb-3">
              <span class="input-group-text text-dark fw-bold">Email</span>
              <input type="email" name="email" class="form-control">
            </div>
            <div class="input-group mb-3">
              <span class="input-group-text text-dark fw-bold">Username</span>
              <input type="text" name="username" class="form-control">
            </div>
            <div class="input-group mb-3">
              <span class="input-group-text text-dark fw-bold">Password</span>
              <input type="password" name="password" class="form-control">
            </div>

            <center>
              <button type="submit" class="btn mx-auto text-white" style="background-color: #082b54">Add Admin</button>
            </center>
          </div>
      </form>
  </div>
  </div>
  </div>




<script>
$(function(){
      $("#adminsTable").DataTable();
  });
</script>

@push('javascript-internal')
<script>
      $(document).ready(function() {
         //Event: delete
         $("form[role='alert']").submit(function(event) {
            event.preventDefault();
            Swal.fire({
            title: $(this).attr('alert-title'),
            text:  $(this).attr('alert-text'),
            icon: 'warning',
            allowOutsideClick: false,
            showCancelButton: true,
            cancelButtonText: $(this).attr('alert-btn-cancel'),
            reverseButtons: true,
            confirmButtonText: $(this).attr('alert-btn-yes'),
         }).then((result) => {
            if (result.isConfirmed) {
               //process ng deleting 
              event.target.submit();
             
            }
         });

         });

      });
      
  </script>
@endpush



<div class="container">
<div class="row my-5">
  <div class="col-sm-12 mx-auto">
    <table class="table w-100 mx-auto" id="adminsTable">
      <thead class="bg-dark text-white">
        <tr>
          <th>ID</th>
          <th>Email</th>
          <th>Username</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($admins as $admin)
        <tr>
          <td>{{$admin->admin_id}}</td>
          <td>{{$admin->email}}</td>
          <td>{{$admin->username}}</td>
          <td class="text-center">

            {{-- <a href="/admins/delete/{{$admin->admin_id}}" class="btn btn-danger"><i class=" fas fa-trash"></i></a> --}}
              <form action="/admins/delete/{{$admin->admin_id}}" role="alert" method="POST" 
              alert-title="Delete Admin Account" alert-text="Do you really want to delete this account?"
              alert-btn-cancel="Cancel" alert-btn-yes="Yes">
                <div class="text-center">
                  @csrf
                 
                  <button type="submit" class="btn btn-sm btn-danger"> <i class=" fas fa-trash"></i>
                  </button>
                </div>
              </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
</div>

@include('sweetalert::alert')

@push('javascript-external')
@endpush

@stack('javascript-external')
@stack('javascript-internal')



@push('css-external')
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2-bootstrap4.min.css') }}">
@endpush

@push('javascript-external')
<script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
@endpush


@endsection


