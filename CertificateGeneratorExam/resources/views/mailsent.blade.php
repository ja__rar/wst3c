@extends('master')

@section('title')
  
  <link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
  <link href="/css/A_home.css" rel="stylesheet" type="text/css">

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

  {{-- Datatables --}}
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" ></script>
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
  {{-- Datatables --}}


  <title>Emails</title>

  <style type="text/css">
    .hello{
      font-size: 30px;
    }
    .active::before{
      background: white;
    }
    .active{
      color: #D84315;
    }
  </style>
  @stack('css-external')
  @stack('css-internal')
@endsection

@section('content')



<header class="header" style="height:130px;">
  <img src="/assets/header.png" alt="" srcset="" class="img-fluid" style="width:400px;">
  <button class="header__btn_open-topnav header__btn"><span class="icon-menu-open"></span></button>
  <ul class="topnav topnav_mobile_show my-auto">
    <button class="header__btn_close-topnav header__btn"><span class="icon-menu-close"></span></button>
    <li class="topnav__item">
      <a href="{{route('seminars')}}" class="btn topnav__link">Seminars</a>
    </li>
    <li class="topnav__item">
      <a href="{{route('templates')}}" class="btn topnav__link">Templates</a>
    </li>
    <li class="topnav__item">
      <a href="{{route('generator')}}" class="btn topnav__link">Generator</a>
    </li>
    <li class="topnav__item">
      <a href="{{route('certs')}}" class="btn topnav__link">Certificates</a>
    </li>
    <li class="topnav__item">
      <a href="{{route('admins')}}" class="btn topnav__link">Admins</a>
    </li>
    <li class="topnav__item">
      <div class="dropdown">
        <button class="btn topnav__link dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
          Menu
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
          <li><a class="dropdown-item" href="{{asset('manual/manual.pdf');}}">User's Manual</a></li>
          <li><a href="{{route('logout')}}" class="dropdown-item"><i class="fa fa-sign-out text-danger" aria-hidden="true"></i> Logout</a></li>
        </ul>
      </div>
    </li>
  </ul>
  <div class="p-1 position-absolute bottom-0 end-0 text-end pe-3">
    <span id='ct6'></span>
  </div>
</header>





<script>
$(function(){
      $("#adminsTable").DataTable();
  });
</script>

@push('javascript-internal')
<script>
      $(document).ready(function() {
         //Event: delete
         $("form[role='alert']").submit(function(event) {
            event.preventDefault();
            Swal.fire({
            title: $(this).attr('alert-title'),
            text:  $(this).attr('alert-text'),
            icon: 'warning',
            allowOutsideClick: false,
            showCancelButton: true,
            cancelButtonText: $(this).attr('alert-btn-cancel'),
            reverseButtons: true,
            confirmButtonText: $(this).attr('alert-btn-yes'),
         }).then((result) => {
            if (result.isConfirmed) {
               //process ng deleting 
              event.target.submit();
             
            }
         });

         });

      });
      
  </script>
@endpush



<div class="container my-5">
  <div class="row mb-5">
    <h1 class="display-3 text-center">Sent Emails</h1>
  </div>
<div class="row my-5">
  <div class="col-sm-12 mx-auto">
    <table class="table w-100 mx-auto" id="adminsTable">
      <thead class="bg-dark text-white">
        <tr>
          <th>ID</th>
          <th>Email</th>
          <th>Date Sent</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($emails as $email)
        <tr>
          <td>{{$email->id}}</td>
          <td>{{$email->email}}</td>
          <td>{{$email->created_at}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
</div>

@include('sweetalert::alert')

@push('javascript-external')
@endpush

@stack('javascript-external')
@stack('javascript-internal')



@push('css-external')
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2-bootstrap4.min.css') }}">
@endpush

@push('javascript-external')
<script src="{{ asset('vendor/select2/js/select2.min.js') }}"></script>
@endpush


@endsection


