-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 04, 2022 at 03:38 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `certs_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `certificates`
--

CREATE TABLE `certificates` (
  `id` int(11) NOT NULL,
  `seminar_id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `awardee` varchar(255) NOT NULL,
  `img_path` varchar(255) NOT NULL,
  `creator` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `certificates`
--

INSERT INTO `certificates` (`id`, `seminar_id`, `token`, `awardee`, `img_path`, `creator`, `created_at`, `updated_at`) VALUES
(17, 12, '04a2b0e18dbf96ee431285134e4792a7', 'Benjie Pecson', 'certificates/certificate_2919-17-24.png', 'admin123@gmail.com', '2022-06-29 11:17:24', '2022-06-29 11:17:24'),
(18, 12, 'a5034a6b21432c189687d17f64649350', 'Benjie Pecson', 'certificates/certificate_2919-35-07.png', 'admin123@gmail.com', '2022-06-29 11:35:07', '2022-06-29 11:35:07'),
(19, 13, 'b165fed1584f10b1404094e709f36c9c', 'Katrina Urbano', 'certificates/certificate_2922-05-49.png', 'admin123@gmail.com', '2022-06-29 14:05:49', '2022-06-29 14:05:49'),
(20, 12, '6e69c9d022d91f49268430ab43366bb3', 'Jonel Nagtalon', 'certificates/certificate_2922-09-38.png', 'admin123@gmail.com', '2022-06-29 14:09:38', '2022-06-29 14:09:38'),
(21, 13, '2e7f57fae4303b9b7eb64f31fada5c91', 'Louie Catabay', 'certificates/certificate_2922-31-54.png', 'admin123@gmail.com', '2022-06-29 14:31:54', '2022-06-29 14:31:54'),
(22, 12, '842266d6aa0cde2417fb5c26768c5c51', 'Windel Rodillas', 'certificates/certificate_3005-26-25.png', 'admin123@gmail.com', '2022-06-29 21:26:25', '2022-06-29 21:26:25'),
(23, 13, '3d8e472cf3ba9b5ba527c4c724299133', 'Camila Delos Santos', 'certificates/certificate_3005-27-34.png', 'benjiepecson@yahoo.com', '2022-06-29 21:27:34', '2022-06-29 21:27:34'),
(24, 14, 'c392430bc3205a6ee7e409538184573c', 'July Rosal', 'certificates/certificate_3006-44-14.png', 'benjiepecson@yahoo.com', '2022-06-29 22:44:14', '2022-06-29 22:44:14'),
(25, 15, '11e2afda8f9b1607502f6493d13b3870', 'Rhaemonette', 'certificates/certificate_0110-12-23.png', 'admin123@gmail.com', '2022-07-01 02:12:23', '2022-07-01 02:12:23'),
(26, 15, '11e2afda8f9b1607502f6493d13b3870', 'Rhaemonette', 'certificates/certificate_0110-15-37.png', 'admin123@gmail.com', '2022-07-01 02:15:37', '2022-07-01 02:15:37'),
(27, 12, 'e57381ce365bfcda3184631918810040', 'Rhaemonette', 'certificates/certificate_0110-17-08.png', 'admin123@gmail.com', '2022-07-01 02:17:08', '2022-07-01 02:17:08'),
(28, 23, 'b1691d0160606436f60024d059863947', 'Benjie Pecson', 'certificates/certificate_0413-12-18.png', 'admin123@gmail.com', '2022-07-04 05:12:18', '2022-07-04 05:12:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `certificates`
--
ALTER TABLE `certificates`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `certificates`
--
ALTER TABLE `certificates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
