<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
    // Route::Get('/',function()  
    // {  
    //     return view('student');  
    // }); 

    // Route::get('student/details',function()  
    // {  
    //     $url=route('student.details');
    //     $name="July Anne Rhaemonette A. Rosal";
    //     $section="IT-3C";  
    //     return "Name: $name"."   ". "Section: $section";

    // })->name('student.details'); 
 
  Route::Get('/',function()  
  {  
    return view('item');  
  });  

  Route::get('item/item',function() 
  {  
        $url=route('item.item');  
        return view('item');
    })->name('item.item');

  Route::get('item/order',function() 
  {  
        $url=route('item.order');  
        return view('order');
    })->name('item.order');

  Route::get('item/customer',function() 
  {  
        $url=route('item.customer');  
        return view('customer');
    })->name('item.customer');

  Route::get('item/odetails',function() 
  {  
        $url=route('item.odetails');  
        return view('odetails');
    })->name('item.odetails');
