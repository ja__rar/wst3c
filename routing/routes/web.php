<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::Get('/',function()  
{  
    return view('student');  
}); 

Route::get('student/details',function()  
{  
    $url=route('student.details');  
    return $url;  
})->name('student.details');  

  // Route::get('/', function()  
  // {  
  //   return "This is a home page";   
  // }  
  // );  
  
  // Route::get('/about', function()  
  // {  
  //   return "This is a about us page";   
  // }  
  // );  

  // Route::get('/contact', function()  
  // {  
  //  return "This is a contact us page";   
  // }  
  // );  

  Route::get('/post/{id?}/{name?}', function($id=null, $name=null)  
  {  
    // return "id number is : ". $id; 
    return "id number is : ". $id ." ".$name;     
  }  
  );  

  // Route::get('/post/{id}/{name}', function($id,$name)  
  // {  
  //   return "id number is : ". $id ." ".$name;   
  // }  
  // );  

  // Route::get('user/{name?}', function ($name='ganda') {  
  //     return $name;  
  // });  

  // Route::Get('/',function()  
  // {  
  //   return view('student');  
  // });  
  // Route::get('student/details',function()  
  // {  
  //   // $url=route('student.details');
  //   $url="<a href='#'>test moko</a>";
  //  return $url;  
  // })->name('student.details');  




