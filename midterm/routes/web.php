<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
  Route::Get('/',function()  
  {  
    return view('item');  
  });  

  Route::get('item/item/{itemNum?}/{itemName?}/{itemPrice?}',function($itemNum,$itemName,$itemPrice) 
  {  
        $url=route('item.item');  
        // return view('item');
        // return view("item", ["itemNum"=>$itemNum], ["itemPrice"=>$itemPrice], ["itemName"=>$itemName]);
        return view('item')->with(['itemNum'=>$itemNum,'itemName'=>$itemName,'itemPrice'=>$itemPrice]);
    })->name('item.item');

  Route::get('item/order/{cusID?}/{cusName?}/{oNum?}/{oDate?}',function($cusID,$cusName,$oNum,$oDate) 
  {  
        $url=route('item.order');  
        // return view('order');
        return view("order")->with(['cusID'=>$cusID, 'cusName'=>$cusName, 'oNum'=>$oNum, 'oDate'=>$oDate]);
    })->name('item.order');

  Route::get('item/customer/{cusID?}/{cusName?}/{cusAdd?}/{cusAge?}',function($cusID,$cusName,$cusAdd,$cusAge) 
  {  
        $url=route('item.customer');  
         return view("customer")->with(['cusID'=>$cusID, 'cusName'=>$cusName, 'cusAdd'=>$cusAdd, 'cusAge'=>$cusAge]);
    })->name('item.customer');


  Route::get('item/odetails/{transNo?}/{oNo?}/{itemID?}/{itemName?}/{itemPrice?}/{itemQuantity?}',function($transNo,$oNo,$itemID,$itemName,$itemPrice,
$itemQuantity) 
  {  
        $url=route('item.odetails');  
        // return view('odetails');
        return view("odetails")->with(['transNo'=>$transNo, 'oNo'=>$oNo, 'itemID'=>$itemID, 'itemName'=>$itemName, 'itemPrice'=>$itemPrice, 'itemQuantity'=>$itemQuantity,]);
    })->name('item.odetails');
