<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AddApointmentController extends Controller
{
    public function appointmentForm() {
    return view('home');
    }

    public function insert(Request $request) {
    $this->validate($request, [
    'start_time' => 'date_format:H:i',
    'finish_time' => 'date_format:H:i|after:start_time',
    ]);


    $client = $request->input('client');
    $start_time = $request->input('start_time');
    $finish_time = $request->input('finish_time');
    $doctor = $request->input('doctor');

    $t1 = strtotime($start_time);
    $t2 = strtotime($finish_time);

    $interval = $t2-$t1;
    $hours = $interval / ( 60 * 60 );


    $max = DB::table('appointments')->where('doctor',$doctor)->count();
    $existingClient = DB::table('appointments')->where('client',$client)->count();



    if($max < 4 && $hours <= 2 && $existingClient < 1){
        $data=array('client'=>$client,'start_time'=>$start_time,'finish_time'=>$finish_time,'doctor'=>$doctor);
        DB::table('appointments')->insert($data);
        echo "Request Appointment is added.<br/>";
        echo '<a href = "/home">Click Here</a> to go back.<br/><br/>';       
    }
    if($max >= 4){
        echo "Fully booked for $doctor. See you next time.<br/>";
        echo '<a href = "/home">Click Here</a> to go back.<br/><br/>';
    }
    if($hours > 2){
        echo "Only allowed maximum of 2 hours.<br/>";
        echo '<a href = "/home">Click Here</a> to go back.<br/><br/>';
    }
    if($existingClient >= 1){
        echo "Repeated appointment request for $client.<br/>";
        echo '<a href = "/home">Click Here</a> to go back.<br/><br/>';
    }
        

    }
}
