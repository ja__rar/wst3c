<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    public function insertform() {
    return view('register');
    }

    public function insert(Request $request) {
    $this->validate($request,[
        'firstname'=>'required',
        'lastname'=>'required',
        'email'=>'required|email|unique:clients,email',
        'username'=>'required|max:10|min:7|unique:clients,username',
        'password'=>'required|max:10|min:7'
    ]);
    
    $firstname = $request->input('firstname');
    $lastname = $request->input('lastname');
    $email = $request->input('email');
    $username = $request->input('username');
    $password = $request->input('password');

    $user = DB::table('clients')->where('email',$email)->first();

    if(!$user){
        $data=array('firstname'=>$firstname,'lastname'=>$lastname,'email'=>$email,'username'=>$username,'password'=>$password);
        DB::table('clients')->insert($data);
        echo "Record inserted successfully.<br/>";
        return redirect('/')->with('success', "Account successfully registered.");       
    }
    if($user){
        echo "Already has a record!<br/>";
        echo '<a href = "/register">Click Here</a> to go back.';
    }
    
        
    }
}
