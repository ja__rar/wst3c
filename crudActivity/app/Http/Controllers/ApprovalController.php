<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ApprovalController extends Controller
{
    public function list() {
    $requests = DB::select('select * from appointments');
    return view('admin',['requests'=>$requests]);
    }

    public function approvedList() {
    $requests = DB::select('select * from approved');
    return view('approvedrequests',['requests'=>$requests]);
    }

    public function destroy($id) {
        DB::delete('delete from appointments where req_id = ?',[$id]);
        echo "Request Appointment declined.<br/>";
        echo '<a href = "/admin">Click Here</a> to go back.';
    }

    public function deleteApproved($id) {
        DB::delete('delete from approved where approved_id = ?',[$id]);
        echo "Successfully removed an appointment.<br/>";
        echo '<a href = "/approvedrequests">Click Here</a> to go back.';
    }

    public function approve($id) {
       $data = DB::select('select * from appointments where req_id =?',[$id]);

       $client = DB::table('appointments')->where('req_id',$id)->pluck('client');
       $appStartTime = DB::table('appointments')->where('req_id',$id)->pluck('start_time');
       $appFinishTime = DB::table('appointments')->where('req_id',$id)->pluck('finish_time');
       $doctor = DB::table('appointments')->where('req_id',$id)->pluck('doctor');

       $client = $client[0];
       $time_start = $appStartTime[0];
       $time_finish =  $appFinishTime[0];
       $doctor = $doctor[0];

       $data1=array('req_id'=>$id,'client'=>$client,'time_start'=>$time_start,'time_finish'=>$time_finish,'doctor'=>$doctor);

       $max = DB::table('approved')->where('doctor',$doctor)->count();

       if($max < 4){
        DB::table('approved')->insert($data1);
        DB::delete('delete from appointments where req_id = ?',[$id]);

        echo "Request Appointment is Approved.<br/>";
        echo '<a href = "/admin">Click Here</a> to go back.';
       }
       else{
        DB::delete('delete from appointments where req_id = ?',[$id]);
        echo "Request Appointment is Declined. Maximum Quota Reached!<br/>";
        echo '<a href = "/admin">Click Here</a> to go back.';
       }

          
    
    }
}
