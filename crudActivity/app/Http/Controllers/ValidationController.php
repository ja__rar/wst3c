<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class ValidationController extends Controller
{
    public function showform() {
      return view('index');
   }
   public function validateform(Request $request) {
      $this->validate($request,[
         'username'=>'required|max:10|min:7',
         'password'=>'required|max:10|min:7'
      ]);

      $clientUsername = DB::table('clients')->where('username',$request->username)->first();
      $clientPassword = DB::table('clients')->where('password',$request->password)->first();


      if($clientUsername && $clientPassword){
         $username = DB::table('clients')->where('username',$request->username)->pluck('username');
         $password = DB::table('clients')->where('password',$request->password)->pluck('password');

         if($username[0] == "admin123" && $password[0] == "admin123"){
            $data = $request->input();
            $request->session()->put('username',$data['username']);
            return redirect('/admin')->with('success', "Let's go, admin.");
         }
         else{
            $data = $request->input();
            $request->session()->put('username',$data['username']);
            return redirect('/home')->with('success', "Account successfully logged in.");
         }
         
      }
      else{
         echo "No account registered. Sorry.<br/>";
         echo '<a href = "/">Click Here</a> to go back.';
      }
      
   }


}
