@extends('layouts.master')
 
@section('meta')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/images/petlogo.png" type="image/x-icon">
    <title>Appointment</title>

    <link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>

    <style>
     body{
        background-image: url(images/bg2.png);
        background-size: auto;
  
      }

    .board-container {
      max-width: 70%;
      border-radius:.55rem!important;
      padding:40px;box-sizing:border-box
    }

    .header {
      overflow: hidden;
      background-color: black;
      padding: 20px 10px;   
    }

    /* Style the header links */
    .header a {
      float: left;
      color: black;
      text-align: center;
      padding: 12px;
      text-decoration: none;
      font-size: 18px;
      line-height: 25px;
      border-radius: 4px;
    }

    /* Style the logo link (notice that we set the same value of line-height and font-size to prevent the header to increase when the font gets bigger */
    .header a.logo {
      font-size: 25px;
      font-weight: bold;
      color:  white;
    }

    /* Float the link section to the right */
    .header-right {
      color: white;
      float: right;
    }

    </style>

    <script>
        function date_time(id) {
            date = new Date;
            year = date.getFullYear();
            month = date.getMonth();
            months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
            d = date.getDate();
            day = date.getDay();
            days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
            h = date.getHours();
            if (h < 10) {
                h = "0" + h;
            }
            m = date.getMinutes();
            if (m < 10) {
                m = "0" + m;
            }
            s = date.getSeconds();
            if (s < 10) {
                s = "0" + s;
            }
            result = ' ' + months[month] + ' ' + d + '' + ', ' + year + ' | ' + days[day] + ' &nbsp; &nbsp;&nbsp; ' + h + ' : ' + m + ' : ' + s;
            document.getElementById(id).innerHTML = result;
            setTimeout('date_time("' + id + '");', '1000');
            return true;
        }

    </script>

@endsection
 
@section('sidebar')
    @parent
 
    <p>This is appended to the master sidebar.</p>
@endsection

@section('header')
    <div class="header sticky-top">
      <a href="#default" class="logo"><img class="img-fluid" src="/images/petlogo.png" style="width:100%; height:auto; max-width:100px;"/>Julalay's 24-hr Pet Center (Admin)</a>
      <div class="header-right">
        <p class="hidden-sm hidden-xs" id="date_time"></p>
        <script type="text/javascript">window.onload = date_time('date_time');</script>
        <div>
            <p></p>
        </div>
        <div>
            <p><b>A Loving Home for Pets</b></p>
        </div>
        <div>
            <p>Grooming and Healthcare</p>
        </div>
      </div>
    </div>
@endsection
 
@section('content')
  <div>
    <div class="">
      <div class="row board-container mx-auto">
        <div class="board mx-auto">
          <div class="container-fluid d-flex justify-content-between bg-info">
            <div class="p-2"><i class="fa-solid fa-user-check fa-2xl"></i></div>
            <div class="p-2"><b>Welcome, {{session('username')}}</b></div>
            <a class="btn btn-danger m-2" href="/logout" role="button">Logout</a>
          </div>
        </div> 
     </div>
    </div>

    <div class="container">
      <h2 class="text-center m-4">Appointment Requests</h2><br/>
      <form action = "/add" method = "post" enctype="multipart/form-data">
        @csrf
        <table class="table table-dark table-striped">
          <thead>
            <tr>
              <th scope="col">Request ID</th>
              <th scope="col">Pet Name</th>
              <th scope="col">Start Time</th>
              <th scope="col">Finish Time</th>
              <th scope="col">Doctor/Attendant</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($requests as $request)
            <tr>

              <th scope="row">{{ $request->req_id }}</th>
              <td>{{ $request->client }}</td>
              <td>{{ $request->start_time }}</td>
              <td>{{ $request->finish_time }}</td>
              <td>{{ $request->doctor }}</td>
              <td><a href = 'approve/{{ $request->req_id }}'><i class="fa-solid fa-circle-check" style="color: #FFC107"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href = 'delete/{{ $request->req_id }}'><i class="fa-solid fa-trash" style="color: #F44336"></i></a></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </form>
      <div class="d-grid gap-2 d-md-flex justify-content-md-end">
        <a class="btn btn-success m-2 end" href="/approvedrequests" role="button">See Approved List</a>
      </div>
         
    </div>

  </div>
@endsection