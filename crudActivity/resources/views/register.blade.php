@extends('layouts.master')
 
@section('meta')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/images/petlogo.png" type="image/x-icon">
    <title>Appointment</title>

    <link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>

    <style>

    body{
        background-image: url("images/bg.png");
    }

    .header {
      overflow: hidden;
      background-color: #BCAAA4;
      background-image: linear-gradient(180deg, #FF8F00, #FFD54F);
      padding: 20px 10px;
      
    }

    /* Style the header links */
    .header a {
      float: left;
      color: black;
      text-align: center;
      padding: 12px;
      text-decoration: none;
      font-size: 18px;
      line-height: 25px;
      border-radius: 4px;
    }

    /* Style the logo link (notice that we set the same value of line-height and font-size to prevent the header to increase when the font gets bigger */
    .header a.logo {
      font-size: 25px;
      font-weight: bold;
    }

    /* Float the link section to the right */
    .header-right {
      float: right;
    }

    </style>

    <script>
        function date_time(id) {
            date = new Date;
            year = date.getFullYear();
            month = date.getMonth();
            months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
            d = date.getDate();
            day = date.getDay();
            days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
            h = date.getHours();
            if (h < 10) {
                h = "0" + h;
            }
            m = date.getMinutes();
            if (m < 10) {
                m = "0" + m;
            }
            s = date.getSeconds();
            if (s < 10) {
                s = "0" + s;
            }
            result = ' ' + months[month] + ' ' + d + '' + ', ' + year + ' | ' + days[day] + ' &nbsp; &nbsp;&nbsp; ' + h + ' : ' + m + ' : ' + s;
            document.getElementById(id).innerHTML = result;
            setTimeout('date_time("' + id + '");', '1000');
            return true;
        }

    </script>

@endsection
 
@section('sidebar')
    @parent
 
    <p>This is appended to the master sidebar.</p>
@endsection

@section('header')
    <div class="header sticky-top">
      <a href="#default" class="logo"><img class="img-fluid" src="/images/petlogo.png" style="width:100%; height:auto; max-width:100px;"/>Julalay's Pet Center</a>
      <div class="header-right">
        <p class="hidden-sm hidden-xs" id="date_time"></p>
        <script type="text/javascript">window.onload = date_time('date_time');</script>
        <div>
            <p></p>
        </div>
        <div>
            <p><b>A Loving Home for Pets</b></p>
        </div>
        <div>
            <p>Grooming and Healthcare</p>
        </div>
      </div>
    </div>
@endsection
 
@section('content')
<div>

<section class="vh-100">
  <div class="container h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-lg-12 col-xl-11">
        <div class="card text-black" style="border-radius: 25px;">
          <div class="card-body p-md-5">
            <div class="row justify-content-center">
              <div class="col-md-10 col-lg-6 col-xl-5 order-2 order-lg-1">
                <center>
                <a href="#default" class="logo"><img class="img-fluid" src="/images/petlogo.png" style="width:100%; height:auto; max-width:100px;"/></a>
                </center>
                <p class="text-center h1 fw-bold mx-1 mx-md-4 mt-4">Register</p>
                <p class="text-center mb-5 mx-1 mx-md-4 ">Hello, furparent! Create your account now.</p>

                 @if (count($errors) > 0)
                     <div class = "alert alert-danger">
                        <ul>
                           @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                           @endforeach
                        </ul>
                     </div>
                @endif

                <form class="mx-1 mx-md-4" action = "/create" method = "post">
                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                  <div class="d-flex flex-row align-items-center mb-4">
                    <i class="fas fa-user fa-lg me-3 fa-fw"></i>
                    <div class="form-outline flex-fill mb-0">
                      <input type="text" class="form-control" name="firstname"/>
                      <label class="form-label">Firstname</label>
                    </div>
                  </div>
                  <div class="d-flex flex-row align-items-center mb-4">
                    <i class="fas fa-user fa-lg me-3 fa-fw"></i>
                    <div class="form-outline flex-fill mb-0">
                      <input type="text" class="form-control" name="lastname"/>
                      <label class="form-label">Lastname</label>
                    </div>
                  </div>
                  <div class="d-flex flex-row align-items-center mb-4">
                    <i class="fas fa-at fa-lg me-3 fa-fw"></i>
                    <div class="form-outline flex-fill mb-0">
                      <input type="email" class="form-control" name="email"/>
                      <label class="form-label">Email</label>
                    </div>
                  </div>

                  <div class="d-flex flex-row align-items-center mb-4">
                    <i class="fas fa-user fa-lg me-3 fa-fw"></i>
                    <div class="form-outline flex-fill mb-0">
                      <input type="text" class="form-control" name="username"/>
                      <label class="form-label">Username</label>
                    </div>
                  </div>

                  <div class="d-flex flex-row align-items-center mb-4">
                    <i class="fas fa-lock fa-lg me-3 fa-fw"></i>
                    <div class="form-outline flex-fill mb-0">
                      <input type="password" class="form-control" name="password"/>
                      <label class="form-label">Password</label>
                    </div>
                  </div>

                  <div class="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
                    <button type="submit" value="Register" class="btn btn-primary btn-sm">Register</button>
                  </div>

                </form>
                <div class="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
                    <a href="index"><button type="button" class="btn btn-warning btn-sm"><i class="fa-solid fa-arrow-left"></i>&nbsp;&nbsp;Back to Login</button></a>
                </div>

              </div>
              <div class="col-md-10 col-lg-6 col-xl-7 d-flex align-items-center order-1 order-lg-2">

                <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-registration/draw1.webp"
                  class="img-fluid" alt="Sample image">

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</div>
@endsection