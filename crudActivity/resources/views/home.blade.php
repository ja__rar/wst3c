
@extends('layouts.master')
 
@section('meta')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/images/petlogo.png" type="image/x-icon">
    <title>Appointment</title>

    <link href="https://fonts.googleapis.com/css2?family=Poppins&family=Raleway&family=Work+Sans&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>

    <style>

    .container {
        max-width: 70%;
        border-radius:.55rem!important;
    }

    body{
        background-image: url(images/bg2.png);
        background-size: auto;
  
      }
    .header {
      overflow: hidden;
      background-color: #FFD54F;
      background-image: linear-gradient(180deg, #FF8F00, #FFD54F);
      padding: 20px 10px;
      
    }

    /* Style the header links */
    .header a {
      float: left;
      color: black;
      text-align: center;
      padding: 12px;
      text-decoration: none;
      font-size: 18px;
      line-height: 25px;
      border-radius: 4px;
    }

    /* Style the logo link (notice that we set the same value of line-height and font-size to prevent the header to increase when the font gets bigger */
    .header a.logo {
      font-size: 25px;
      font-weight: bold;
    }

    /* Float the link section to the right */
    .header-right {
      float: right;
    }

    .right {
      float: right;
    }

    </style>

    <script>
        function date_time(id) {
            date = new Date;
            year = date.getFullYear();
            month = date.getMonth();
            months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
            d = date.getDate();
            day = date.getDay();
            days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
            h = date.getHours();
            if (h < 10) {
                h = "0" + h;
            }
            m = date.getMinutes();
            if (m < 10) {
                m = "0" + m;
            }
            s = date.getSeconds();
            if (s < 10) {
                s = "0" + s;
            }
            result = ' ' + months[month] + ' ' + d + '' + ', ' + year + ' | ' + days[day] + ' &nbsp; &nbsp;&nbsp; ' + h + ' : ' + m + ' : ' + s;
            document.getElementById(id).innerHTML = result;
            setTimeout('date_time("' + id + '");', '1000');
            return true;
        }

    </script>

@endsection
 
@section('sidebar')
    @parent
 
    <p>This is appended to the master sidebar.</p>
@endsection

@section('header')
    <div class="header sticky-top">
      <a href="#default" class="logo"><img class="img-fluid" src="/images/petlogo.png" style="width:100%; height:auto; max-width:100px;"/>Julalay's 24-hour Pet Center</a>
      <div class="header-right">
        <p class="hidden-sm hidden-xs" id="date_time"></p>
        <script type="text/javascript">window.onload = date_time('date_time');</script>
        <div>
            <p></p>
        </div>
        <div>
            <p><b>A Loving Home for Pets</b></p>
        </div>
        <div>
            <p>Grooming and Healthcare</p>
        </div>
      </div>
    </div>
@endsection
 
@section('content')
  <div>
    @if (count($errors) > 0)
         <div class = "alert alert-danger">
            <ul>
               @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
               @endforeach
            </ul>
         </div>
    @endif


    <div class="container bg-light pb-2 mt-4">
      <div class="container-fluid d-flex justify-content-between bg-info">
        <div class="p-2"><i class="fa-solid fa-user-check fa-2xl"></i></div>
        <div class="p-2"><b>Hello, {{session('username')}}</b></div>
        <a class="btn btn-danger m-2" href="/logout" role="button">Logout</a>
      </div>
      
      <br/>
      <h2 class="text-center">Book a visit today!</h2><br/>
      <form action = "/add" method = "post" enctype="multipart/form-data">
        @csrf
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="Name"><b>Pet's Name:</b></label>
            <input type="text" class="form-control" name="client" required="">
          </div>
        </div>
        <div class="row mt-4">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <strong>Start Time : </strong>
            <input type="time" name="start_time">    
         </div>
       </div>
        <div class="row mt-4">
         <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <strong>Finish Time : </strong>
            <input type="time" name="finish_time">    
         </div>
        </div>

         <div class="row mt-4">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <lable><b>Doctor: </b></lable>
                <select name="doctor" required="">
                  <option value="Dr. K">Dr. K</option>
                  <option value="Dr. L">Dr. L</option>
                  <option value="Dr. M">Dr. M</option>  
                  <option value="Dr. N">Dr.N</option>  
                </select>
            </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4" style="margin-top:60px">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>
      </form>
    </div>

  </div>
@endsection