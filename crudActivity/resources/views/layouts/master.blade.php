<!DOCTYPE html>
<html>
<head>
	@yield('meta')
</head>
<body>
	@yield('header')
	@section('sidebar')

	<div class="container">
        @yield('content')
    </div>

</body>
</html>