<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\AddApointmentController;
use App\Http\Controllers\ValidationController;
use App\Http\Controllers\ApprovalController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/index', function () {
    return view('index');
});

Route::get('/home', function () {
    return view('home');
});

Route::get('/admin', function () {  
    return view('admin');
});



Route::get('/', [ValidationController::class, 'showform' ]);
Route::post('/', [ValidationController::class, 'validateform' ]);

Route::get('register', [RegisterController::class, 'insertform' ]);
Route::post('create', [RegisterController::class, 'insert' ]);
Route::get('home', [AddApointmentController::class, 'appointmentForm' ]);
Route::post('add', [AddApointmentController::class, 'insert' ]);

Route::get('admin', [ApprovalController::class, 'list' ]);
Route::get('approvedrequests', [ApprovalController::class, 'approvedList' ]);
Route::get('deleteapproved/{id}',[ApprovalController::class, 'deleteApproved']);
Route::get('delete/{id}',[ApprovalController::class, 'destroy']);
Route::get('approve/{id}',[ApprovalController::class, 'approve']);

Route::get('/logout', function () {
    if(session()->has('username')){
        session()->pull('username');
    }
    return redirect('/index');
});

Route::get('/', function () {
    if(session()->has('username')){
        return redirect('/home');
    }
    else{
        return redirect('/index');
    }  
});
